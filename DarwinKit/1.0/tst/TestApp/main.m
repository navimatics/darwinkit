int main(int argc, char *argv[])
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    NSDictionary *defaults = [NSDictionary dictionaryWithObjectsAndKeys:
        @"Dark", @"DKGraphicsStyle",
        nil];
    [[NSUserDefaults standardUserDefaults] registerDefaults:defaults];
    [NSApplication loadStyledAppearance];
    int result = NSApplicationMain(argc, (const char **)argv);
    [pool release];
    return result;
}
