#import "AppDelegate.h"

@interface NSObject ()
- (void)insertInMainMenu;
@end
@implementation AppDelegate
@synthesize window = _window;
- (void)dealloc
{
    [super dealloc];
}
- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    if ([[NSBundle bundleWithPath:@"/Library/Frameworks/FScript.framework"] load])
        [NSClassFromString(@"FScriptMenuItem") insertInMainMenu];
}
- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)application
{
    return YES;
}
- (IBAction)toggleDarkAppearance:(id)sender
{
    NSString *name = [NSApplication styledAppearance];
    [NSApplication setStyledAppearance:0 == [name length] ? @"Dark" : @""];
}
@end
