/*
 * DarwinKit/mac/NSButtonCell+StyledAppearance.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>
#import <DarwinKit/mac/DKGraphicsStyle.h>

#define RoundedCornerRadius             4
#define SmallRoundedCornerRadius        1

@implementation NSButtonCell (StyledAppearance)
+ (void)loadStyledAppearance
{
    static BOOL done;
    if (!done)
    {
        [self
            swizzleInstanceMethod:@selector(drawBezelWithFrame:inView:)
            withMethod:@selector(__swizzle__drawBezelWithFrame:inView:)];
        [self
            swizzleInstanceMethod:@selector(drawImage:withFrame:inView:)
            withMethod:@selector(__swizzle__drawImage:withFrame:inView:)];
        [self
            swizzleInstanceMethod:@selector(drawTitle:withFrame:inView:)
            withMethod:@selector(__swizzle__drawTitle:withFrame:inView:)];
        [self
            swizzleInstanceMethod:@selector(attributedTitle)
            withMethod:@selector(__swizzle__attributedTitle)];
        [self
            swizzleInstanceMethod:@selector(attributedAlternateTitle)
            withMethod:@selector(__swizzle__attributedAlternateTitle)];
        done = YES;
    }
}
- (NSRect)__helper__buttonRectFromFrameRect:(NSRect)frameRect
{
    /* insets based on Xcode 4: CocoaPlugin.ibplugin: AppKitInsets.plist */
    struct Insets
    {
        CGFloat b, l, r, t;
    };
    static struct Insets regularInsets[] =
    {
        { 0, 0, 0, 0 },
        { 6/*8*/, 6, 6, 4 },            /* NSRoundedBezelStyle          = 1, */
        { 0, 0, 0, 0 },                 /* NSRegularSquareBezelStyle    = 2, */
        { 0, 0, 0, 0 },                 /* NSThickSquareBezelStyle      = 3, */
        { 0, 0, 0, 0 },                 /* NSThickerSquareBezelStyle    = 4, */
        { 0, 0, 0, 0 },                 /* NSDisclosureBezelStyle       = 5, */
        { 0, 0, 0, 0 },                 /* NSShadowlessSquareBezelStyle = 6, */
        { 7, 6, 7, 4 },                 /* NSCircularBezelStyle         = 7, */
        { 0, 0, 0, 0 },                 /* NSTexturedSquareBezelStyle   = 8, */
        { 4, 3, 3, 1 },                 /* NSHelpButtonBezelStyle       = 9, */
        { 0, 0, 0, 0 },                 /* NSSmallSquareBezelStyle      = 10, */
        { 0, 0, 0, 0 },                 /* NSTexturedRoundedBezelStyle  = 11, */
        { 0/*2*/, 0, 0, 0 },            /* NSRoundRectBezelStyle        = 12, */
        { 0, 0, 0, 0 },                 /* NSRecessedBezelStyle         = 13, */
        { 2/*3*/, 4, 4, 2 },            /* NSRoundedDisclosureBezelStyle= 14, */
        { 0, 0, 0, 0 },                 /* NSInlineBezelStyle           = 15, */
    };
    static struct Insets smallInsets[] =
    {
        { 0, 0, 0, 0 },
        { 5/*7*/, 5, 5, 4 },            /* NSRoundedBezelStyle          = 1, */
        { 0, 0, 0, 0 },                 /* NSRegularSquareBezelStyle    = 2, */
        { 0, 0, 0, 0 },                 /* NSThickSquareBezelStyle      = 3, */
        { 0, 0, 0, 0 },                 /* NSThickerSquareBezelStyle    = 4, */
        { 0, 0, 0, 0 },                 /* NSDisclosureBezelStyle       = 5, */
        { 0, 0, 0, 0 },                 /* NSShadowlessSquareBezelStyle = 6, */
        { 6, 6, 6, 5 },                 /* NSCircularBezelStyle         = 7, */
        { 0, 0, 0, 0 },                 /* NSTexturedSquareBezelStyle   = 8, */
        { 4, 3, 3, 1 },                 /* NSHelpButtonBezelStyle       = 9, */
        { 0, 0, 0, 0 },                 /* NSSmallSquareBezelStyle      = 10, */
        { 0, 0, 0, 0 },                 /* NSTexturedRoundedBezelStyle  = 11, */
        { 0/*2*/, 0, 0, 0 },            /* NSRoundRectBezelStyle        = 12, */
        { 0, 0, 0, 0 },                 /* NSRecessedBezelStyle         = 13, */
        { 2/*3*/, 3, 3, 2 },            /* NSRoundedDisclosureBezelStyle= 14, */
        { 0, 0, 0, 0 },                 /* NSInlineBezelStyle           = 15, */
    };
    static struct Insets miniInsets[] =
    {
        { 0, 0, 0, 0 },
        { 0/*2*/, 1, 1, 0 },            /* NSRoundedBezelStyle          = 1, */
        { 0, 0, 0, 0 },                 /* NSRegularSquareBezelStyle    = 2, */
        { 0, 0, 0, 0 },                 /* NSThickSquareBezelStyle      = 3, */
        { 0, 0, 0, 0 },                 /* NSThickerSquareBezelStyle    = 4, */
        { 0, 0, 0, 0 },                 /* NSDisclosureBezelStyle       = 5, */
        { 0, 0, 0, 0 },                 /* NSShadowlessSquareBezelStyle = 6, */
        { 7, 6, 7, 5 },                 /* NSCircularBezelStyle         = 7, */
        { 0, 0, 0, 0 },                 /* NSTexturedSquareBezelStyle   = 8, */
        { 4, 3, 3, 1 },                 /* NSHelpButtonBezelStyle       = 9, */
        { 0, 0, 0, 0 },                 /* NSSmallSquareBezelStyle      = 10, */
        { 0, 0, 0, 0 },                 /* NSTexturedRoundedBezelStyle  = 11, */
        { 0/*2*/, 0, 0, 1 },            /* NSRoundRectBezelStyle        = 12, */
        { 0, 0, 0, 0 },                 /* NSRecessedBezelStyle         = 13, */
        { 0, 1, 1, 0 },                 /* NSRoundedDisclosureBezelStyle= 14, */
        { 0, 0, 0, 0 },                 /* NSInlineBezelStyle           = 15, */
    };
    struct Insets *insets;
    NSBezelStyle bezelStyle = self.bezelStyle;
    NSControlSize controlSize = self.controlSize;
    if (bezelStyle < NSRoundedBezelStyle)
        bezelStyle = 0;
    else if (bezelStyle > NSInlineBezelStyle)
        bezelStyle = 0;
    switch (controlSize)
    {
    default:
    case NSRegularControlSize:
        insets = regularInsets + bezelStyle;
        break;
    case NSSmallControlSize:
        insets = smallInsets + bezelStyle;
        break;
    case NSMiniControlSize:
        insets = miniInsets + bezelStyle;
        break;
    }
    BOOL isFlipped = [[NSGraphicsContext currentContext] isFlipped];
    return CGRectMake(
        frameRect.origin.x + insets->l,
        frameRect.origin.y + isFlipped ? insets->t : insets->b,
        frameRect.size.width - insets->l - insets->r,
        frameRect.size.height - insets->t - insets->b);
}
- (void)__swizzle__drawBezelWithFrame:(NSRect)frameRect inView:(NSView *)controlView
{
    BOOL defaultDraw = YES;
    if ([self.controlView shouldAppearStyled])
    {
        CGRect buttonRect = [self __helper__buttonRectFromFrameRect:frameRect];
        BOOL isEnabled = self.isEnabled;
        BOOL isHighlighted = self.isHighlighted;
        BOOL isPushed = isHighlighted ||
            (((NSPushInCellMask | NSChangeGrayCellMask | NSChangeBackgroundCellMask) & self.showsStateBy) &&
            NSOnState == self.state);
        NSWindow *window = controlView.window;
        BOOL isDefault = window.defaultButtonCell == self && window.isKeyWindow;
        switch (self.bezelStyle)
        {
        case NSRoundedBezelStyle:
            if (isDefault)
                [DKCurrentGraphicsStyle
                    drawDefaultButtonFaceWithRect:buttonRect
                    cornerRadius:RoundedCornerRadius
                    isEnabled:isEnabled
                    isPushed:isPushed];
            else
                [DKCurrentGraphicsStyle
                    drawButtonFaceWithRect:buttonRect
                    cornerRadius:RoundedCornerRadius
                    isEnabled:isEnabled
                    isPushed:isPushed];
            defaultDraw = NO;
            break;
        case NSRegularSquareBezelStyle:
            break;
        case NSThickSquareBezelStyle:
            break;
        case NSThickerSquareBezelStyle:
            break;
        case NSDisclosureBezelStyle:
            break;
        case NSShadowlessSquareBezelStyle:
            break;
        case NSCircularBezelStyle:
            if (isDefault)
                [DKCurrentGraphicsStyle
                    drawDefaultButtonFaceWithRect:buttonRect
                    cornerRadius:buttonRect.size.height / 2
                    isEnabled:isEnabled
                    isPushed:isPushed];
            else
                [DKCurrentGraphicsStyle
                    drawButtonFaceWithRect:buttonRect
                    cornerRadius:buttonRect.size.height / 2
                    isEnabled:isEnabled
                    isPushed:isPushed];
            defaultDraw = NO;
            break;
        case NSTexturedSquareBezelStyle:
            break;
        case NSHelpButtonBezelStyle:
            [DKCurrentGraphicsStyle
                drawButtonFaceWithRect:buttonRect
                cornerRadius:buttonRect.size.height / 2
                isEnabled:isEnabled
                isPushed:isPushed];
            {
                static NSImage *helpButtonImage;
                if (nil == helpButtonImage)
                    helpButtonImage = [[DKCurrentGraphicsStyle imageNamed:@"helpButtonTemplate"] retain];
                NSImage *image = helpButtonImage;
                NSColor *imageColor = isEnabled ?
                    [DKCurrentGraphicsStyle styledControlTextColor] :
                    [DKCurrentGraphicsStyle styledDisabledControlTextColor];
                [DKCurrentGraphicsStyle
                    drawControlImage:image
                    rect:CGRectInset(buttonRect, 2, 2)
                    imageScaling:NSImageScaleProportionallyUpOrDown
                    color:imageColor
                    isEnabled:isEnabled];
            }
            defaultDraw = NO;
            break;
        case NSSmallSquareBezelStyle:
            [DKCurrentGraphicsStyle
                drawButtonFaceWithRect:buttonRect
                cornerRadius:SmallRoundedCornerRadius
                isEnabled:isEnabled
                isPushed:isPushed];
            defaultDraw = NO;
            break;
        case NSTexturedRoundedBezelStyle:
            [DKCurrentGraphicsStyle
                drawButtonFaceWithRect:buttonRect
                cornerRadius:RoundedCornerRadius
                isEnabled:isEnabled
                isPushed:isPushed];
            defaultDraw = NO;
            break;
        case NSRoundRectBezelStyle:
            [DKCurrentGraphicsStyle
                drawButtonFaceWithRect:buttonRect
                cornerRadius:buttonRect.size.height / 2
                isEnabled:isEnabled
                isPushed:isPushed];
            defaultDraw = NO;
            break;
        case NSRecessedBezelStyle:
            break;
        case NSRoundedDisclosureBezelStyle:
            [DKCurrentGraphicsStyle
                drawButtonFaceWithRect:buttonRect
                cornerRadius:RoundedCornerRadius
                isEnabled:isEnabled
                isPushed:isHighlighted];
            {
                static NSImage *disclosureUpImage, *disclosureDnImage;
                if (nil == disclosureUpImage)
                    disclosureUpImage = [[DKCurrentGraphicsStyle imageNamed:@"disclosureUpTemplate"] retain];
                if (nil == disclosureDnImage)
                    disclosureDnImage = [[DKCurrentGraphicsStyle imageNamed:@"disclosureDnTemplate"] retain];
                NSImage *image = NSOnState == self.state ? disclosureUpImage : disclosureDnImage;
                NSColor *imageColor = isEnabled ?
                    [DKCurrentGraphicsStyle styledControlTextColor] :
                    [DKCurrentGraphicsStyle styledDisabledControlTextColor];
                [DKCurrentGraphicsStyle
                    drawControlImage:image
                    rect:CGRectInset(buttonRect, 2, 2)
                    imageScaling:NSImageScaleProportionallyUpOrDown
                    color:imageColor
                    isEnabled:isEnabled];
            }
            defaultDraw = NO;
            break;
        case NSInlineBezelStyle:
            break;
        default:
            break;
        }
    }
    if (defaultDraw)
        [self __swizzle__drawBezelWithFrame:frameRect inView:controlView];
}
- (void)__swizzle__drawImage:(NSImage*)image withFrame:(NSRect)frameRect inView:(NSView*)controlView
{
    BOOL defaultDraw = YES;
    if ([self.controlView shouldAppearStyled])
    {
        BOOL isEnabled = self.isEnabled;
        NSColor *foregroundColor = isEnabled ?
            [DKCurrentGraphicsStyle styledControlTextColor] :
            [DKCurrentGraphicsStyle styledDisabledControlTextColor];
        switch (self.bezelStyle)
        {
        case NSRoundedBezelStyle:
            defaultDraw = NO;
            break;
        case NSRegularSquareBezelStyle:
            break;
        case NSThickSquareBezelStyle:
            break;
        case NSThickerSquareBezelStyle:
            break;
        case NSDisclosureBezelStyle:
            break;
        case NSShadowlessSquareBezelStyle:
            if (self.isHighlighted && image != self.alternateImage)
                foregroundColor = isEnabled ?
                    [DKCurrentGraphicsStyle styledDefaultButtonTextColor] :
                    [DKCurrentGraphicsStyle styledDisabledControlTextColor];
            defaultDraw = NO;
            break;
        case NSCircularBezelStyle:
            defaultDraw = NO;
            break;
        case NSTexturedSquareBezelStyle:
            break;
        case NSHelpButtonBezelStyle:
            defaultDraw = NO;
            break;
        case NSSmallSquareBezelStyle:
            defaultDraw = NO;
            break;
        case NSTexturedRoundedBezelStyle:
            if ((NSContentsCellMask & self.showsStateBy) && NSOnState == self.state)
                foregroundColor = isEnabled ?
                    [DKCurrentGraphicsStyle styledAlternateControlTextColor] :
                    [DKCurrentGraphicsStyle styledDisabledAlternateControlTextColor];
            defaultDraw = NO;
            break;
        case NSRoundRectBezelStyle:
            defaultDraw = NO;
            break;
        case NSRecessedBezelStyle:
            break;
        case NSRoundedDisclosureBezelStyle:
            defaultDraw = NO;
            break;
        case NSInlineBezelStyle:
            break;
        default:
            break;
        }
        if (!defaultDraw)
            [DKCurrentGraphicsStyle
                drawControlImage:image
                rect:frameRect
                imageScaling:self.imageScaling
                color:foregroundColor
                isEnabled:isEnabled];
    }
    if (defaultDraw)
        [self __swizzle__drawImage:image withFrame:frameRect inView:controlView];
}
- (NSRect)__swizzle__drawTitle:(NSAttributedString*)title withFrame:(NSRect)frameRect inView:(NSView*)controlView
{
    BOOL defaultDraw = YES;
    if ([self.controlView shouldAppearStyled])
    {
        BOOL isEnabled = self.isEnabled;
        NSColor *foregroundColor = isEnabled ?
            [DKCurrentGraphicsStyle styledControlTextColor] :
            [DKCurrentGraphicsStyle styledDisabledControlTextColor];
        switch (self.bezelStyle)
        {
        case NSRoundedBezelStyle:
            break;
        case NSRegularSquareBezelStyle:
            break;
        case NSThickSquareBezelStyle:
            break;
        case NSThickerSquareBezelStyle:
            break;
        case NSDisclosureBezelStyle:
            break;
        case NSShadowlessSquareBezelStyle:
            break;
        case NSCircularBezelStyle:
            break;
        case NSTexturedSquareBezelStyle:
            break;
        case NSHelpButtonBezelStyle:
            break;
        case NSSmallSquareBezelStyle:
            break;
        case NSTexturedRoundedBezelStyle:
            if ((NSContentsCellMask & self.showsStateBy) && NSOnState == self.state)
                foregroundColor = isEnabled ?
                    [DKCurrentGraphicsStyle styledAlternateControlTextColor] :
                    [DKCurrentGraphicsStyle styledDisabledAlternateControlTextColor];
            defaultDraw = NO;
            break;
        case NSRoundRectBezelStyle:
            break;
        case NSRecessedBezelStyle:
            break;
        case NSRoundedDisclosureBezelStyle:
            break;
        case NSInlineBezelStyle:
            break;
        default:
            break;
        }
        if (!defaultDraw)
            [DKCurrentGraphicsStyle
                drawControlText:[title string]
                rect:frameRect
                controlSize:self.controlSize
                color:foregroundColor
                alignment:self.alignment];
    }
    if (defaultDraw)
        frameRect = [self __swizzle__drawTitle:title withFrame:frameRect inView:controlView];
    return frameRect;
}
- (NSAttributedString *)__swizzle__attributedTitle
{
    NSAttributedString *title = [self __swizzle__attributedTitle];
    if ([self.controlView shouldAppearStyled])
    {
        NSRange range = NSMakeRange(0, [title length]);
        if (0 < range.length)
        {
            NSWindow *window = self.controlView.window;
            BOOL isDefault = window.defaultButtonCell == self && window.isKeyWindow;
            title = [[[NSMutableAttributedString alloc] initWithAttributedString:title] autorelease];
            NSColor *foregroundColor;
            if (isDefault)
                foregroundColor = [DKCurrentGraphicsStyle styledDefaultButtonTextColor];
            else
            {
                foregroundColor =
                    [title attribute:NSForegroundColorAttributeName atIndex:0 effectiveRange:NULL];
                if (nil != foregroundColor)
                    foregroundColor = [DKCurrentGraphicsStyle getStyledColor:foregroundColor];
                else
                    foregroundColor = self.isEnabled ?
                        [DKCurrentGraphicsStyle styledControlTextColor] :
                        [DKCurrentGraphicsStyle styledDisabledControlTextColor];
            }
            [(id)title addAttribute:NSForegroundColorAttributeName value:foregroundColor range:range];
        }
    }
    return title;
}
- (NSAttributedString *)__swizzle__attributedAlternateTitle
{
    NSAttributedString *title = [self __swizzle__attributedAlternateTitle];
    if ([self.controlView shouldAppearStyled])
    {
        NSRange range = NSMakeRange(0, [title length]);
        if (0 < range.length)
        {
            NSWindow *window = self.controlView.window;
            BOOL isDefault = window.defaultButtonCell == self && window.isKeyWindow;
            title = [[[NSMutableAttributedString alloc] initWithAttributedString:title] autorelease];
            NSColor *foregroundColor;
            if (isDefault)
                foregroundColor = [DKCurrentGraphicsStyle styledDefaultButtonTextColor];
            else
            {
                foregroundColor =
                    [title attribute:NSForegroundColorAttributeName atIndex:0 effectiveRange:NULL];
                if (nil != foregroundColor)
                    foregroundColor = [DKCurrentGraphicsStyle getStyledColor:foregroundColor];
                else
                    foregroundColor = self.isEnabled ?
                        [DKCurrentGraphicsStyle styledControlTextColor] :
                        [DKCurrentGraphicsStyle styledDisabledControlTextColor];
            }
            [(id)title addAttribute:NSForegroundColorAttributeName value:foregroundColor range:range];
        }
    }
    return title;
}
@end
