/*
 * DarwinKit/mac/NSPopUpButtonCell+StyledAppearance.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>
#import <DarwinKit/mac/DKGraphicsStyle.h>

@implementation NSPopUpButtonCell (StyledAppearance)
+ (void)loadStyledAppearance
{
    static BOOL done;
    if (!done)
    {
        [self
            swizzleInstanceMethod:@selector(drawBezelWithFrame:inView:)
            withMethod:@selector(__swizzle2__drawBezelWithFrame:inView:)];
        [self
            swizzleInstanceMethod:@selector(drawImageWithFrame:inView:)
            withMethod:@selector(__swizzle2__drawImageWithFrame:inView:)];
        [self
            swizzleInstanceMethod:@selector(drawTitle:withFrame:inView:)
            withMethod:@selector(__swizzle2__drawTitle:withFrame:inView:)];
        done = YES;
    }
}
- (NSRect)__helper__buttonRectFromFrameRect:(NSRect)frameRect
{
    /* insets based on experimentation */
    struct Insets
    {
        CGFloat b, l, r, t;
    };
    static struct Insets regularInsets[] =
    {
        { 0, 0, 0, 0 },
        { 4, 3, 3, 2 },                 /* NSRoundedBezelStyle          = 1, */
        { 0, 0, 0, 0 },                 /* NSRegularSquareBezelStyle    = 2, */
        { 0, 0, 0, 0 },                 /* NSThickSquareBezelStyle      = 3, */
        { 0, 0, 0, 0 },                 /* NSThickerSquareBezelStyle    = 4, */
        { 0, 0, 0, 0 },                 /* NSDisclosureBezelStyle       = 5, */
        { 0, 0, 0, 0 },                 /* NSShadowlessSquareBezelStyle = 6, */
        { 0, 0, 0, 0 },                 /* NSCircularBezelStyle         = 7, */
        { 0, 0, 0, 0 },                 /* NSTexturedSquareBezelStyle   = 8, */
        { 0, 0, 0, 0 },                 /* NSHelpButtonBezelStyle       = 9, */
        { 0, 0, 0, 0 },                 /* NSSmallSquareBezelStyle      = 10, */
        { 0, 0, 0, 0 },                 /* NSTexturedRoundedBezelStyle  = 11, */
        { 0, 0, 0, 0 },                 /* NSRoundRectBezelStyle        = 12, */
        { 0, 0, 0, 0 },                 /* NSRecessedBezelStyle         = 13, */
        { 0, 0, 0, 0 },                 /* NSRoundedDisclosureBezelStyle= 14, */
        { 0, 0, 0, 0 },                 /* NSInlineBezelStyle           = 15, */
    };
    static struct Insets smallInsets[] =
    {
        { 0, 0, 0, 0 },
        { 4, 3, 3, 1 },                 /* NSRoundedBezelStyle          = 1, */
        { 0, 0, 0, 0 },                 /* NSRegularSquareBezelStyle    = 2, */
        { 0, 0, 0, 0 },                 /* NSThickSquareBezelStyle      = 3, */
        { 0, 0, 0, 0 },                 /* NSThickerSquareBezelStyle    = 4, */
        { 0, 0, 0, 0 },                 /* NSDisclosureBezelStyle       = 5, */
        { 0, 0, 0, 0 },                 /* NSShadowlessSquareBezelStyle = 6, */
        { 0, 0, 0, 0 },                 /* NSCircularBezelStyle         = 7, */
        { 0, 0, 0, 0 },                 /* NSTexturedSquareBezelStyle   = 8, */
        { 0, 0, 0, 0 },                 /* NSHelpButtonBezelStyle       = 9, */
        { 0, 0, 0, 0 },                 /* NSSmallSquareBezelStyle      = 10, */
        { 0, 0, 0, 0 },                 /* NSTexturedRoundedBezelStyle  = 11, */
        { 0, 0, 0, 0 },                 /* NSRoundRectBezelStyle        = 12, */
        { 0, 0, 0, 0 },                 /* NSRecessedBezelStyle         = 13, */
        { 0, 0, 0, 0 },                 /* NSRoundedDisclosureBezelStyle= 14, */
        { 0, 0, 0, 0 },                 /* NSInlineBezelStyle           = 15, */
    };
    static struct Insets miniInsets[] =
    {
        { 0, 0, 0, 0 },
        { 0, 1, 2, 0 },                 /* NSRoundedBezelStyle          = 1, */
        { 0, 0, 0, 0 },                 /* NSRegularSquareBezelStyle    = 2, */
        { 0, 0, 0, 0 },                 /* NSThickSquareBezelStyle      = 3, */
        { 0, 0, 0, 0 },                 /* NSThickerSquareBezelStyle    = 4, */
        { 0, 0, 0, 0 },                 /* NSDisclosureBezelStyle       = 5, */
        { 0, 0, 0, 0 },                 /* NSShadowlessSquareBezelStyle = 6, */
        { 0, 0, 0, 0 },                 /* NSCircularBezelStyle         = 7, */
        { 0, 0, 0, 0 },                 /* NSTexturedSquareBezelStyle   = 8, */
        { 0, 0, 0, 0 },                 /* NSHelpButtonBezelStyle       = 9, */
        { 0, 0, 0, 0 },                 /* NSSmallSquareBezelStyle      = 10, */
        { 0, 0, 0, 0 },                 /* NSTexturedRoundedBezelStyle  = 11, */
        { 0, 0, 0, 0 },                 /* NSRoundRectBezelStyle        = 12, */
        { 0, 0, 0, 0 },                 /* NSRecessedBezelStyle         = 13, */
        { 0, 0, 0, 0 },                 /* NSRoundedDisclosureBezelStyle= 14, */
        { 0, 0, 0, 0 },                 /* NSInlineBezelStyle           = 15, */
    };
    struct Insets *insets;
    NSBezelStyle bezelStyle = self.bezelStyle;
    NSControlSize controlSize = self.controlSize;
    if (bezelStyle < NSRoundedBezelStyle)
        bezelStyle = 0;
    else if (bezelStyle > NSInlineBezelStyle)
        bezelStyle = 0;
    switch (controlSize)
    {
    default:
    case NSRegularControlSize:
        insets = regularInsets + bezelStyle;
        break;
    case NSSmallControlSize:
        insets = smallInsets + bezelStyle;
        break;
    case NSMiniControlSize:
        insets = miniInsets + bezelStyle;
        break;
    }
    BOOL isFlipped = [[NSGraphicsContext currentContext] isFlipped];
    return CGRectMake(
        frameRect.origin.x + insets->l,
        frameRect.origin.y + isFlipped ? insets->t : insets->b,
        frameRect.size.width - insets->l - insets->r,
        frameRect.size.height - insets->t - insets->b);
}
- (NSRect)__helper__arrowRectWithFrame:(NSRect)frameRect
{
    CGSize arrowSize;
    if (self.pullsDown)
        switch (self.controlSize)
        {
        default:
        case NSRegularControlSize:
            arrowSize.width = 8;
            arrowSize.height = 6;
            break;
        case NSSmallControlSize:
            arrowSize.width = 8;
            arrowSize.height = 6;
            break;
        case NSMiniControlSize:
            arrowSize.width = 6;
            arrowSize.height = 5;
            break;
        }
    else
        switch (self.controlSize)
        {
        default:
        case NSRegularControlSize:
            arrowSize.width = 8;
            arrowSize.height = 13;
            break;
        case NSSmallControlSize:
            arrowSize.width = 8;
            arrowSize.height = 10;
            break;
        case NSMiniControlSize:
            arrowSize.width = 7;
            arrowSize.height = 9;
            break;
        }
    CGRect buttonRect = [self __helper__buttonRectFromFrameRect:frameRect];
    buttonRect.size.width -= 4;
    switch (self.arrowPosition)
    {
    default:
        return CGRectNull;
    case NSPopUpArrowAtCenter:
        return CGRectMake(
            buttonRect.origin.x + (buttonRect.size.width - arrowSize.width),
            buttonRect.origin.y + (buttonRect.size.height - arrowSize.height) / 2,
            arrowSize.width,
            arrowSize.height);
    case NSPopUpArrowAtBottom:
        return CGRectMake(
            buttonRect.origin.x + buttonRect.size.width - arrowSize.width,
            buttonRect.origin.y + (buttonRect.size.height - arrowSize.height) / 2,
            arrowSize.width,
            arrowSize.height);
    }
}
- (void)__swizzle2__drawBezelWithFrame:(NSRect)frameRect inView:(NSView *)controlView
{
    if ([self.controlView shouldAppearStyled])
    {
        [self __swizzle2__drawBezelWithFrame:frameRect inView:controlView];
        CGRect arrowRect = [self __helper__arrowRectWithFrame:frameRect];
        if (!CGRectIsNull(arrowRect))
        {
            BOOL isEnabled = self.isEnabled;
            static NSImage *popUpImage, *popDnImage;
            if (nil == popUpImage)
                popUpImage = [[DKCurrentGraphicsStyle imageNamed:@"popUpTemplate"] retain];
            if (nil == popDnImage)
                popDnImage = [[DKCurrentGraphicsStyle imageNamed:@"popDnTemplate"] retain];
            NSImage *image = self.pullsDown ? popDnImage : popUpImage;
            NSColor *imageColor = isEnabled ?
                [DKCurrentGraphicsStyle styledControlTextColor] :
                [DKCurrentGraphicsStyle styledDisabledControlTextColor];
            [DKCurrentGraphicsStyle
                drawControlImage:image
                rect:arrowRect
                imageScaling:NSImageScaleProportionallyUpOrDown
                color:imageColor
                isEnabled:isEnabled];
        }
    }
    else
        [self __swizzle2__drawBezelWithFrame:frameRect inView:controlView];
}
- (void)__swizzle2__drawImageWithFrame:(NSRect)frameRect inView:(NSView *)controlView
{
    if ([self.controlView shouldAppearStyled])
        [self drawImage:self.image withFrame:[self imageRectForBounds:frameRect] inView:controlView];
    else
        [self __swizzle2__drawImageWithFrame:frameRect inView:controlView];
}
- (NSRect)__swizzle2__drawTitle:(NSAttributedString*)title withFrame:(NSRect)frameRect inView:(NSView*)controlView
{
    BOOL defaultDraw = YES;
    if ([self.controlView shouldAppearStyled])
    {
        BOOL isEnabled = self.isEnabled;
        NSColor *foregroundColor = isEnabled ?
            [DKCurrentGraphicsStyle styledControlTextColor] :
            [DKCurrentGraphicsStyle styledDisabledControlTextColor];
        switch (self.bezelStyle)
        {
        case NSRoundedBezelStyle:
            defaultDraw = NO;
            break;
        case NSRegularSquareBezelStyle:
            break;
        case NSThickSquareBezelStyle:
            break;
        case NSThickerSquareBezelStyle:
            break;
        case NSDisclosureBezelStyle:
            break;
        case NSShadowlessSquareBezelStyle:
            break;
        case NSCircularBezelStyle:
            break;
        case NSTexturedSquareBezelStyle:
            break;
        case NSHelpButtonBezelStyle:
            break;
        case NSSmallSquareBezelStyle:
            break;
        case NSTexturedRoundedBezelStyle:
            if ((NSContentsCellMask & self.showsStateBy) && NSOnState == self.state)
                foregroundColor = isEnabled ?
                    [DKCurrentGraphicsStyle styledAlternateControlTextColor] :
                    [DKCurrentGraphicsStyle styledDisabledAlternateControlTextColor];
            defaultDraw = NO;
            break;
        case NSRoundRectBezelStyle:
            break;
        case NSRecessedBezelStyle:
            break;
        case NSRoundedDisclosureBezelStyle:
            break;
        case NSInlineBezelStyle:
            break;
        default:
            break;
        }
        if (!defaultDraw)
            [DKCurrentGraphicsStyle
                drawControlText:[title string]
                rect:frameRect
                controlSize:self.controlSize
                color:foregroundColor
                alignment:self.alignment];
    }
    if (defaultDraw)
        frameRect = [self __swizzle2__drawTitle:title withFrame:frameRect inView:controlView];
    return frameRect;
}
@end
