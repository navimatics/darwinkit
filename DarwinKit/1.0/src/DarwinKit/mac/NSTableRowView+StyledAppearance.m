/*
 * DarwinKit/mac/NSTableRowView+StyledAppearance.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>
#import <DarwinKit/mac/DKGraphicsStyle.h>

@implementation NSTableRowView (StyledAppearance)
+ (void)loadStyledAppearance
{
    static BOOL done;
    if (!done)
    {
        [self
            swizzleInstanceMethod:@selector(drawBackgroundInRect:)
            withMethod:@selector(__swizzle__drawBackgroundInRect:)];
        [self
            swizzleInstanceMethod:@selector(drawSelectionInRect:)
            withMethod:@selector(__swizzle__drawSelectionInRect:)];
        [self
            swizzleInstanceMethod:@selector(drawSeparatorInRect:)
            withMethod:@selector(__swizzle__drawSeparatorInRect:)];
        done = YES;
    }
}
- (void)__swizzle__drawBackgroundInRect:(NSRect)rect
{
    if ([self shouldAppearStyled])
    {
        if (self.isGroupRowStyle)
            [DKCurrentGraphicsStyle drawTableRowFaceWithRect:self.bounds];
        else
        {
            [[DKCurrentGraphicsStyle getStyledBackgroundColor:self.backgroundColor] setFill];
            NSRectFill(rect);
        }
    }
    else
        [self __swizzle__drawBackgroundInRect:rect];
}
- (void)__swizzle__drawSelectionInRect:(NSRect)rect
{
    BOOL defaultDraw = YES;
    if ([self shouldAppearStyled])
    {
        switch (self.selectionHighlightStyle)
        {
        default:
        case NSTableViewSelectionHighlightStyleNone:
            break;
        case NSTableViewSelectionHighlightStyleRegular:
            defaultDraw = NO;
            break;
        case NSTableViewSelectionHighlightStyleSourceList:
            break;
        }
        if (!defaultDraw)
        {
            NSView *view = [self superview];
            while (nil != view && ![view isKindOfClass:[NSTableView class]])
                view = [view superview];
            NSWindow *window = self.window;
            NSResponder *firstResponder = window.firstResponder;
            if (nil != view && [firstResponder isKindOfClass:[NSView class]] &&
                [(id)firstResponder isDescendantOf:view] &&
                [window isKeyWindow])
                [[DKCurrentGraphicsStyle styledSecondarySelectedControlColor] setFill];
            else
                [[DKCurrentGraphicsStyle styledAlternateSelectedControlColor] setFill];
            NSRectFill(rect);
        }
    }
    if (defaultDraw)
        [self __swizzle__drawSelectionInRect:rect];
}
- (void)__swizzle__drawSeparatorInRect:(NSRect)rect
{
    [self __swizzle__drawSeparatorInRect:rect];
}
@end
