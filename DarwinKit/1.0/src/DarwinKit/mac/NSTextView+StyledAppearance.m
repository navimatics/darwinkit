/*
 * DarwinKit/mac/NSTextView+StyledAppearance.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>
#import <DarwinKit/mac/DKGraphicsStyle.h>

@implementation NSTextView (StyledAppearance)
+ (void)loadStyledAppearance
{
    static BOOL done;
    if (!done)
    {
        [self
            swizzleInstanceMethod:@selector(textColor)
            withMethod:@selector(__swizzle__textColor)];
        [self
            swizzleInstanceMethod:@selector(backgroundColor)
            withMethod:@selector(__swizzle__backgroundColor)];
        [self
            swizzleInstanceMethod:@selector(typingAttributes)
            withMethod:@selector(__swizzle__typingAttributes)];
        [self
            swizzleInstanceMethod:@selector(selectedTextAttributes)
            withMethod:@selector(__swizzle__selectedTextAttributes)];
        [self
            swizzleInstanceMethod:@selector(linkTextAttributes)
            withMethod:@selector(__swizzle__linkTextAttributes)];
        [self
            swizzleInstanceMethod:@selector(insertionPointColor)
            withMethod:@selector(__swizzle__insertionPointColor)];
        done = YES;
    }
}
- (NSColor *)__swizzle__textColor
{
    if ([self shouldAppearStyled])
        return [DKCurrentGraphicsStyle getStyledColor:[self __swizzle__textColor]];
    else
        return [self __swizzle__textColor];
}
- (NSColor *)__swizzle__backgroundColor
{
    if ([self shouldAppearStyled])
        return [DKCurrentGraphicsStyle getStyledBackgroundColor:[self __swizzle__backgroundColor]];
    else
        return [self __swizzle__backgroundColor];
}
- (NSDictionary *)__swizzle__typingAttributes
{
    if ([self shouldAppearStyled])
    {
        NSDictionary *attr = [self __swizzle__typingAttributes];
        NSColor *foregroundColor = [attr objectForKey:NSForegroundColorAttributeName];
        foregroundColor = foregroundColor
            ? [DKCurrentGraphicsStyle getStyledColor:foregroundColor]
            : [DKCurrentGraphicsStyle styledTextColor];
        NSColor *backgroundColor = [attr objectForKey:NSBackgroundColorAttributeName];
        backgroundColor = backgroundColor
            ? [DKCurrentGraphicsStyle getStyledBackgroundColor:backgroundColor]
            : [DKCurrentGraphicsStyle styledTextBackgroundColor];
        attr = [NSMutableDictionary dictionaryWithDictionary:attr];
        [(id)attr setObject:foregroundColor forKey:NSForegroundColorAttributeName];
        [(id)attr setObject:backgroundColor forKey:NSBackgroundColorAttributeName];
        return attr;
    }
    else
        return [self __swizzle__typingAttributes];
}
- (NSDictionary *)__swizzle__selectedTextAttributes
{
    if ([self shouldAppearStyled])
    {
        NSDictionary *attr = [self __swizzle__selectedTextAttributes];
        NSColor *foregroundColor = [DKCurrentGraphicsStyle styledSelectedTextColor];
        NSColor *backgroundColor = [DKCurrentGraphicsStyle styledSelectedTextBackgroundColor];
        attr = [NSMutableDictionary dictionaryWithDictionary:attr];
        [(id)attr setObject:foregroundColor forKey:NSForegroundColorAttributeName];
        [(id)attr setObject:backgroundColor forKey:NSBackgroundColorAttributeName];
        return attr;
    }
    else
        return [self __swizzle__selectedTextAttributes];
}
- (NSDictionary *)__swizzle__linkTextAttributes
{
    if ([self shouldAppearStyled])
    {
        NSDictionary *attr = [self __swizzle__linkTextAttributes];
        NSColor *foregroundColor = [DKCurrentGraphicsStyle styledLinkTextColor];
        attr = [NSMutableDictionary dictionaryWithDictionary:attr];
        [(id)attr setObject:foregroundColor forKey:NSForegroundColorAttributeName];
        return attr;
    }
    else
        return [self __swizzle__linkTextAttributes];
}
- (NSColor *)__swizzle__insertionPointColor
{
    if ([self shouldAppearStyled])
        return [DKCurrentGraphicsStyle getStyledColor:[self __swizzle__insertionPointColor]];
    else
        return [self __swizzle__insertionPointColor];
}
@end
