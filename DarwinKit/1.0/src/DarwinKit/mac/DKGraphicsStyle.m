/*
 * DarwinKit/mac/DKGraphicsStyle.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>
#import <DarwinKit/mac/DKGraphicsStyle.h>

DKGraphicsStyle *DKCurrentGraphicsStyle;
@implementation DKGraphicsStyle
static NSMutableDictionary *_graphicsStyleClasses;
+ (void)load
{
    _graphicsStyleClasses = [[NSMutableDictionary alloc] init];
}
+ (void)registerGraphicsStyleClass:(Class)cls forName:(NSString *)name
{
    [_graphicsStyleClasses setObject:cls forKey:name];
}
+ (DKGraphicsStyle *)currentStyle
{
    return DKCurrentGraphicsStyle;
}
+ (void)setCurrentStyleWithName:(NSString *)name
{
    [DKCurrentGraphicsStyle release];
    DKCurrentGraphicsStyle = [[[_graphicsStyleClasses objectForKey:name] alloc] init];
}
- (NSBundle *)bundle
{
    static NSBundle *bundle;
    if (nil == bundle)
        bundle = [NSBundle bundleForClass:[self class]];
    return bundle;
}
- (NSImage *)imageNamed:(NSString *)name
{
    NSImage *image = [[[NSImage alloc]
        initWithContentsOfFile:[[self bundle] pathForImageResource:name]] autorelease];
    if ([name hasSuffix:@"Template"])
        image.template = YES;
    return image;
}
@end
