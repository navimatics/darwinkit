/*
 * DarwinKit/mac/DKGradientGraphicsStyle.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>
#import <DarwinKit/mac/DKGradientGraphicsStyle.h>
#import <sys/time.h>

#define NELEMS(a)                       (sizeof(a) / sizeof((a)[0]))

static NSBezierPath *bezierPathWithTopRoundedCornersRect(CGRect rect, CGFloat radius)
{
    NSBezierPath *path = [[[NSBezierPath alloc] init] autorelease];
    [path moveToPoint:rect.origin];
    [path
        appendBezierPathWithArcFromPoint:CGPointMake(
            rect.origin.x,
            rect.origin.y + rect.size.height)
        toPoint:CGPointMake(
            rect.origin.x + rect.size.width,
            rect.origin.y + rect.size.height)
        radius:radius];
    [path
        appendBezierPathWithArcFromPoint:CGPointMake(
            rect.origin.x + rect.size.width,
            rect.origin.y + rect.size.height)
        toPoint:CGPointMake(
            rect.origin.x + rect.size.width,
            rect.origin.y)
        radius:radius];
    [path
        lineToPoint:CGPointMake(
            rect.origin.x + rect.size.width,
            rect.origin.y)];
    [path lineToPoint:rect.origin];
    [path closePath];
    return path;
}
static inline CGFloat normalizeRadius(CGFloat dim, CGFloat radius)
{
    CGFloat halfDim = dim / 2;
    return halfDim > radius ? radius : halfDim;
}

@implementation DKGradientGraphicsStyle
{
    NSColor *_styledWindowFrameColorHilite;
    NSColor *_styledWindowFrameColorLite;
    NSColor *_styledWindowFrameColorDark;
    NSGradient *_styledButtonBezelGradient;
    NSGradient *_styledButtonGradient;
    NSGradient *_styledDisabledButtonBezelGradient;
    NSGradient *_styledDisabledButtonGradient;
    NSGradient *_styledDefaultButtonBezelGradient;
    NSGradient *_styledDefaultButtonGradient;
    NSGradient *_styledAnimatedDefaultButtonGradients[10];
    NSGradient *_styledDisabledDefaultButtonBezelGradient;
    NSGradient *_styledDisabledDefaultButtonGradient;
    NSGradient *_styledControlBezelGradient;
    NSGradient *_styledControlGradient;
    NSGradient *_styledDisabledControlBezelGradient;
    NSGradient *_styledDisabledControlGradient;
}
- (id)init
{
    self = [super init];
    if (nil != self)
    {
        _styledWindowFrameColorHilite = [[[self styledWindowFrameColor] shadedColorWithDifference:0.20] retain];
        _styledWindowFrameColorLite = [[[self styledWindowFrameColor] shadedColorWithDifference:0.10] retain];
        _styledWindowFrameColorDark = [[[self styledWindowFrameColor] shadedColorWithDifference:-0.10] retain];
        _styledButtonBezelGradient = [[NSGradient alloc]
            initWithStartingColor:[[self styledControlColor] shadedColorWithDifference:0.20]
            endingColor:[[self styledControlColor] shadedColorWithDifference:-0.20]];
        _styledButtonGradient = [[NSGradient alloc]
            initWithStartingColor:[[self styledControlColor] shadedColorWithDifference:0.10]
            endingColor:[[self styledControlColor] shadedColorWithDifference:-0.10]];
        _styledDisabledButtonBezelGradient = [[NSGradient alloc]
            initWithStartingColor:[[self styledDisabledControlColor] shadedColorWithDifference:0.10]
            endingColor:[[self styledDisabledControlColor] shadedColorWithDifference:-0.10]];
        _styledDisabledButtonGradient = [[NSGradient alloc]
            initWithStartingColor:[[self styledDisabledControlColor] shadedColorWithDifference:0.05]
            endingColor:[[self styledDisabledControlColor] shadedColorWithDifference:-0.05]];
        _styledDefaultButtonBezelGradient = [[NSGradient alloc]
            initWithStartingColor:[[self styledDefaultButtonColor] shadedColorWithDifference:0.20]
            endingColor:[[self styledDefaultButtonColor] shadedColorWithDifference:-0.20]];
        _styledDefaultButtonGradient = [[NSGradient alloc]
            initWithStartingColor:[[self styledDefaultButtonColor] shadedColorWithDifference:0.10]
            endingColor:[[self styledDefaultButtonColor] shadedColorWithDifference:-0.10]];
        for (size_t i = 0; NELEMS(_styledAnimatedDefaultButtonGradients) / 2 > i; i++)
            _styledAnimatedDefaultButtonGradients[i] = [[NSGradient alloc]
                initWithStartingColor:[[self styledDefaultButtonColor] shadedColorWithDifference:0.10 + 0.02 * i]
                endingColor:[[self styledDefaultButtonColor] shadedColorWithDifference:-0.10 + 0.02 * i]];
        for (size_t i = NELEMS(_styledAnimatedDefaultButtonGradients) / 2, j = i - 1;
            NELEMS(_styledAnimatedDefaultButtonGradients) > i;
            i++, j--)
            _styledAnimatedDefaultButtonGradients[i] = [[NSGradient alloc]
                initWithStartingColor:[[self styledDefaultButtonColor] shadedColorWithDifference:0.10 + 0.02 * j]
                endingColor:[[self styledDefaultButtonColor] shadedColorWithDifference:-0.10 + 0.02 * j]];
        _styledDisabledDefaultButtonBezelGradient = [[NSGradient alloc]
            initWithStartingColor:[[self styledDisabledDefaultButtonColor] shadedColorWithDifference:0.10]
            endingColor:[[self styledDisabledDefaultButtonColor] shadedColorWithDifference:-0.10]];
        _styledDisabledDefaultButtonGradient = [[NSGradient alloc]
            initWithStartingColor:[[self styledDisabledDefaultButtonColor] shadedColorWithDifference:0.05]
            endingColor:[[self styledDisabledDefaultButtonColor] shadedColorWithDifference:-0.05]];
        _styledControlBezelGradient = [[NSGradient alloc]
            initWithStartingColor:[[self styledControlColor] shadedColorWithDifference:-0.05]
            endingColor:[[self styledControlColor] shadedColorWithDifference:0]];
        _styledControlGradient = [[NSGradient alloc]
            initWithStartingColor:[self styledShadowColor]
            endingColor:[self styledControlBackgroundColor]];
        _styledDisabledControlBezelGradient = [[NSGradient alloc]
            initWithStartingColor:[[self styledDisabledControlColor] shadedColorWithDifference:-0.05]
            endingColor:[[self styledDisabledControlColor] shadedColorWithDifference:0]];
        _styledDisabledControlGradient = [_styledControlGradient retain];
    }
    return self;
}
- (void)dealloc
{
    [_styledDisabledControlGradient release];
    [_styledDisabledControlBezelGradient release];
    [_styledControlGradient release];
    [_styledControlBezelGradient release];
    [_styledDisabledDefaultButtonGradient release];
    [_styledDisabledDefaultButtonBezelGradient release];
    for (size_t i = 0; NELEMS(_styledAnimatedDefaultButtonGradients) > i; i++)
        [_styledAnimatedDefaultButtonGradients[i] release];
    [_styledDefaultButtonGradient release];
    [_styledDefaultButtonBezelGradient release];
    [_styledDisabledButtonGradient release];
    [_styledDisabledButtonBezelGradient release];
    [_styledButtonGradient release];
    [_styledButtonBezelGradient release];
    [_styledWindowFrameColorDark release];
    [_styledWindowFrameColorLite release];
    [_styledWindowFrameColorHilite release];
    [super dealloc];
}
/* drawing controls */
- (void)drawWindowFrameWithRect:(NSRect)rect
{
    BOOL isFlipped = [[NSGraphicsContext currentContext] isFlipped];
    [[self styledWindowFrameGradientForRect:rect]
        drawInRect:rect
        angle:isFlipped ? 90 : 270];
}
- (void)drawWindowFrameWithRect:(NSRect)rect cornerRadius:(CGFloat)cornerRadius
{
    BOOL isFlipped = [[NSGraphicsContext currentContext] isFlipped];
    CGFloat radius = normalizeRadius(rect.size.height, cornerRadius);
    [[self styledWindowFrameGradientForRect:rect]
        drawInBezierPath:bezierPathWithTopRoundedCornersRect(rect, radius)
        angle:isFlipped ? 90 : 270];
}
- (void)drawButtonFaceWithRect:(NSRect)rect cornerRadius:(CGFloat)cornerRadius
    isEnabled:(BOOL)isEnabled isPushed:(BOOL)isPushed
{
    BOOL isFlipped = [[NSGraphicsContext currentContext] isFlipped];
    CGFloat radius = normalizeRadius(rect.size.height, cornerRadius);
    NSGradient *bezelGradient = isEnabled ?
        [self styledButtonBezelGradient] :
        [self styledDisabledButtonBezelGradient];
    NSGradient *faceGradient = isEnabled ?
        [self styledButtonGradient] :
        [self styledDisabledButtonGradient];
    [bezelGradient
        drawInBezierPath:[NSBezierPath
            bezierPathWithRoundedRect:rect
            xRadius:radius
            yRadius:radius]
        angle:isFlipped ? 90 : 270];
    radius = normalizeRadius(rect.size.height - 2, cornerRadius);
    [faceGradient
        drawInBezierPath:[NSBezierPath
            bezierPathWithRoundedRect:CGRectInset(rect, 1, 1)
            xRadius:radius
            yRadius:radius]
        angle:isFlipped ^ isPushed ? 90 : 270];
}
- (void)drawDefaultButtonFaceWithRect:(NSRect)rect cornerRadius:(CGFloat)cornerRadius
    isEnabled:(BOOL)isEnabled isPushed:(BOOL)isPushed
{
    BOOL isFlipped = [[NSGraphicsContext currentContext] isFlipped];
    CGFloat radius = normalizeRadius(rect.size.height, cornerRadius);
    NSGradient *bezelGradient = isEnabled ?
        [self styledDefaultButtonBezelGradient] :
        [self styledDisabledDefaultButtonBezelGradient];
    NSGradient *faceGradient = isEnabled ?
        (!isPushed ?
            [self styledAnimatedDefaultButtonGradient] :
            [self styledDefaultButtonGradient]) :
        [self styledDisabledDefaultButtonGradient];
    [bezelGradient
        drawInBezierPath:[NSBezierPath
            bezierPathWithRoundedRect:rect
            xRadius:radius
            yRadius:radius]
        angle:isFlipped ? 90 : 270];
    radius = normalizeRadius(rect.size.height - 2, cornerRadius);
    [faceGradient
        drawInBezierPath:[NSBezierPath
            bezierPathWithRoundedRect:CGRectInset(rect, 1, 1)
            xRadius:radius
            yRadius:radius]
        angle:isFlipped ^ isPushed ? 90 : 270];
}
- (void)drawSegmentedFaceWithRect:(NSRect)rect cornerRadius:(CGFloat)cornerRadius
    segmentWidths:(const CGFloat *)widths selectedSegments:(BOOL *)selected segmentCount:(NSInteger)count
    pushedWhenSelected:(BOOL)pushedWhenSelected
    isEnabled:(BOOL)isEnabled
{
    BOOL isFlipped = [[NSGraphicsContext currentContext] isFlipped];
    NSGradient *regularBezelGradient = isEnabled ?
        [self styledButtonBezelGradient] :
        [self styledDisabledButtonBezelGradient];
    NSGradient *selectedBezelGradient = pushedWhenSelected ?
        regularBezelGradient :
        (isEnabled ?
            [self styledDefaultButtonBezelGradient] :
            [self styledDisabledDefaultButtonBezelGradient]);
    NSGradient *regularFaceGradient = isEnabled ?
        [self styledButtonGradient] :
        [self styledDisabledButtonGradient];
    NSGradient *selectedFaceGradient = pushedWhenSelected ?
        regularFaceGradient :
        (isEnabled ?
            [self styledDefaultButtonGradient] :
            [self styledDisabledDefaultButtonGradient]);
    [NSGraphicsContext saveGraphicsState];
    CGFloat defaultLineWidth = [NSBezierPath defaultLineWidth];
    [NSBezierPath setDefaultLineWidth:1];
    [[self styledControlBackgroundColor] setStroke];
    CGFloat radius = normalizeRadius(rect.size.height, cornerRadius);
    [[NSBezierPath
        bezierPathWithRoundedRect:rect
        xRadius:radius
        yRadius:radius] addClip];
    CGFloat totalWidth = 0;
    for (NSInteger i = 0; count > i; i++)
    {
        NSGradient *bezelGradient = !selected[i] ? regularBezelGradient : selectedBezelGradient;
        [bezelGradient
            drawInRect:CGRectMake(
                rect.origin.x + totalWidth,
                rect.origin.y,
                widths[i],
                rect.size.height)
            angle:isFlipped ? 90 : 270];
        totalWidth += widths[i];
    }
    totalWidth = 0;
    for (NSInteger i = 0; count > i; i++)
    {
        if (0 < i)
            [NSBezierPath
                strokeLineFromPoint:CGPointMake(rect.origin.x + totalWidth, rect.origin.y)
                toPoint:CGPointMake(rect.origin.x + totalWidth, rect.origin.y + rect.size.height)];
        totalWidth += widths[i];
    }
    CGRect insetRect = CGRectInset(rect, 1, 1);
    radius = normalizeRadius(insetRect.size.height, cornerRadius);
    [[NSBezierPath
        bezierPathWithRoundedRect:insetRect
        xRadius:radius
        yRadius:radius] addClip];
    totalWidth = 0;
    for (NSInteger i = 0; count > i; i++)
    {
        NSGradient *faceGradient = !selected[i] ? regularFaceGradient : selectedFaceGradient;
        BOOL isPushed = selected[i] && pushedWhenSelected;
        [faceGradient
            drawInRect:CGRectMake(
                rect.origin.x + totalWidth + 1,
                rect.origin.y + 1,
                widths[i] - 2,
                rect.size.height - 2)
            angle:isFlipped ^ isPushed ? 90 : 270];
        totalWidth += widths[i];
    }
    [NSBezierPath setDefaultLineWidth:defaultLineWidth];
    [NSGraphicsContext restoreGraphicsState];
}
- (void)drawStepperFaceWithRect:(NSRect)rect cornerRadius:(CGFloat)cornerRadius
    pushedButton:(NSInteger)pushedButton
    isEnabled:(BOOL)isEnabled
{
    BOOL isFlipped = [[NSGraphicsContext currentContext] isFlipped];
    NSGradient *bezelGradient = isEnabled ?
        [self styledButtonBezelGradient] :
        [self styledDisabledButtonBezelGradient];
    NSGradient *faceGradient = isEnabled ?
        [self styledButtonGradient] :
        [self styledDisabledButtonGradient];
    CGFloat radius = normalizeRadius(rect.size.width, cornerRadius);
    [bezelGradient
        drawInBezierPath:[NSBezierPath
            bezierPathWithRoundedRect:rect
            xRadius:radius
            yRadius:radius]
        angle:isFlipped ? 90 : 270];
    [NSGraphicsContext saveGraphicsState];
    rect = CGRectInset(rect, 1, 1);
    radius = normalizeRadius(rect.size.width, cornerRadius);
    [[NSBezierPath
        bezierPathWithRoundedRect:rect
        xRadius:radius
        yRadius:radius] addClip];
    [faceGradient
        drawInRect:CGRectMake(
            rect.origin.x, rect.origin.y + (isFlipped ? 0 : rect.size.height / 2),
            rect.size.width, rect.size.height / 2)
        angle: isFlipped ^ (pushedButton == +1) ? 90 : 270];
    [faceGradient
        drawInRect:CGRectMake(
            rect.origin.x, rect.origin.y + (!isFlipped ? 0 : rect.size.height / 2),
            rect.size.width, rect.size.height / 2)
        angle: isFlipped ^ (pushedButton == -1) ? 90 : 270];
    [NSGraphicsContext restoreGraphicsState];
}
- (void)drawControlFaceWithRect:(NSRect)rect
    isEnabled:(BOOL)isEnabled
{
    BOOL isFlipped = [[NSGraphicsContext currentContext] isFlipped];
    NSGradient *bezelGradient = isEnabled ?
        [self styledControlBezelGradient] :
        [self styledDisabledControlBezelGradient];
    NSGradient *faceGradient = isEnabled ?
        [self styledControlGradient] :
        [self styledDisabledControlGradient];
    [bezelGradient
        drawInRect:rect
        angle:isFlipped ? 90 : 270];
    [faceGradient
        drawInRect:CGRectMake(
            rect.origin.x + 1, rect.origin.y + (isFlipped ? 0 : 1),
            rect.size.width - 2, rect.size.height - 1)
        angle:isFlipped ? 90 : 270];
    [[self styledControlBackgroundColor] setFill];
    NSRectFill(CGRectInset(rect, 2, 2));
}
- (void)drawControlFaceWithRect:(NSRect)rect
    hasBackground:(BOOL)hasBackground
    isEnabled:(BOOL)isEnabled
{
    if (!hasBackground)
    {
        [NSGraphicsContext saveGraphicsState];
        NSBezierPath *path = [NSBezierPath bezierPathWithRect:rect];
        [path appendBezierPathWithRect:CGRectInset(rect, 2, 2)];
        [path addClip];
    }
    BOOL isFlipped = [[NSGraphicsContext currentContext] isFlipped];
    NSGradient *bezelGradient = isEnabled ?
        [self styledControlBezelGradient] :
        [self styledDisabledControlBezelGradient];
    NSGradient *faceGradient = isEnabled ?
        [self styledControlGradient] :
        [self styledDisabledControlGradient];
    [bezelGradient
        drawInRect:rect
        angle:isFlipped ? 90 : 270];
    [faceGradient
        drawInRect:CGRectMake(
            rect.origin.x + 1, rect.origin.y + (isFlipped ? 0 : 1),
            rect.size.width - 2, rect.size.height - 1)
        angle:isFlipped ? 90 : 270];
    if (!hasBackground)
        [NSGraphicsContext restoreGraphicsState];
    else
    {
        [[self styledControlBackgroundColor] setFill];
        NSRectFill(CGRectInset(rect, 2, 2));
    }
}
- (void)drawControlFaceWithRect:(NSRect)rect
    cornerRadius:(CGFloat)cornerRadius
    isEnabled:(BOOL)isEnabled
{
    BOOL isFlipped = [[NSGraphicsContext currentContext] isFlipped];
    CGFloat radius = normalizeRadius(rect.size.height, cornerRadius);
    NSGradient *bezelGradient = isEnabled ?
        [self styledControlBezelGradient] :
        [self styledDisabledControlBezelGradient];
    NSGradient *faceGradient = isEnabled ?
        [self styledControlGradient] :
        [self styledDisabledControlGradient];
    [bezelGradient
        drawInBezierPath:[NSBezierPath
            bezierPathWithRoundedRect:rect
            xRadius:radius
            yRadius:radius]
        angle:isFlipped ? 90 : 270];
    radius = normalizeRadius(rect.size.height - 1, cornerRadius);
    [faceGradient
        drawInBezierPath:[NSBezierPath
            bezierPathWithRoundedRect:CGRectMake(
            rect.origin.x + 1, rect.origin.y + (isFlipped ? 0 : 1),
            rect.size.width - 2, rect.size.height - 1)
            xRadius:radius
            yRadius:radius]
        angle:isFlipped ? 90 : 270];
    radius = normalizeRadius(rect.size.height - 4, cornerRadius);
    [[self styledControlBackgroundColor] setFill];
    [[NSBezierPath bezierPathWithRoundedRect:CGRectInset(rect, 2, 2) xRadius:radius yRadius:radius]
        fill];
}
- (void)drawControlSimpleFaceWithRect:(NSRect)rect
    isEnabled:(BOOL)isEnabled
{
    NSColor *bezelColor = isEnabled ?
        [self styledControlColor] :
        [self styledDisabledControlColor];
    [bezelColor setFill];
    NSFrameRectWithWidth(rect, 1);
    [[self styledControlBackgroundColor] setFill];
    NSRectFill(CGRectInset(rect, 1, 1));
}
- (void)drawControlSimpleFaceWithRect:(NSRect)rect
    hasBackground:(BOOL)hasBackground
    isEnabled:(BOOL)isEnabled
{
    NSColor *bezelColor = isEnabled ?
        [self styledControlColor] :
        [self styledDisabledControlColor];
    [bezelColor setFill];
    NSFrameRectWithWidth(rect, 1);
    if (hasBackground)
    {
        [[self styledControlBackgroundColor] setFill];
        NSRectFill(CGRectInset(rect, 1, 1));
    }
}
- (void)drawControlFaceBackgroundWithRect:(NSRect)rect
{
    [[self styledControlBackgroundColor] setFill];
    NSRectFill(rect);
}
- (void)drawTableRowFaceWithRect:(NSRect)rect
{
    BOOL isFlipped = [[NSGraphicsContext currentContext] isFlipped];
    NSGradient *bezelGradient = [self styledButtonBezelGradient];
    NSGradient *faceGradient = [self styledButtonGradient];
    [bezelGradient
        drawInRect:rect
        angle:isFlipped ? 90 : 270];
    [faceGradient
        drawInRect:CGRectMake(
            rect.origin.x, rect.origin.y + (isFlipped ? 0 : 1),
            rect.size.width, rect.size.height - 2)
        angle:isFlipped ? 90 : 270];
}
- (void)drawControlText:(NSString *)text rect:(NSRect)rect
    font:(NSFont *)font color:(NSColor *)color shadow:(BOOL)shadow
    alignment:(NSTextAlignment)alignment
{
    static NSMutableParagraphStyle *leftAlignedParagraphStyle;
    static NSMutableParagraphStyle *centeredParagraphStyle;
    static NSMutableParagraphStyle *rightAlignedParagraphStyle;
    NSParagraphStyle *paragraphStyle;
    switch (alignment)
    {
    default:
    case NSLeftTextAlignment:
        if (nil == leftAlignedParagraphStyle)
        {
            leftAlignedParagraphStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
            [leftAlignedParagraphStyle setAlignment:NSLeftTextAlignment];
            [leftAlignedParagraphStyle setLineBreakMode:NSLineBreakByTruncatingTail];
        }
        paragraphStyle = leftAlignedParagraphStyle;
        break;
    case NSCenterTextAlignment:
        if (nil == centeredParagraphStyle)
        {
            centeredParagraphStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
            [centeredParagraphStyle setAlignment:NSCenterTextAlignment];
            [centeredParagraphStyle setLineBreakMode:NSLineBreakByTruncatingTail];
        }
        paragraphStyle = centeredParagraphStyle;
        break;
    case NSRightTextAlignment:
        if (nil == rightAlignedParagraphStyle)
        {
            rightAlignedParagraphStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
            [rightAlignedParagraphStyle setAlignment:NSRightTextAlignment];
            [rightAlignedParagraphStyle setLineBreakMode:NSLineBreakByTruncatingTail];
        }
        paragraphStyle = rightAlignedParagraphStyle;
        break;
    }
    BOOL isFlipped = [[NSGraphicsContext currentContext] isFlipped];
    NSMutableDictionary *attributes = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
        font, NSFontAttributeName,
        shadow ? [self styledShadowColor] : color, NSForegroundColorAttributeName,
        paragraphStyle, NSParagraphStyleAttributeName,
        nil];
    CGSize size = [text sizeWithAttributes:attributes];
    rect.origin.y += (rect.size.height - size.height) / 2;
    rect.size.height = size.height;
    if (shadow)
    {
        [text drawInRect:CGRectOffset(rect, 0, isFlipped ? +1 : -1) withAttributes:attributes];
        [attributes setObject:color forKey:NSForegroundColorAttributeName];
    }
    [text drawInRect:rect withAttributes:attributes];
    [attributes release];
}
- (void)drawControlText:(NSString *)text rect:(NSRect)rect
    controlSize:(NSControlSize)controlSize color:(NSColor *)color shadow:(BOOL)shadow
    alignment:(NSTextAlignment)alignment
{
    return [self
        drawControlText:text
        rect:rect
        font:[NSFont controlContentFontOfSize:[NSFont systemFontSizeForControlSize:controlSize]]
        color:color
        shadow:shadow
        alignment:alignment];
}
- (void)drawControlText:(NSString *)text rect:(NSRect)rect
    controlSize:(NSControlSize)controlSize color:(NSColor *)color
    alignment:(NSTextAlignment)alignment
{
    return [self
        drawControlText:text
        rect:rect
        font:[NSFont controlContentFontOfSize:[NSFont systemFontSizeForControlSize:controlSize]]
        color:color
        shadow:NO
        alignment:alignment];
}
- (void)drawControlImage:(NSImage *)image rect:(NSRect)rect
    imageScaling:(NSImageScaling)imageScaling
    color:(NSColor *)color
    isEnabled:(BOOL)isEnabled
{
    CGRect imageRect;
    imageRect.size = [image size];
    switch (imageScaling)
    {
    case NSImageScaleProportionallyDown:
    case NSImageScaleProportionallyUpOrDown:
        {
            CGFloat xscale = rect.size.width / imageRect.size.width;
            CGFloat yscale = rect.size.height / imageRect.size.height;
            CGFloat scale = NSImageScaleProportionallyDown == imageScaling ?
                MIN(1.0, MIN(xscale, yscale)) : MIN(xscale, yscale);
            imageRect.size.width *= scale;
            imageRect.size.height *= scale;
        }
        /* fall through */
    case NSImageScaleNone:
        if (imageRect.size.width < rect.size.width)
        {
            rect.origin.x += (rect.size.width - imageRect.size.width) / 2;
            rect.size.width = imageRect.size.width;
            imageRect.origin.x = 0;
        }
        else
        {
            imageRect.origin.x = (imageRect.size.width - rect.size.width) / 2;
            imageRect.size.width = rect.size.width;
        }
        if (imageRect.size.height < rect.size.height)
        {
            rect.origin.y += (rect.size.height - imageRect.size.height) / 2;
            rect.size.height = imageRect.size.height;
            imageRect.origin.y = 0;
        }
        else
        {
            imageRect.origin.y = (imageRect.size.height - rect.size.height) / 2;
            imageRect.size.height = rect.size.height;
        }
        break;
    default:
    case NSImageScaleAxesIndependently:
        imageRect.origin = CGPointZero;
        break;
    }
    if (image.isTemplate)
    {
        NSGraphicsContext *context = [NSGraphicsContext currentContext];
        [context saveGraphicsState];
        CGContextRef cgcontext = [context graphicsPort];
        CGImageRef cgimage = [image CGImageForProposedRect:&imageRect context:context hints:nil];
        if ([context isFlipped])
        {
            CGContextTranslateCTM(cgcontext, rect.origin.x, rect.origin.y + rect.size.height);
            CGContextScaleCTM(cgcontext, 1, -1);
            CGContextTranslateCTM(cgcontext, -rect.origin.x, -rect.origin.y);
        }
        CGContextClipToMask(cgcontext, rect, cgimage);
        [color setFill];
        NSRectFill(rect);
        [context restoreGraphicsState];
    }
    else
    {
        [image
            drawInRect:rect
            fromRect:CGRectZero
            operation:NSCompositeSourceOver
            fraction:isEnabled ? 1.0 : 0.5
            respectFlipped:YES
            hints:nil];
    }
}
/* gradients */
- (NSGradient *)styledWindowFrameGradientForRect:(NSRect)rect
{
    NSGradient *gradient = [[NSGradient alloc] initWithColorsAndLocations:
        _styledWindowFrameColorHilite, 0.0,
        _styledWindowFrameColorLite, 1.0 / rect.size.height,
        _styledWindowFrameColorDark, 1.0,
        nil];
    return [gradient autorelease];
}
- (NSGradient *)styledButtonBezelGradient
{
    return _styledButtonBezelGradient;
}
- (NSGradient *)styledButtonGradient
{
    return _styledButtonGradient;
}
- (NSGradient *)styledDisabledButtonBezelGradient
{
    return _styledDisabledButtonBezelGradient;
}
- (NSGradient *)styledDisabledButtonGradient
{
    return _styledDisabledButtonGradient;
}
- (NSGradient *)styledDefaultButtonBezelGradient
{
    return _styledDefaultButtonBezelGradient;
}
- (NSGradient *)styledDefaultButtonGradient
{
    return _styledDefaultButtonGradient;
}
- (NSGradient *)styledAnimatedDefaultButtonGradient
{
    struct timeval tv; gettimeofday(&tv, NULL);
    size_t frameIndex =
        (tv.tv_usec / 100000) % NELEMS(_styledAnimatedDefaultButtonGradients);
    return _styledAnimatedDefaultButtonGradients[frameIndex];
}
- (NSGradient *)styledDisabledDefaultButtonBezelGradient
{
    return _styledDisabledDefaultButtonBezelGradient;
}
- (NSGradient *)styledDisabledDefaultButtonGradient
{
    return _styledDisabledDefaultButtonGradient;
}
- (NSGradient *)styledControlBezelGradient
{
    return _styledControlBezelGradient;
}
- (NSGradient *)styledControlGradient
{
    return _styledControlGradient;
}
- (NSGradient *)styledDisabledControlBezelGradient
{
    return _styledDisabledControlBezelGradient;
}
- (NSGradient *)styledDisabledControlGradient
{
    return _styledDisabledControlGradient;
}
@end
