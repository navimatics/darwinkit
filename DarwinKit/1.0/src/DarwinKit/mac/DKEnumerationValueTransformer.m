/*
 * DarwinKit/mac/DKEnumerationValueTransformer.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>

@implementation DKEnumerationValueTransformer
{
    NSDictionary *_forwardDict;
    NSDictionary *_inverseDict;
}
- (id)initWithIdentifiersAndValues:(NSString *)firstIdent, ...
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    va_list ap;
    va_start(ap, firstIdent);
    for (NSString *ident = firstIdent; nil != ident; ident = va_arg(ap, NSString *))
    {
        NSNumber *value = [[NSNumber alloc] initWithInt:va_arg(ap, int)];
        [dict setObject:value forKey:ident];
        [value release];
    }
    va_end(ap);
    self = [self initWithEnumerationDictionary:dict];
    [dict release];
    return self;
}
- (id)initWithEnumerationDictionary:(NSDictionary *)dict
{
    self = [super init];
    if (nil != self)
    {
        _forwardDict = [dict retain];
        _inverseDict = [[NSMutableDictionary alloc] init];
        for (id key in _forwardDict)
            [(id)_inverseDict setObject:key forKey:[_forwardDict objectForKey:key]];
    }
    return self;
}
- (void)dealloc
{
    [_inverseDict release];
    [_forwardDict release];
    [super dealloc];
}
+ (Class)transformedValueClass
{
    return [NSNumber class];
}
+ (BOOL)allowsReverseTransformation
{
    return YES;
}
- (id)transformedValue:(id)value
{
    return [_forwardDict objectForKey:value];
}
- (id)reverseTransformedValue:(id)value
{
    return [_inverseDict objectForKey:value];
}
@end
