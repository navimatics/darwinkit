/*
 * DarwinKit/mac/NSBezierPath+Compatibility.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>

@implementation NSBezierPath (Compatibility)
- (void)addLineToPoint:(CGPoint)point
{
    [self lineToPoint:point];
}
- (void)addCurveToPoint:(CGPoint)endPoint
    controlPoint1:(CGPoint)controlPoint1
    controlPoint2:(CGPoint)controlPoint2
{
    [self curveToPoint:endPoint controlPoint1:controlPoint1 controlPoint2:controlPoint2];
}
- (void)addArcWithCenter:(CGPoint)center
    radius:(CGFloat)radius
    startAngle:(CGFloat)startAngle
    endAngle:(CGFloat)endAngle
    clockwise:(BOOL)clockwise
{
    [self
        appendBezierPathWithArcWithCenter:center
        radius:radius
        startAngle:startAngle * 180 / M_PI
        endAngle:endAngle * 180 / M_PI
        clockwise:clockwise];
}
- (void)appendPath:(NSBezierPath *)path
{
    [self appendBezierPath:path];
}
@end
