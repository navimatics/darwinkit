/*
 * DarwinKit/mac/NSImageCell+StyledAppearance.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>
#import <DarwinKit/mac/DKGraphicsStyle.h>

#define RoundedCornerRadius             4

@implementation NSImageCell (StyledAppearance)
+ (void)loadStyledAppearance
{
    static BOOL done;
    if (!done)
    {
        [self
            swizzleInstanceMethod:@selector(drawWithFrame:inView:)
            withMethod:@selector(__swizzle__drawWithFrame:inView:)];
        done = YES;
    }
}
- (void)__swizzle__drawWithFrame:(NSRect)frame inView:(NSView *)controlView
{
    BOOL defaultDraw = YES;
    if ([self.controlView shouldAppearStyled])
    {
        switch (self.imageFrameStyle)
        {
        default:
        case NSImageFrameNone:
            [self drawInteriorWithFrame:frame inView:controlView];
            defaultDraw = NO;
            break;
        case NSImageFramePhoto:
            break;
        case NSImageFrameGrayBezel:
        case NSImageFrameGroove:
            [DKCurrentGraphicsStyle
                drawControlFaceWithRect:frame cornerRadius:RoundedCornerRadius
                isEnabled:YES];
            [self drawInteriorWithFrame:frame inView:controlView];
            defaultDraw = NO;
            break;
        case NSImageFrameButton:
            break;
        }
    }
    if (defaultDraw)
        [self __swizzle__drawWithFrame:frame inView:controlView];
}
@end
