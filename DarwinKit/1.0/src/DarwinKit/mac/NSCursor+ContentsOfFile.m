/*
 * DarwinKit/mac/NSCursor+ContentsOfFile.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>

@implementation NSCursor (CursorResource)
- (id)initWithContentsOfFile:(NSString *)path
{
    NSImage *image = [[NSImage alloc] initWithContentsOfFile:path];
    if (nil == image)
    {
        [self dealloc];
        return nil;
    }
    CGPoint hotspot = CGPointZero;
    path = [[path stringByDeletingPathExtension] stringByAppendingPathExtension:@"hotspot"];
    NSString *hotspotString = [[NSString alloc]
        initWithContentsOfFile:path encoding:NSUTF8StringEncoding error:0];
    if (nil != hotspotString)
    {
        hotspot = NSPointFromString(hotspotString);
        [hotspotString release];
        CGSize size = image.size;
        hotspot.x *= size.width;
        hotspot.y *= size.height;
    }
    self = [self initWithImage:image hotSpot:hotspot];
    [image release];
    return self;
}
- (id)initWithResource:(NSString *)name
{
    return [self initWithContentsOfFile:[[[NSBundle mainBundle] resourcePath]
        stringByAppendingPathComponent:name]];
}
@end
