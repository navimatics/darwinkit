/*
 * DarwinKit/mac/NSWindow+StyledAppearance.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>
#import <DarwinKit/mac/DKGraphicsStyle.h>
#import <objc/runtime.h>

static BOOL HackInstanceMethod(Class cls, SEL oldsel, SEL newsel, IMP newimp)
{
    Method oldmeth = class_getInstanceMethod(cls, oldsel);
    if (NULL == oldmeth)
    {
        NSLog(@"%s error: selector %s not found in class %@",
            __PRETTY_FUNCTION__, sel_getName(oldsel), cls);
        return NO;
    }
    IMP oldimp = method_getImplementation(oldmeth);
    if (oldimp == newimp)
        return YES;
    const char *typenc = method_getTypeEncoding(oldmeth);
    class_addMethod(cls, oldsel, oldimp, typenc);
    class_addMethod(cls, newsel, newimp, typenc);
    method_exchangeImplementations(
        class_getInstanceMethod(cls, oldsel),
        class_getInstanceMethod(cls, newsel));
    return YES;
}

#define WindowTitleBarHeight            22
#define WindowTitleFontSize             13
#define UtilityWindowTitleBarHeight     16
#define UtilityWindowTitleFontSize      11
#define RoundedCornerRadius             4
#define TitlePadding                    8

static size_t GraphicsStyleKey;
#define GraphicsStyleFVDR               1 /* FrameViewDrawRect processing */
#define GraphicsStyleVSAS               2 /* viewsShouldAppearStyled returns YES */
#define GetGraphicsStyle(win)           \
    [(NSNumber *)objc_getAssociatedObject(win, &GraphicsStyleKey) unsignedIntValue]
#define SetGraphicsStyle(win, num)      \
    objc_setAssociatedObject(win, &GraphicsStyleKey, [NSNumber numberWithUnsignedInt:num],\
        OBJC_ASSOCIATION_RETAIN_NONATOMIC)

@interface NSView ()
#if defined(DARWINKIT_CONFIG_USECOCOAPRIVATEAPIS)
- (NSRect)titlebarRect;
- (NSRect)_titlebarTitleRect;
- (float)roundedCornerRadius;
- (id)titleFont;
#endif
- (void)__hack__drawRect:(NSRect)rect;
@end
static void FrameViewDrawRect(NSView *self, SEL _cmd, NSRect rect)
{
    [self __hack__drawRect:rect];
        /* always call the underlying drawRect: or the top corners will not paint properly */
    if (nil == DKCurrentGraphicsStyle)
        return;
    NSWindow *window = self.window;
    if (GraphicsStyleFVDR & GetGraphicsStyle(window))
    {
        BOOL isKeyWindow = window.isKeyWindow;
        BOOL isUtilityWindow = 0 != (window.styleMask & NSUtilityWindowMask);
        CGRect frameRect = window.frame;
        CGRect contentRect = [window contentRectForFrameRect:frameRect];
        CGRect unifiedRect;
        unifiedRect.origin = CGPointMake(
            0,
            (contentRect.origin.y - frameRect.origin.y) + contentRect.size.height);
        unifiedRect.size = CGSizeMake(
            frameRect.size.width,
            frameRect.size.height - unifiedRect.origin.y);
        CGRect titlebarRect;
#if defined(DARWINKIT_CONFIG_USECOCOAPRIVATEAPIS)
        if ([self respondsToSelector:@selector(titlebarRect)])
            titlebarRect = [self titlebarRect];
        else
#endif
        {
            if (!isUtilityWindow)
                titlebarRect = CGRectMake(
                    0, unifiedRect.origin.y + unifiedRect.size.height - WindowTitleBarHeight,
                    unifiedRect.size.width, WindowTitleBarHeight);
            else
                titlebarRect = CGRectMake(
                    0, unifiedRect.origin.y + unifiedRect.size.height - UtilityWindowTitleBarHeight,
                    unifiedRect.size.width, UtilityWindowTitleBarHeight);
        }
        [[DKCurrentGraphicsStyle styledWindowBackgroundColor] setFill];
        NSRectFill(CGRectMake(0, 0, frameRect.size.width, unifiedRect.origin.y));
        if (!isUtilityWindow)
        {
            CGFloat roundedCornerRadius = RoundedCornerRadius;
#if defined(DARWINKIT_CONFIG_USECOCOAPRIVATEAPIS)
            if ([self respondsToSelector:@selector(roundedCornerRadius)])
                roundedCornerRadius = [self roundedCornerRadius];
#endif
            [DKCurrentGraphicsStyle drawWindowFrameWithRect:unifiedRect cornerRadius:roundedCornerRadius];
        }
        else
            [DKCurrentGraphicsStyle drawWindowFrameWithRect:unifiedRect];
        NSFont *titleFont;
#if defined(DARWINKIT_CONFIG_USECOCOAPRIVATEAPIS)
        if ([self respondsToSelector:@selector(titleFont)])
            titleFont = [self titleFont];
        else
#endif
        {
            if (!isUtilityWindow)
                titleFont = [NSFont titleBarFontOfSize:WindowTitleFontSize];
            else
                titleFont = [NSFont titleBarFontOfSize:UtilityWindowTitleFontSize];
        }
        static NSMutableParagraphStyle *paragraphStyle = nil;
        if (nil == paragraphStyle)
        {
            paragraphStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
            [paragraphStyle setAlignment:NSCenterTextAlignment];
            [paragraphStyle setLineBreakMode:NSLineBreakByTruncatingTail];
        }
        NSMutableDictionary *titleAttributes = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
            titleFont, NSFontAttributeName,
            [DKCurrentGraphicsStyle styledShadowColor], NSForegroundColorAttributeName,
            paragraphStyle, NSParagraphStyleAttributeName,
            nil];
        NSString *title = window.title;
        CGRect titleRect;
#if defined(DARWINKIT_CONFIG_USECOCOAPRIVATEAPIS)
        if ([self respondsToSelector:@selector(_titlebarTitleRect)])
            titleRect = [self _titlebarTitleRect];
        else
#endif
        {
            CGFloat titleMinX = titlebarRect.origin.x + TitlePadding;
            CGFloat titleMaxX = titlebarRect.origin.x + titlebarRect.size.width - TitlePadding;
            NSButton *leftStandardButton = [window standardWindowButton:NSWindowZoomButton];
            if (nil == leftStandardButton)
            {
                leftStandardButton = [window standardWindowButton:NSWindowMiniaturizeButton];
                if (nil == leftStandardButton)
                    leftStandardButton = [window standardWindowButton:NSWindowCloseButton];
            }
            if (nil != leftStandardButton)
            {
                CGRect leftStandardButtonFrame = leftStandardButton.frame;
                titleMinX =
                    leftStandardButtonFrame.origin.x + leftStandardButtonFrame.size.width + TitlePadding;
            }
            NSButton *rightStandardButton = [window standardWindowButton:NSWindowFullScreenButton];
            if (nil == rightStandardButton)
                rightStandardButton = [window standardWindowButton:NSWindowToolbarButton];
            if (nil != rightStandardButton)
            {
                CGRect rightStandardButtonFrame = rightStandardButton.frame;
                titleMaxX =
                    rightStandardButtonFrame.origin.x - rightStandardButtonFrame.size.width - TitlePadding;
            }
            NSButton *documentStandardButton = [window standardWindowButton:NSWindowDocumentIconButton];
            if (nil != documentStandardButton)
            {
                CGRect documentStandardButtonFrame = documentStandardButton.frame;
                titleMinX =
                    documentStandardButtonFrame.origin.x + documentStandardButtonFrame.size.width + TitlePadding;
            }
            CGSize titleSize = [title sizeWithAttributes:titleAttributes];
            titleRect.origin.x = titlebarRect.origin.x + (titlebarRect.size.width - titleSize.width) / 2;
            if (titleRect.origin.x < titleMinX)
                titleRect.origin.x = titleMinX;
            titleRect.size.width = titleSize.width;
            if (titleRect.size.width > titleMaxX - titleMinX)
                titleRect.size.width = titleMaxX - titleMinX;
            titleRect.origin.y = titlebarRect.origin.y + (titlebarRect.size.height - titleSize.height) / 2;
            titleRect.size.height = titleSize.height;
        }
        if (0 < titleRect.size.width)
        {
            [title drawInRect:CGRectOffset(titleRect, 0, -1) withAttributes:titleAttributes];
            [titleAttributes
                setObject:isKeyWindow ?
                    [DKCurrentGraphicsStyle styledWindowFrameTextColor] :
                    [DKCurrentGraphicsStyle styledWindowInactiveFrameTextColor]
                forKey:NSForegroundColorAttributeName];
            [title drawInRect:titleRect withAttributes:titleAttributes];
        }
        [titleAttributes release];
    }
}
static void ToolbarViewDrawRect(NSView *self, SEL _cmd, NSRect rect)
{
    [self __hack__drawRect:rect];
        /* always call the underlying drawRect: to avoid artifacts */
    if (nil == DKCurrentGraphicsStyle)
        return;
    NSView *toolbarView = [self.subviews lastObject];
    CGRect toolbarRect = [toolbarView frame];
    [DKCurrentGraphicsStyle drawWindowFrameWithRect:toolbarRect];
}

@implementation NSWindow (StyledAppearance)
static Class _NSToolbarFullScreenWindowClass;
+ (void)loadStyledAppearance
{
    static BOOL done;
    if (!done)
    {
        [self
            swizzleInstanceMethod:@selector(initWithContentRect:styleMask:backing:defer:)
            withMethod:@selector(__swizzle__initWithContentRect:styleMask:backing:defer:)];
        [self
            swizzleInstanceMethod:@selector(setContentView:)
            withMethod:@selector(__swizzle__setContentView:)];
        done = YES;
    }
}
- (BOOL)viewsShouldAppearStyled
{
    if (nil == DKCurrentGraphicsStyle)
        return NO;
    return 0 != (GraphicsStyleVSAS & GetGraphicsStyle(self));
}
- (id)
    __swizzle__initWithContentRect:(NSRect)contentRect
    styleMask:(NSUInteger)styleMask
    backing:(NSBackingStoreType)bufferingType
    defer:(BOOL)flag
{
    self = [self
        __swizzle__initWithContentRect:contentRect
        styleMask:styleMask
        backing:bufferingType
        defer:flag];
    Class cls = [self class];
    NSString *clsname = NSStringFromClass(cls);
    if ([cls isSubclassOfClass:[NSSavePanel class]] ||
        [clsname hasPrefix:@"FI_"]) /* FinderKit windows */
    {
        /* ignore */
    }
    else if (NSNotFound != [clsname rangeOfString:@"NSToolbarFullScreen"].location)
    {
        if (nil == _NSToolbarFullScreenWindowClass)
            _NSToolbarFullScreenWindowClass = cls;
        SetGraphicsStyle(self, GraphicsStyleVSAS);
    }
    else if (NSNotFound != [clsname rangeOfString:@"NSPopover"].location)
    {
        SetGraphicsStyle(self, GraphicsStyleVSAS);
    }
    else if (NSTitledWindowMask == (styleMask & NSTitledWindowMask))
    {
        if (0 == (styleMask & NSHUDWindowMask))
        {
            NSView *frameView = [self.contentView superview];
            HackInstanceMethod([frameView class], @selector(drawRect:), @selector(__hack__drawRect:),
                (IMP)FrameViewDrawRect);
            SetGraphicsStyle(self, GraphicsStyleFVDR | GraphicsStyleVSAS);
        }
        else
            SetGraphicsStyle(self, GraphicsStyleVSAS);
    }
    return self;
}
- (void)__swizzle__setContentView:(NSView *)view
{
    [self __swizzle__setContentView:view];
    if (nil != _NSToolbarFullScreenWindowClass &&
        [self isMemberOfClass:_NSToolbarFullScreenWindowClass] &&
        NSNotFound != [NSStringFromClass([view class]) rangeOfString:@"NSToolbar"].location)
    {
        HackInstanceMethod([view class], @selector(drawRect:), @selector(__hack__drawRect:),
            (IMP)ToolbarViewDrawRect);
    }
}
@end
