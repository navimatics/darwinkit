/*
 * DarwinKit/mac/NSWindow+AutosaveFrameTopLeftFix.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>

@implementation NSWindow (AutosaveFrameTopLeftFix)
+ (void)loadAutosaveFrameTopLeftFix
{
    static BOOL done;
    if (!done)
    {
        [self
            swizzleInstanceMethod:@selector(setFrameUsingName:)
            withMethod:@selector(__swizzle__setFrameUsingName:)];
        done = YES;
    }
}
- (BOOL)__swizzle__setFrameUsingName:(NSString *)name
{
    if (0 == (self.styleMask & NSResizableWindowMask))
    {
        CGRect frameRect = self.frame;
        CGSize frameSize = frameRect.size;
        if ([self setFrameUsingName:name force:YES])
        {
            frameRect = self.frame;
            frameRect.origin = CGPointMake(
                frameRect.origin.x, frameRect.origin.y + frameRect.size.height - frameSize.height);
            frameRect.size = frameSize;
            [self setFrame:frameRect display:NO animate:NO];
            return YES;
        }
        else
            return NO;
    }
    else
        return [self __swizzle__setFrameUsingName:name];
}
@end
