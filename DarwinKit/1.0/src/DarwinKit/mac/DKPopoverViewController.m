/*
 * DarwinKit/mac/DKPopoverViewController.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>

@interface DKPopoverDetachableWindow : NSPanel
- (id)initWithContentViewController:(NSViewController *)controller;
@end
@implementation DKPopoverDetachableWindow
{
    NSViewController *_contentViewController;
}
- (id)initWithContentViewController:(NSViewController *)controller
{
    self = [super initWithContentRect:controller.view.bounds
        styleMask:NSTitledWindowMask | NSClosableWindowMask | NSUtilityWindowMask
        backing:NSBackingStoreBuffered
        defer:YES];
    if (nil != self)
    {
        [self.contentView addSubview:controller.view];
        _contentViewController = [controller retain];
    }
    return self;
}
- (void)dealloc
{
    [_contentViewController release];
    [super dealloc];
}
@end

@implementation DKPopoverViewController
{
    NSWindow *_detachableWindow;
}
@synthesize behavior = _behavior;
@synthesize popoverDelegate = _popoverDelegate;
@synthesize popover = _popover;
- (void)dealloc
{
    [_detachableWindow release];
    [_popover release];
    [super dealloc];
}
- (void)showInPopoverRelativeToRect:(NSRect)rect
    ofView:(NSView *)view
    preferredEdge:(NSRectEdge)edge
{
    if (nil == _popover)
    {
        _popover = [[NSPopover alloc] init];
        _popover.behavior = _behavior;
        _popover.animates = YES;
        _popover.appearance =
            [[[NSUserDefaults standardUserDefaults] stringForKey:@"DKGraphicsStyle"] isEqualToString:@"Dark" ] ?
                NSPopoverAppearanceHUD :
                NSPopoverAppearanceMinimal;
        _popover.contentViewController = self;
        _popover.delegate = self;
    }
    [_popover showRelativeToRect:rect ofView:view preferredEdge:edge];
}
- (void)close
{
    [_popover close];
}
/* NSPopoverDelegate */
- (void)popoverWillShow:(NSNotification *)notification
{
    if ([_popoverDelegate respondsToSelector:@selector(popoverWillShow:)])
        [_popoverDelegate popoverWillShow:notification];
}
- (void)popoverDidShow:(NSNotification *)notification
{
    if ([_popoverDelegate respondsToSelector:@selector(popoverDidShow:)])
        [_popoverDelegate popoverDidShow:notification];
}
- (BOOL)popoverShouldClose:(NSPopover *)popover
{
    if ([_popoverDelegate respondsToSelector:@selector(popoverShouldClose:)])
        return [_popoverDelegate popoverShouldClose:popover];
    return YES;
}
- (void)popoverWillClose:(NSNotification *)notification
{
    if ([_popoverDelegate respondsToSelector:@selector(popoverWillClose:)])
        [_popoverDelegate popoverWillClose:notification];
}
- (void)popoverDidClose:(NSNotification *)notification
{
    if ([_popoverDelegate respondsToSelector:@selector(popoverDidClose:)])
        [_popoverDelegate popoverDidClose:notification];
    [_popover release];
    _popover = nil;
}
- (NSWindow *)detachableWindowForPopover:(NSPopover *)popover
{
    if (nil == _detachableWindow)
    {
        NSViewController *controller = [self detachableWindowViewController];
        if (nil != controller)
        {
            _detachableWindow = [[DKPopoverDetachableWindow alloc]
                initWithContentViewController:controller];
            [_detachableWindow retain];
                /* retain count == 2 */
            _detachableWindow.releasedWhenClosed = YES;
        }
    }
    return _detachableWindow;
}
- (NSViewController *)detachableWindowViewController
{
    return nil;
}
@end
