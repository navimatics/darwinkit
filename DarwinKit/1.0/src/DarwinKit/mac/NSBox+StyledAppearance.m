/*
 * DarwinKit/mac/NSBox+StyledAppearance.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>
#import <DarwinKit/mac/DKGraphicsStyle.h>

#define RoundedCornerRadius             4

@implementation NSBox (StyledAppearance)
+ (void)loadStyledAppearance
{
    static BOOL done;
    if (!done)
    {
        [self
            swizzleInstanceMethod:@selector(drawRect:)
            withMethod:@selector(__swizzle__drawRect:)];
        done = YES;
    }
}
- (void)__swizzle__drawRect:(NSRect)rect
{
    BOOL defaultDraw = YES;
    if ([self shouldAppearStyled])
    {
        if (NSNoBorder != self.borderType)
        {
            NSBoxType boxType = self.boxType;
            switch (boxType)
            {
            case NSBoxPrimary:
            case NSBoxSecondary:
            case NSBoxOldStyle:
                {
#if 1
                    /*
                     * [self borderRect] appears to be returning incorrect rects
                     * when title is near the top. Use rect computed from contentView instead.
                     */
                    CGRect borderRect = [self.contentView frame];
                    CGSize contentMargins = self.contentViewMargins;
                    borderRect = CGRectInset(borderRect, -contentMargins.width, -contentMargins.height);
#else
                    CGRect borderRect = self.borderRect;
#endif
                    if (NSBoxOldStyle != boxType)
                        [DKCurrentGraphicsStyle
                            drawControlFaceWithRect:borderRect cornerRadius:RoundedCornerRadius
                            isEnabled:YES];
                    else
                        [DKCurrentGraphicsStyle
                            drawControlSimpleFaceWithRect:borderRect
                            isEnabled:YES];
                    id cell = self.titleCell;
                    if ([cell isKindOfClass:[NSTextFieldCell class]] && NSNoTitle != self.titlePosition)
                    {
                        NSTextFieldCell *titleCell = (NSTextFieldCell *)cell;
                        titleCell.textColor = [DKCurrentGraphicsStyle styledControlTextColor];
                        [titleCell drawWithFrame:self.titleRect inView:self];
                    }
                    defaultDraw = NO;
                }
                break;
            case NSBoxSeparator:
                {
                    CGRect bounds = self.bounds;
                    if (bounds.size.width > bounds.size.height)
                    {
                        /* horizontal line */
                        [[DKCurrentGraphicsStyle styledControlColor] setStroke];
                        CGFloat midy = bounds.origin.y + bounds.size.height / 2;
                        [NSBezierPath
                            strokeLineFromPoint:CGPointMake(bounds.origin.x, midy)
                            toPoint:CGPointMake(bounds.origin.x + bounds.size.width, midy)];
                    }
                    else
                    {
                        /* vertical line */
                        [[DKCurrentGraphicsStyle styledControlColor] setStroke];
                        CGFloat midx = bounds.origin.x + bounds.size.width / 2;
                        [NSBezierPath
                            strokeLineFromPoint:CGPointMake(midx, bounds.origin.y)
                            toPoint:CGPointMake(midx, bounds.origin.y + bounds.size.height)];
                    }
                    defaultDraw = NO;
                }
                break;
            }
        }
    }
    if (defaultDraw)
        [self __swizzle__drawRect:rect];
}
@end
