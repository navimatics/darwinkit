/*
 * DarwinKit/mac/NSView+ViewSearching.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>

@implementation NSView (ViewSearching)
- (id)viewWithClass:(Class)cls
{
    if ([self isKindOfClass:cls])
        return self;
    for (NSView *subview in self.subviews)
    {
        NSView *view = [subview viewWithClass:cls];
        if (nil != view)
            return view;
    }
    return nil;
}
- (id)viewWithClass:(Class)cls tag:(int)tag
{
    if ([self isKindOfClass:cls] && self.tag == tag)
        return self;
    for (NSView *subview in self.subviews)
    {
        NSView *view = [subview viewWithClass:cls tag:tag];
        if (nil != view)
            return view;
    }
    return nil;
}
@end
