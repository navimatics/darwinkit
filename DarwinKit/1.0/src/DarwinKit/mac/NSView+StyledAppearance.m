/*
 * DarwinKit/mac/NSView+StyledAppearance.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>

@implementation NSView (StyledAppearance)
+ (void)loadStyledAppearance
{
}
- (BOOL)shouldAppearStyled
{
    return [self.window viewsShouldAppearStyled];
}
@end
