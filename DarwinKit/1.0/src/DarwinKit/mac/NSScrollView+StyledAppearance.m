/*
 * DarwinKit/mac/NSScrollView+StyledAppearance.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>
#import <DarwinKit/mac/DKGraphicsStyle.h>

@implementation NSScrollView (StyledAppearance)
+ (void)loadStyledAppearance
{
    static BOOL done;
    if (!done)
    {
        [self
            swizzleInstanceMethod:@selector(drawRect:)
            withMethod:@selector(__swizzle__drawRect:)];
        done = YES;
    }
}
- (void)__swizzle__drawRect:(NSRect)rect
{
    if ([self shouldAppearStyled])
    {
        switch (self.borderType)
        {
        default:
        case NSNoBorder:
            break;
        case NSLineBorder:
            [DKCurrentGraphicsStyle
                drawControlSimpleFaceWithRect:self.bounds
                hasBackground:NO
                isEnabled:YES];
            break;
        case NSBezelBorder:
        case NSGrooveBorder:
            [DKCurrentGraphicsStyle
                drawControlFaceWithRect:self.bounds
                hasBackground:NO
                isEnabled:YES];
            break;
        }
    }
    else
        [self __swizzle__drawRect:rect];
}
@end
