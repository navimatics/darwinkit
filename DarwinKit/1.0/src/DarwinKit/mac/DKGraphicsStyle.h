/*
 * DarwinKit/mac/DKGraphicsStyle.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_MAC_DKGRAPHICSSTYLE_H_INCLUDED
#define DARWINKIT_MAC_DKGRAPHICSSTYLE_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

@interface DKGraphicsStyle : NSObject
+ (void)registerGraphicsStyleClass:(Class)cls forName:(NSString *)name;
+ (DKGraphicsStyle *)currentStyle;
+ (void)setCurrentStyleWithName:(NSString *)name;
- (NSBundle *)bundle;
- (NSImage *)imageNamed:(NSString *)name;
@end
extern DKGraphicsStyle *DKCurrentGraphicsStyle;

@interface DKGraphicsStyle (StyledControlDrawing)
- (void)drawWindowFrameWithRect:(NSRect)rect;
- (void)drawWindowFrameWithRect:(NSRect)rect cornerRadius:(CGFloat)cornerRadius;
- (void)drawButtonFaceWithRect:(NSRect)rect cornerRadius:(CGFloat)cornerRadius
    isEnabled:(BOOL)isEnabled isPushed:(BOOL)isPushed;
- (void)drawDefaultButtonFaceWithRect:(NSRect)rect cornerRadius:(CGFloat)cornerRadius
    isEnabled:(BOOL)isEnabled isPushed:(BOOL)isPushed;
- (void)drawSegmentedFaceWithRect:(NSRect)rect cornerRadius:(CGFloat)cornerRadius
    segmentWidths:(const CGFloat *)widths selectedSegments:(BOOL *)selected segmentCount:(NSInteger)count
    pushedWhenSelected:(BOOL)pushedWhenSelected
    isEnabled:(BOOL)isEnabled;
- (void)drawStepperFaceWithRect:(NSRect)rect cornerRadius:(CGFloat)cornerRadius
    pushedButton:(NSInteger)pushedButton
    isEnabled:(BOOL)isEnabled;
- (void)drawControlFaceWithRect:(NSRect)rect
    isEnabled:(BOOL)isEnabled;
- (void)drawControlFaceWithRect:(NSRect)rect
    hasBackground:(BOOL)hasBackground
    isEnabled:(BOOL)isEnabled;
- (void)drawControlFaceWithRect:(NSRect)rect
    cornerRadius:(CGFloat)cornerRadius
    isEnabled:(BOOL)isEnabled;
- (void)drawControlSimpleFaceWithRect:(NSRect)rect
    isEnabled:(BOOL)isEnabled;
- (void)drawControlSimpleFaceWithRect:(NSRect)rect
    hasBackground:(BOOL)hasBackground
    isEnabled:(BOOL)isEnabled;
- (void)drawControlFaceBackgroundWithRect:(NSRect)rect;
- (void)drawTableRowFaceWithRect:(NSRect)rect;
- (void)drawControlText:(NSString *)text rect:(NSRect)rect
    font:(NSFont *)font color:(NSColor *)color shadow:(BOOL)shadow
    alignment:(NSTextAlignment)alignment;
- (void)drawControlText:(NSString *)text rect:(NSRect)rect
    controlSize:(NSControlSize)controlSize color:(NSColor *)color shadow:(BOOL)shadow
    alignment:(NSTextAlignment)alignment;
- (void)drawControlText:(NSString *)text rect:(NSRect)rect
    controlSize:(NSControlSize)controlSize color:(NSColor *)color
    alignment:(NSTextAlignment)alignment;
- (void)drawControlImage:(NSImage *)image rect:(NSRect)rect
    imageScaling:(NSImageScaling)imageScaling
    color:(NSColor *)color
    isEnabled:(BOOL)isEnabled;
@end

@interface DKGraphicsStyle (StyledColors)
- (NSColor *)styledControlColor;
- (NSColor *)styledControlTextColor;
- (NSColor *)styledAlternateControlTextColor;
- (NSColor *)styledControlBackgroundColor;
- (NSColor *)styledDisabledControlColor;
- (NSColor *)styledDisabledControlTextColor;
- (NSColor *)styledDisabledAlternateControlTextColor;
- (NSColor *)styledSelectedControlColor;
- (NSColor *)styledSecondarySelectedControlColor;
- (NSColor *)styledAlternateSelectedControlColor;
- (NSColor *)styledDefaultButtonColor;
- (NSColor *)styledDefaultButtonTextColor;
- (NSColor *)styledDisabledDefaultButtonColor;
- (NSColor *)styledTextColor;
- (NSColor *)styledTextBackgroundColor;
- (NSColor *)styledSelectedTextColor;
- (NSColor *)styledSelectedTextBackgroundColor;
- (NSColor *)styledLinkTextColor;
- (NSColor *)styledWindowFrameColor;
- (NSColor *)styledWindowFrameTextColor;
- (NSColor *)styledWindowInactiveFrameTextColor;
- (NSColor *)styledWindowBackgroundColor;
- (NSColor *)styledShadowColor;
- (NSColor *)getStyledColor:(NSColor *)color;
- (NSColor *)getStyledBackgroundColor:(NSColor *)color;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_MAC_DKGRAPHICSSTYLE_H_INCLUDED
