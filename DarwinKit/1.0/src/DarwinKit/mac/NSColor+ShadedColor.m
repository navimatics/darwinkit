/*
 * DarwinKit/mac/NSColor+ShadedColor.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>
#import <DarwinKit/shared/ColorConversions.h>

@implementation NSColor (ShadedColor)
- (NSColor *)shadedColorWithDifference:(CGFloat)diff
{
    NSColorSpace *srgb = [NSColorSpace sRGBColorSpace];
    NSColor *color = [self colorUsingColorSpace:srgb];
    assert(4 == [color numberOfComponents]);
    CGFloat rgba[4], xyz[3], lab[3];
    [color getComponents:rgba];
    srgb_to_ciexyz(rgba, xyz); ciexyz_to_cielab(xyz, lab);
    lab[0] += 100 * diff;
    cielab_to_ciexyz(lab, xyz); ciexyz_to_srgb(xyz, rgba);
    return [NSColor colorWithColorSpace:srgb components:rgba count:4];
}
@end
