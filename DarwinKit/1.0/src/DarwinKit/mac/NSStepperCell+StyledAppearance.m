/*
 * DarwinKit/mac/NSStepperCell+StyledAppearance.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>
#import <DarwinKit/mac/DKGraphicsStyle.h>

#define RoundedCornerRadius             4

@implementation NSStepperCell (StyledAppearance)
typedef struct
{
    NSInteger pushedButton;
} AssociatedData;
static size_t AssociatedDataKey = sizeof(AssociatedData);
+ (void)loadStyledAppearance
{
    static BOOL done;
    if (!done)
    {
        [self
            swizzleInstanceMethod:@selector(drawWithFrame:inView:)
            withMethod:@selector(__swizzle__drawWithFrame:inView:)];
        [self
            swizzleInstanceMethod:@selector(startTrackingAt:inView:)
            withMethod:@selector(__swizzle__startTrackingAt:inView:)];
        [self
            swizzleInstanceMethod:@selector(continueTracking:at:inView:)
            withMethod:@selector(__swizzle__continueTracking:at:inView:)];
        [self
            swizzleInstanceMethod:@selector(stopTracking:at:inView:mouseIsUp:)
            withMethod:@selector(__swizzle__stopTracking:at:inView:mouseIsUp:)];
        done = YES;
    }
}
- (NSRect)__helper__buttonRectFromFrameRect:(NSRect)frameRect
{
    /* insets based on Xcode 4: CocoaPlugin.ibplugin: AppKitInsets.plist: "Little Arrows" */
    struct Insets
    {
        CGFloat b, l, r, t;
    };
    static struct Insets regularInsets = { 2/*3*/, 3, 3, 2 };
    static struct Insets smallInsets = { 1/*2*/, 2, 2, 1 };
    static struct Insets miniInsets = { 0, 2, 2, 0 };
    struct Insets *insets;
    NSControlSize controlSize = self.controlSize;
    switch (controlSize)
    {
    default:
    case NSRegularControlSize:
        insets = &regularInsets;
        break;
    case NSSmallControlSize:
        insets = &smallInsets;
        break;
    case NSMiniControlSize:
        insets = &miniInsets;
        break;
    }
    BOOL isFlipped = [[NSGraphicsContext currentContext] isFlipped];
    return CGRectMake(
        frameRect.origin.x + insets->l,
        frameRect.origin.y + isFlipped ? insets->t : insets->b,
        frameRect.size.width - insets->l - insets->r,
        frameRect.size.height - insets->t - insets->b);
}
- (void)__helper__updatePushedButton:(CGPoint)point inView:(NSView *)controlView
{
    BOOL isFlipped = controlView.isFlipped;
    CGRect rect = controlView.bounds;
    NSInteger pushedButton = 0;
    if (CGRectContainsPoint(CGRectMake(
        rect.origin.x, rect.origin.y + (isFlipped ? 0 : rect.size.height / 2),
        rect.size.width, rect.size.height / 2), point))
        pushedButton = +1;
    else if (CGRectContainsPoint(CGRectMake(
        rect.origin.x, rect.origin.y + (!isFlipped ? 0 : rect.size.height / 2),
        rect.size.width, rect.size.height / 2), point))
        pushedButton = -1;
    AssociatedData *assoc = [self getAssociatedData:&AssociatedDataKey];
    if (assoc->pushedButton != pushedButton)
    {
        assoc->pushedButton = pushedButton;
        [controlView setNeedsDisplay:YES];
    }
}
- (void)__swizzle__drawWithFrame:(NSRect)frameRect inView:(NSView *)controlView
{
    if ([self.controlView shouldAppearStyled])
    {
        AssociatedData *assoc = [self getAssociatedData:&AssociatedDataKey];
        BOOL isFlipped = controlView.isFlipped;
        BOOL isEnabled = self.isEnabled;
        CGRect buttonRect = [self __helper__buttonRectFromFrameRect:frameRect];
        [DKCurrentGraphicsStyle
            drawStepperFaceWithRect:buttonRect
            cornerRadius:RoundedCornerRadius
            pushedButton:assoc->pushedButton
            isEnabled:isEnabled];
        static NSImage *stepperUpImage, *stepperDnImage;
        if (nil == stepperUpImage)
            stepperUpImage = [[DKCurrentGraphicsStyle imageNamed:@"stepperUpTemplate"] retain];
        if (nil == stepperDnImage)
            stepperDnImage = [[DKCurrentGraphicsStyle imageNamed:@"stepperDnTemplate"] retain];
        NSColor *imageColor = isEnabled ?
            [DKCurrentGraphicsStyle styledControlTextColor] :
            [DKCurrentGraphicsStyle styledDisabledControlTextColor];
        buttonRect = CGRectInset(buttonRect, 2, 2);
        [DKCurrentGraphicsStyle
            drawControlImage:stepperUpImage
            rect:CGRectMake(
                buttonRect.origin.x, buttonRect.origin.y + (isFlipped ? 0 : buttonRect.size.height / 2),
                buttonRect.size.width, buttonRect.size.height / 2)
            imageScaling:NSImageScaleProportionallyUpOrDown
            color:imageColor
            isEnabled:isEnabled];
        [DKCurrentGraphicsStyle
            drawControlImage:stepperDnImage
            rect:CGRectMake(
                buttonRect.origin.x, buttonRect.origin.y + (!isFlipped ? 0 : buttonRect.size.height / 2),
                buttonRect.size.width, buttonRect.size.height / 2)
            imageScaling:NSImageScaleProportionallyUpOrDown
            color:imageColor
            isEnabled:isEnabled];
    }
    else
        [self __swizzle__drawWithFrame:frameRect inView:controlView];
}
- (BOOL)__swizzle__startTrackingAt:(NSPoint)startPoint inView:(NSView *)controlView
{
    [self __helper__updatePushedButton:startPoint inView:controlView];
    return [self __swizzle__startTrackingAt:startPoint inView:controlView];
}
- (BOOL)__swizzle__continueTracking:(NSPoint)lastPoint at:(NSPoint)currentPoint inView:(NSView *)controlView
{
    [self __helper__updatePushedButton:currentPoint inView:controlView];
    return [self __swizzle__continueTracking:lastPoint at:currentPoint inView:controlView];
}
- (void)__swizzle__stopTracking:(NSPoint)lastPoint at:(NSPoint)stopPoint inView:(NSView *)controlView mouseIsUp:(BOOL)flag
{
    AssociatedData *assoc = [self getAssociatedData:&AssociatedDataKey];
    if (assoc->pushedButton != 0)
    {
        assoc->pushedButton = 0;
        [controlView setNeedsDisplay:YES];
    }
    [self __swizzle__stopTracking:lastPoint at:stopPoint inView:controlView mouseIsUp:flag];
}
@end
