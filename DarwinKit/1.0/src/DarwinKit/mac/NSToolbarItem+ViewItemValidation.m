/*
 * DarwinKit/mac/NSToolbarItem+ViewItemValidation.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>

@implementation NSToolbarItem (ViewItemValidation)
+ (void)loadViewItemValidation
{
    static BOOL done;
    if (!done)
    {
        [self
            swizzleInstanceMethod:@selector(validate)
            withMethod:@selector(__swizzle__validate)];
        done = YES;
    }
}
- (void)__swizzle__validate
{
    NSView *view = self.view;
    if (nil != view)
    {
        id target = self.target;
        SEL action = self.action;
        if (nil == target)
            target = [NSApp targetForAction:action];
        BOOL enabled = YES;
        if (nil != target)
        {
            if ([target respondsToSelector:@selector(validateToolbarItem:)])
                enabled = [target validateToolbarItem:self];
            else if ([target respondsToSelector:@selector(validateUserInterfaceItem:)])
                enabled = [target validateUserInterfaceItem:self];
        }
        self.enabled = enabled;
    }
    else
        [self __swizzle__validate];
}
@end
