/*
 * DarwinKit/mac/DKGradientGraphicsStyle.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_MAC_DKGRADIENTGRAPHICSSTYLE_H_INCLUDED
#define DARWINKIT_MAC_DKGRADIENTGRAPHICSSTYLE_H_INCLUDED

#import <DarwinKit/mac/DKGraphicsStyle.h>

#ifdef __cplusplus
extern "C" {
#endif

@interface DKGradientGraphicsStyle : DKGraphicsStyle
- (NSGradient *)styledWindowFrameGradientForRect:(NSRect)rect;
- (NSGradient *)styledButtonBezelGradient;
- (NSGradient *)styledButtonGradient;
- (NSGradient *)styledDisabledButtonBezelGradient;
- (NSGradient *)styledDisabledButtonGradient;
- (NSGradient *)styledDefaultButtonBezelGradient;
- (NSGradient *)styledDefaultButtonGradient;
- (NSGradient *)styledAnimatedDefaultButtonGradient;
- (NSGradient *)styledDisabledDefaultButtonBezelGradient;
- (NSGradient *)styledDisabledDefaultButtonGradient;
- (NSGradient *)styledControlBezelGradient;
- (NSGradient *)styledControlGradient;
- (NSGradient *)styledDisabledControlBezelGradient;
- (NSGradient *)styledDisabledControlGradient;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_MAC_DKGRADIENTGRAPHICSSTYLE_H_INCLUDED
