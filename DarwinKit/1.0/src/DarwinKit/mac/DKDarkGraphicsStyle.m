/*
 * DarwinKit/mac/DKDarkGraphicsStyle.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>
#import <DarwinKit/mac/DKDarkGraphicsStyle.h>

static inline NSColor *createNSColor(CGFloat r, CGFloat g, CGFloat b, CGFloat a)
{
    CGFloat components[4] = { r, g, b, a };
    return [[NSColor colorWithColorSpace:[NSColorSpace sRGBColorSpace] components:components count:4]
        retain];
}

@implementation DKDarkGraphicsStyle
{
    NSColor *_styledControlColor;
    NSColor *_styledControlTextColor;
    NSColor *_styledAlternateControlTextColor;
    NSColor *_styledControlBackgroundColor;
    NSColor *_styledDisabledControlColor;
    NSColor *_styledDisabledControlTextColor;
    NSColor *_styledDisabledAlternateControlTextColor;
    NSColor *_styledSelectedControlColor;
    NSColor *_styledSecondarySelectedControlColor;
    NSColor *_styledAlternateSelectedControlColor;
    NSColor *_styledDefaultButtonColor;
    NSColor *_styledDefaultButtonTextColor;
    NSColor *_styledDisabledDefaultButtonColor;
    NSColor *_styledTextColor;
    NSColor *_styledTextBackgroundColor;
    NSColor *_styledSelectedTextColor;
    NSColor *_styledSelectedTextBackgroundColor;
    NSColor *_styledLinkTextColor;
    NSColor *_styledWindowFrameColor;
    NSColor *_styledWindowFrameTextColor;
    NSColor *_styledWindowInactiveFrameTextColor;
    NSColor *_styledWindowBackgroundColor;
    NSColor *_styledShadowColor;
    NSDictionary *_styledColors;
}
+ (void)load
{
    [self registerGraphicsStyleClass:self forName:@"Dark"];
}
- (id)init
{
    /* [super init] will call us back for various colors; MUST do this first */
    _styledControlColor =                   createNSColor(0.25, 0.25, 0.25, 1.0);
    _styledControlTextColor =               createNSColor(0.75, 0.75, 0.75, 1.0);
    _styledAlternateControlTextColor =      createNSColor(0.0, 0.5, 1.0, 1.0);
    _styledControlBackgroundColor =         createNSColor(0.1, 0.1, 0.1, 1.0);
    _styledDisabledControlColor =           createNSColor(0.15, 0.15, 0.15, 1.0);
    _styledDisabledControlTextColor =       createNSColor(0.5, 0.5, 0.5, 1.0);
    _styledDisabledAlternateControlTextColor = [[_styledAlternateControlTextColor
        shadedColorWithDifference:-0.20] retain];
    _styledSelectedControlColor =           [[NSColor selectedControlColor] retain];
    _styledSecondarySelectedControlColor =  [[[createNSColor(0.16, 0.5, 0.85, 1.0) autorelease]
        shadedColorWithDifference:-0.20] retain];
    _styledAlternateSelectedControlColor =  createNSColor(0.40, 0.40, 0.40, 1.0);
    _styledDefaultButtonColor =             createNSColor(0.16, 0.5, 0.85, 1.0);
    _styledDefaultButtonTextColor =         createNSColor(1.0, 1.0, 1.0, 1.0);
    _styledDisabledDefaultButtonColor =     [[_styledDefaultButtonColor
        shadedColorWithDifference:-0.20] retain];
    _styledTextColor =                      createNSColor(1.0, 1.0, 1.0, 1.0);
    _styledTextBackgroundColor =            [[NSColor clearColor] retain];
    _styledSelectedTextColor =              createNSColor(0.0, 0.0, 0.0, 1.0);
    _styledSelectedTextBackgroundColor =    [[NSColor selectedTextBackgroundColor] retain];
    _styledLinkTextColor =                  createNSColor(0.5, 0.5, 1.0, 1.0);
    _styledWindowFrameColor =               createNSColor(0.25, 0.25, 0.25, 1.0);
    _styledWindowFrameTextColor =           createNSColor(1.0, 1.0, 1.0, 1.0);
    _styledWindowInactiveFrameTextColor =   createNSColor(0.75, 0.75, 0.75, 1.0);
    _styledWindowBackgroundColor =          createNSColor(0.1, 0.1, 0.1, 1.0);
    _styledShadowColor =                    createNSColor(0.0, 0.0, 0.0, 1.0);
    _styledColors = [[NSDictionary alloc] initWithObjectsAndKeys:
        _styledControlColor, [NSColor controlColor],
        _styledControlTextColor, [NSColor controlTextColor],
        _styledDisabledControlTextColor, [NSColor disabledControlTextColor],
        _styledControlBackgroundColor, [NSColor controlBackgroundColor],
        _styledTextColor, [NSColor textColor],
        _styledTextBackgroundColor, [NSColor textBackgroundColor],
        _styledSelectedTextColor, [NSColor selectedTextColor],
        _styledSelectedTextBackgroundColor, [NSColor selectedTextBackgroundColor],
        nil];
    return [super init];
}
- (void)dealloc
{
    [_styledColors release];
    [_styledShadowColor release];
    [_styledWindowBackgroundColor release];
    [_styledWindowInactiveFrameTextColor release];
    [_styledWindowFrameTextColor release];
    [_styledWindowFrameColor release];
    [_styledLinkTextColor release];
    [_styledSelectedTextBackgroundColor release];
    [_styledSelectedTextColor release];
    [_styledTextBackgroundColor release];
    [_styledTextColor release];
    [_styledDisabledDefaultButtonColor release];
    [_styledDefaultButtonTextColor release];
    [_styledDefaultButtonColor release];
    [_styledAlternateSelectedControlColor release];
    [_styledSecondarySelectedControlColor release];
    [_styledSelectedControlColor release];
    [_styledDisabledAlternateControlTextColor release];
    [_styledDisabledControlTextColor release];
    [_styledDisabledControlColor release];
    [_styledControlBackgroundColor release];
    [_styledAlternateControlTextColor release];
    [_styledControlTextColor release];
    [_styledControlColor release];
    [super dealloc];
}
- (NSColor *)styledControlColor
{
    return _styledControlColor;
}
- (NSColor *)styledControlTextColor
{
    return _styledControlTextColor;
}
- (NSColor *)styledAlternateControlTextColor
{
    return _styledAlternateControlTextColor;
}
- (NSColor *)styledControlBackgroundColor
{
    return _styledControlBackgroundColor;
}
- (NSColor *)styledDisabledControlColor
{
    return _styledDisabledControlColor;
}
- (NSColor *)styledDisabledControlTextColor
{
    return _styledDisabledControlTextColor;
}
- (NSColor *)styledDisabledAlternateControlTextColor
{
    return _styledDisabledAlternateControlTextColor;
}
- (NSColor *)styledSelectedControlColor
{
    return _styledSelectedControlColor;
}
- (NSColor *)styledSecondarySelectedControlColor
{
    return _styledSecondarySelectedControlColor;
}
- (NSColor *)styledAlternateSelectedControlColor
{
    return _styledAlternateSelectedControlColor;
}
- (NSColor *)styledDefaultButtonColor
{
    return _styledDefaultButtonColor;
}
- (NSColor *)styledDefaultButtonTextColor
{
    return _styledDefaultButtonTextColor;
}
- (NSColor *)styledDisabledDefaultButtonColor
{
    return _styledDisabledDefaultButtonColor;
}
- (NSColor *)styledTextColor
{
    return _styledTextColor;
}
- (NSColor *)styledTextBackgroundColor
{
    return _styledTextBackgroundColor;
}
- (NSColor *)styledSelectedTextColor
{
    return _styledSelectedTextColor;
}
- (NSColor *)styledSelectedTextBackgroundColor
{
    return _styledSelectedTextBackgroundColor;
}
- (NSColor *)styledLinkTextColor
{
    return _styledLinkTextColor;
}
- (NSColor *)styledWindowFrameColor
{
    return _styledWindowFrameColor;
}
- (NSColor *)styledWindowFrameTextColor
{
    return _styledWindowFrameTextColor;
}
- (NSColor *)styledWindowInactiveFrameTextColor
{
    return _styledWindowInactiveFrameTextColor;
}
- (NSColor *)styledWindowBackgroundColor
{
    return _styledWindowBackgroundColor;
}
- (NSColor *)styledShadowColor
{
    return _styledShadowColor;
}
- (NSColor *)getStyledColor:(NSColor *)color
{
    NSColor *result = [_styledColors objectForKey:color];
    if (nil == result)
        result = color;
    return result;
}
- (NSColor *)getStyledBackgroundColor:(NSColor *)color
{
    NSColor *result = [_styledColors objectForKey:color];
    if (nil == result)
    {
        if (color == [NSColor whiteColor])
            result = _styledControlBackgroundColor;
        else
            result = color;
    }
    return result;
}
@end
