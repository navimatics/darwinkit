/*
 * DarwinKit/mac/DKChangeableContentWindowController.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>

@implementation DKChangeableContentWindowController
- (void)windowDidLoad
{
    [super windowDidLoad];
    NSString *ident = nil;
    NSString *autosaveName = [self identifierAutosaveName];
    if (nil != autosaveName)
        ident = [[NSUserDefaults standardUserDefaults] objectForKey:autosaveName];
    if (nil == ident)
        ident = [self defaultIdentifier];
    if (nil != ident)
    {
        [self.window.toolbar setSelectedItemIdentifier:ident];
        [self setContentView:[[self viewsDictionary] objectForKey:ident]];
    }
}
- (void)setContentView:(NSView *)view
{
    NSWindow *window = self.window;
    CGRect frameRect = window.frame;
    CGSize frameSize = nil != view ?
        [window frameRectForContentRect:view.bounds].size :
        frameRect.size;
    frameRect.origin = CGPointMake(
        frameRect.origin.x, frameRect.origin.y + frameRect.size.height - frameSize.height);
    frameRect.size = frameSize;
    window.contentView = nil;
    [window setFrame:frameRect display:YES animate:window.isVisible];
    window.contentView = view;
}
- (void)setSelectedContentViewWithIdentifier:(NSString *)ident
{
    NSView *view = [[self viewsDictionary] objectForKey:ident];
    if (![self contentViewShouldChangeWithView:view])
        return;
    [self.window.toolbar setSelectedItemIdentifier:ident];
    [self setContentView:view];
    NSString *autosaveName = [self identifierAutosaveName];
    if (nil != autosaveName)
        [[NSUserDefaults standardUserDefaults] setObject:ident forKey:autosaveName];
}
- (BOOL)contentViewShouldChangeWithView:(NSView *)view
{
    return YES;
}
- (void)changeContentViewFromInterfaceItemWithIdentifier:(NSString *)ident
{
    NSView *view = [[self viewsDictionary] objectForKey:ident];
    if (![self contentViewShouldChangeWithView:view])
        return;
    [self setContentView:view];
    NSString *autosaveName = [self identifierAutosaveName];
    if (nil != autosaveName)
        [[NSUserDefaults standardUserDefaults] setObject:ident forKey:autosaveName];
}
- (IBAction)changeContentViewFromToolbarItem:(id)sender
{
    [self changeContentViewFromInterfaceItemWithIdentifier:[sender itemIdentifier]];
}
- (IBAction)changeContentViewFromButton:(id)sender
{
    switch ([sender state])
    {
    case NSMixedState:
        [self changeContentViewFromInterfaceItemWithIdentifier:@"NSMixedState"];
        break;
    case NSOnState:
        [self changeContentViewFromInterfaceItemWithIdentifier:@"NSOnState"];
        break;
    case NSOffState:
        [self changeContentViewFromInterfaceItemWithIdentifier:@"NSOffState"];
        break;
    }
}
- (NSDictionary *)viewsDictionary
{
    return nil;
}
- (NSString *)defaultIdentifier
{
    return nil;
}
- (NSString *)identifierAutosaveName
{
    return nil;
}
@end
