/*
 * DarwinKit/mac/NSApplication+StandardAboutPanel.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>

#define ApplicationIconControl          'I'
#define ApplicationNameControl          'N'
#define ApplicationVersionControl       'V'
#define CopyrightControl                'C'
#define CompanyLogoControl              'L'
#define InformationButtonControl        'B'

@interface DKApplicationStandardAboutPanelController : NSWindowController
@end
@implementation DKApplicationStandardAboutPanelController
{
    NSDictionary *_options;
}
- (id)initWithOptions:(NSDictionary *)options
{
    self = [super initWithWindowNibName:@"DKApplicationStandardAboutPanel"];
    if (nil != self)
        _options = [options retain];
    return self;
}
- (void)dealloc
{
    [_options release];
    [super dealloc];
}
- (void)windowDidLoad
{
    /* based on Technical Note TN2179 */
    [super windowDidLoad];
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *appIcon = [_options objectForKey:@"ApplicationIcon"];
    if (nil == appIcon)
    {
        appIcon = [bundle objectForInfoDictionaryKey:@"CFBundleIconFile"];
        if (nil == appIcon)
            appIcon = @"NSApplicationIcon";
    }
    NSString *appName = [_options objectForKey:@"ApplicationName"];
    if (nil == appName)
    {
        appName = [bundle objectForInfoDictionaryKey:@"CFBundleName"];
        if (nil == appName)
            appName = [[NSProcessInfo processInfo] processName];
    }
    NSString *appBuild = [_options objectForKey:@"Version"];
    if (nil == appBuild)
    {
        appBuild = [bundle objectForInfoDictionaryKey:@"MyBuildRevision"];
            /* diverge from TN2179 -- use our own version control revision */
        if (nil == appBuild)
            appBuild = [bundle objectForInfoDictionaryKey:@"CFBundleVersion"];
    }
    NSString *appVersion = [_options objectForKey:@"ApplicationVersion"];
    if (nil == appVersion)
    {
        appVersion = [bundle objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
        if (nil == appVersion)
            appVersion = appBuild;
    }
    if ([appVersion isEqualToString:appBuild])
        appBuild = nil;
    if (nil != appVersion &&
        0 == [appVersion rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]].location)
        appVersion = [@"Version " stringByAppendingString:appVersion];
    if (nil != appVersion && nil != appBuild)
        appVersion = [NSString stringWithFormat:@"%@ (%@)", appVersion, appBuild];
    NSString *copyright = [_options objectForKey:@"Copyright"];
    if (nil == copyright)
        copyright = [bundle objectForInfoDictionaryKey:@"NSHumanReadableCopyright"];
    NSString *companyLogo = [_options objectForKey:@"CompanyLogo"];
    if (nil == companyLogo)
        companyLogo = @"CompanyLogo";
    BOOL hasCredits = [_options objectForKey:@"Credits"] ||
        [bundle URLForResource:@"Credits" withExtension:@"html"] ||
        [bundle URLForResource:@"Credits" withExtension:@"rtf"] ||
        [bundle URLForResource:@"Credits" withExtension:@"rtfd"] ||
        [bundle URLForResource:@"Credits" withExtension:@"pdf"];
    NSView *contentView = self.window.contentView;
    [[contentView viewWithTag:ApplicationIconControl] setImage:[NSImage imageNamed:appIcon]];
    [[contentView viewWithTag:ApplicationNameControl] setStringValue:appName];
    [[contentView viewWithTag:ApplicationVersionControl] setStringValue:appVersion];
    [[contentView viewWithTag:CopyrightControl] setStringValue:copyright];
    [[contentView viewWithTag:CompanyLogoControl] setImage:[NSImage imageNamed:companyLogo]];
    [[contentView viewWithTag:InformationButtonControl] setHidden:!hasCredits];
}
- (void)windowWillClose:(NSNotification *)notification
{
    [self autorelease];
        /* balance the retain count from the original init */
}
- (IBAction)informationButtonAction:(id)sender
{
    NSAttributedString *credits = [_options objectForKey:@"Credits"];
    if (nil != credits)
    {
        NSAlert *alert = [NSAlert
            alertWithMessageText:@""
            defaultButton:@"OK"
            alternateButton:nil
            otherButton:nil
            informativeTextWithFormat:@"%@", [credits string]];
        [alert
            beginSheetModalForWindow:self.window
            modalDelegate:nil
            didEndSelector:nil
            contextInfo:nil];
    }
    else
    {
        NSBundle *bundle = [NSBundle mainBundle];
        NSURL *url;
        if ((url = [bundle URLForResource:@"Credits" withExtension:@"html"]) ||
            (url = [bundle URLForResource:@"Credits" withExtension:@"rtf"]) ||
            (url = [bundle URLForResource:@"Credits" withExtension:@"rtfd"]) ||
            (url = [bundle URLForResource:@"Credits" withExtension:@"pdf"]))
            [[NSWorkspace sharedWorkspace] openURL:url];
    }
}
@end

@implementation NSApplication (StandardAboutPanel)
+ (void)loadStandardAboutPanel
{
    static BOOL done;
    if (!done)
    {
        [self
            swizzleInstanceMethod:@selector(orderFrontStandardAboutPanel:)
            withMethod:@selector(__swizzle__orderFrontStandardAboutPanel:)];
        [self
            swizzleInstanceMethod:@selector(orderFrontStandardAboutPanelWithOptions:)
            withMethod:@selector(__swizzle__orderFrontStandardAboutPanelWithOptions:)];
        done = YES;
    }
}
- (void)__swizzle__orderFrontStandardAboutPanel:(id)sender
{
    [self orderFrontStandardAboutPanelWithOptions:nil];
}
- (void)__swizzle__orderFrontStandardAboutPanelWithOptions:(NSDictionary *)options
{
    NSWindowController *controller =
        [[DKApplicationStandardAboutPanelController alloc] initWithOptions:options];
        /* released when closed */
    [controller showWindow:self];
}
@end
