/*
 * DarwinKit/mac/DKActionMenuButton.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>

@implementation DKActionMenuButton
- (void)awakeFromNib
{
    NSMenuItem *item = [[NSMenuItem alloc] init];
    item.title = self.alternateTitle;
    item.image = self.alternateImage;
    self.alternateTitle = nil;
    self.alternateImage = nil;
    [self.cell setUsesItemFromMenu:NO];
    [self.cell setMenuItem:item];
    [item release];
}
@end
