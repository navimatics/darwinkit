/*
 * DarwinKit/mac/NSApplication+StyledAppearance.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>
#import <DarwinKit/mac/DKGraphicsStyle.h>

@implementation NSApplication (StyledAppearance)
static NSString *_name;
+ (void)loadStyledAppearance
{
    static BOOL done;
    if (!done)
    {
        _name = [[[NSUserDefaults standardUserDefaults] stringForKey:@"DKGraphicsStyle"] copy];
        if (nil != _name)
            [DKGraphicsStyle setCurrentStyleWithName:_name];
        [NSBox loadStyledAppearance];
        [NSButtonCell loadStyledAppearance];
        [NSClipView loadStyledAppearance];
        [NSImageCell loadStyledAppearance];
        [NSPopUpButtonCell loadStyledAppearance];
        [NSScrollView loadStyledAppearance];
        [NSSegmentedCell loadStyledAppearance];
        [NSStepperCell loadStyledAppearance];
        [NSTableRowView loadStyledAppearance];
        [NSTableView loadStyledAppearance];
        [NSTextFieldCell loadStyledAppearance];
        [NSTextView loadStyledAppearance];
        [NSView loadStyledAppearance];
        [NSWindow loadStyledAppearance];
        done = YES;
    }
}
+ (NSString *)styledAppearance
{
    return _name;
}
+ (void)setStyledAppearance:(NSString *)name
{
    if (nil == name)
        name = @"";
    if (![_name isEqualToString:name])
    {
        [_name release];
        _name = [name copy];
        [[NSUserDefaults standardUserDefaults] setObject:_name forKey:@"DKGraphicsStyle"];
        [DKGraphicsStyle setCurrentStyleWithName:_name];
        for (NSWindow *window in [self sharedApplication].windows)
            [window display];
    }
}
@end
