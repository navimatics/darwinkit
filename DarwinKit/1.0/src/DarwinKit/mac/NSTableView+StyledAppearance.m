/*
 * DarwinKit/mac/NSTableView+StyledAppearance.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>
#import <DarwinKit/mac/DKGraphicsStyle.h>

@implementation NSTableView (StyledAppearance)
+ (void)loadStyledAppearance
{
    static BOOL done;
    if (!done)
    {
        [self
            swizzleInstanceMethod:@selector(backgroundColor)
            withMethod:@selector(__swizzle__backgroundColor)];
        done = YES;
    }
}
- (NSColor *)__swizzle__backgroundColor
{
    if ([self shouldAppearStyled])
        return [DKCurrentGraphicsStyle getStyledBackgroundColor:[self __swizzle__backgroundColor]];
    else
        return [self __swizzle__backgroundColor];
}
@end
