/*
 * DarwinKit/mac/NSSegmentedCell+StyledAppearance.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>
#import <DarwinKit/mac/DKGraphicsStyle.h>

/* define SHOW_DEBUG_VISUAL_AIDS when debugging a visual issue */
//#define SHOW_DEBUG_VISUAL_AIDS

#define RoundedCornerRadius             4
#define SmallRoundedCornerRadius        1

@implementation NSSegmentedCell (StyledAppearance)
+ (void)loadStyledAppearance
{
    static BOOL done;
    if (!done)
    {
        [self
            swizzleInstanceMethod:@selector(drawWithFrame:inView:)
            withMethod:@selector(__swizzle__drawWithFrame:inView:)];
        [self
            swizzleInstanceMethod:@selector(drawSegment:inFrame:withView:)
            withMethod:@selector(__swizzle__drawSegment:inFrame:withView:)];
        done = YES;
    }
}
static inline NSSegmentStyle getSegmentStyle(NSSegmentedCell *cell)
{
    NSSegmentStyle segmentStyle = cell.segmentStyle;
    if (NSSegmentStyleAutomatic == segmentStyle)
        segmentStyle = NSSegmentStyleRounded;
    return segmentStyle;
}
- (NSRect)__helper__segmentedRectFromFrameRect:(NSRect)frameRect
{
    /* insets based on Xcode 4: CocoaPlugin.ibplugin: AppKitInsets.plist */
    struct Insets
    {
        CGFloat b, l, r, t;
    };
    static struct Insets regularInsets[] =
    {
        { 0, 0, 0, 0 },                 /* NSSegmentStyleAutomatic      = 0, */
        { 2/*3*/, 2, 2, 1 },            /* NSSegmentStyleRounded        = 1, */
        { 2, 0, 0, 1 },                 /* NSSegmentStyleTexturedRounded= 2, */
        { 1/*2*/, 1, 1, 0/*1*/ },       /* NSSegmentStyleRoundRect      = 3, */
        { 2, 0, 0, 1 },                 /* NSSegmentStyleTexturedSquare = 4, */
        { 1, 0, 0, 0 },                 /* NSSegmentStyleCapsule        = 5, */
        { 1, 0, 0, 1 },                 /* NSSegmentStyleSmallSquare    = 6, */
    };
    static struct Insets smallInsets[] =
    {
        { 0, 0, 0, 0 },                 /* NSSegmentStyleAutomatic      = 0, */
        { 2/*3*/, 2, 2, 0 },            /* NSSegmentStyleRounded        = 1, */
        { 1, 0, 0, 0 },                 /* NSSegmentStyleTexturedRounded= 2, */
        { 1/*2*/, 1, 1, 0/*1*/ },       /* NSSegmentStyleRoundRect      = 3, */
        { 1, 0, 0, 0 },                 /* NSSegmentStyleTexturedSquare = 4, */
        { 2, 0, 0, 1 },                 /* NSSegmentStyleCapsule        = 5, */
        { 1, 0, 0, 1 },                 /* NSSegmentStyleSmallSquare    = 6, */
    };
    static struct Insets miniInsets[] =
    {
        { 0, 0, 0, 0 },                 /* NSSegmentStyleAutomatic      = 0, */
        { 0/*1*/, 1, 1, 0 },            /* NSSegmentStyleRounded        = 1, */
        { 1, 0, 0, 1 },                 /* NSSegmentStyleTexturedRounded= 2, */
        { 2, 1, 1, 2 },                 /* NSSegmentStyleRoundRect      = 3, */
        { 1, 0, 0, 1 },                 /* NSSegmentStyleTexturedSquare = 4, */
        { 2, 0/*1*/, 0, 2 },            /* NSSegmentStyleCapsule        = 5, */
        { 1, 0, 0, 1 },                 /* NSSegmentStyleSmallSquare    = 6, */
    };
    struct Insets *insets;
    NSSegmentStyle segmentStyle = getSegmentStyle(self);
    NSControlSize controlSize = self.controlSize;
    if (segmentStyle < NSSegmentStyleAutomatic)
        segmentStyle = NSSegmentStyleAutomatic;
    else if (segmentStyle > NSSegmentStyleSmallSquare)
        segmentStyle = NSSegmentStyleAutomatic;
    switch (controlSize)
    {
    default:
    case NSRegularControlSize:
        insets = regularInsets + segmentStyle;
        break;
    case NSSmallControlSize:
        insets = smallInsets + segmentStyle;
        break;
    case NSMiniControlSize:
        insets = miniInsets + segmentStyle;
        break;
    }
    BOOL isFlipped = [[NSGraphicsContext currentContext] isFlipped];
    return CGRectMake(
        frameRect.origin.x + insets->l,
        frameRect.origin.y + isFlipped ? insets->t : insets->b,
        frameRect.size.width - insets->l - insets->r,
        frameRect.size.height - insets->t - insets->b);
}
- (void)__helper__computeSegmentMetricsForFrameRect:(NSRect)rect
    widths:(CGFloat *)widths count:(NSInteger)count
{
    CGFloat totalWidth = 0;
    NSInteger zeroWidthCells = 0;
    for (NSInteger i = 0; count > i; i++)
    {
        widths[i] = [self widthForSegment:i];
        if (0 == widths[i])
            zeroWidthCells++;
        else
            totalWidth += widths[i];
    }
    if (0 < zeroWidthCells)
    {
        totalWidth = rect.size.width - totalWidth;
        for (NSInteger i = 0; count > i; i++)
            if (0 == widths[i])
                widths[i] = totalWidth / zeroWidthCells;
    }
    else
    {
        if (2 <= count)
        {
            CGFloat remWidth = rect.size.width - totalWidth;
            widths[0] += remWidth / 2;
            widths[count - 1] += remWidth / 2;
        }
        else if (1 <= count)
        {
            CGFloat remWidth = rect.size.width - totalWidth;
            widths[0] += remWidth;
        }
    }
}
- (void)__swizzle__drawWithFrame:(NSRect)frameRect inView:(NSView *)controlView
{
    if ([self.controlView shouldAppearStyled])
    {
        CGRect segmentedRect = [self __helper__segmentedRectFromFrameRect:frameRect];
        NSSegmentStyle segmentStyle = getSegmentStyle(self);
        BOOL isEnabled = self.isEnabled;
        NSInteger segmentCount = self.segmentCount;
        CGFloat segmentWidths[segmentCount];
        BOOL selectedSegments[segmentCount];
        [self
            __helper__computeSegmentMetricsForFrameRect:segmentedRect
            widths:segmentWidths
            count:segmentCount];
        if (NSSegmentSwitchTrackingSelectAny == self.trackingMode)
            for (NSInteger i = 0; segmentCount > i; i++)
                selectedSegments[i] = [self isSelectedForSegment:i];
        else
        {
            NSInteger selectedSegment = self.selectedSegment;
            for (NSInteger i = 0; segmentCount > i; i++)
                selectedSegments[i] = i == selectedSegment;
        }
        switch (segmentStyle)
        {
        default:
        case NSSegmentStyleRounded:
            [DKCurrentGraphicsStyle
                drawSegmentedFaceWithRect:segmentedRect
                cornerRadius:RoundedCornerRadius
                segmentWidths:segmentWidths
                selectedSegments:selectedSegments
                segmentCount:segmentCount
                pushedWhenSelected:NO
                isEnabled:isEnabled];
            break;
        case NSSegmentStyleTexturedRounded:
            [DKCurrentGraphicsStyle
                drawSegmentedFaceWithRect:segmentedRect
                cornerRadius:RoundedCornerRadius
                segmentWidths:segmentWidths
                selectedSegments:selectedSegments
                segmentCount:segmentCount
                pushedWhenSelected:YES
                isEnabled:isEnabled];
            break;
        case NSSegmentStyleRoundRect:
            [DKCurrentGraphicsStyle
                drawSegmentedFaceWithRect:segmentedRect
                cornerRadius:segmentedRect.size.height / 2
                segmentWidths:segmentWidths
                selectedSegments:selectedSegments
                segmentCount:segmentCount
                pushedWhenSelected:YES
                isEnabled:isEnabled];
            break;
        case NSSegmentStyleTexturedSquare:
            [DKCurrentGraphicsStyle
                drawSegmentedFaceWithRect:segmentedRect
                cornerRadius:RoundedCornerRadius
                segmentWidths:segmentWidths
                selectedSegments:selectedSegments
                segmentCount:segmentCount
                pushedWhenSelected:YES
                isEnabled:isEnabled];
            break;
        case NSSegmentStyleCapsule:
            [DKCurrentGraphicsStyle
                drawSegmentedFaceWithRect:segmentedRect
                cornerRadius:RoundedCornerRadius
                segmentWidths:segmentWidths
                selectedSegments:selectedSegments
                segmentCount:segmentCount
                pushedWhenSelected:YES
                isEnabled:isEnabled];
            break;
        case NSSegmentStyleSmallSquare:
            [DKCurrentGraphicsStyle
                drawSegmentedFaceWithRect:segmentedRect
                cornerRadius:SmallRoundedCornerRadius
                segmentWidths:segmentWidths
                selectedSegments:selectedSegments
                segmentCount:segmentCount
                pushedWhenSelected:YES
                isEnabled:isEnabled];
            break;
        }
        CGFloat totalWidth = 0;
        for (NSInteger i = 0; segmentCount > i; i++)
        {
            [self
                drawSegment:i
                inFrame:CGRectMake(
                    segmentedRect.origin.x + totalWidth + 1,
                    segmentedRect.origin.y + 1,
                    segmentWidths[i] - 2,
                    segmentedRect.size.height - 2)
                withView:controlView];
            totalWidth += segmentWidths[i];
        }
    }
    else
        [self __swizzle__drawWithFrame:frameRect inView:controlView];
#if defined(SHOW_DEBUG_VISUAL_AIDS)
    [[NSColor redColor] set];
    NSFrameRect(frameRect);
#endif
}
- (void)__swizzle__drawSegment:(NSInteger)segment inFrame:(NSRect)frameRect withView:(NSView *)controlView
{
    if ([self.controlView shouldAppearStyled])
    {
        BOOL isEnabled = self.isEnabled && [self isEnabledForSegment:segment];
        NSString *label = [self labelForSegment:segment];
        NSImage *image = [self imageForSegment:segment];
        NSColor *foregroundColor = ![self isSelectedForSegment:segment] ?
            (isEnabled ?
                [DKCurrentGraphicsStyle styledControlTextColor] :
                [DKCurrentGraphicsStyle styledDisabledControlTextColor]) :
            (isEnabled ?
                [DKCurrentGraphicsStyle styledDefaultButtonTextColor] :
                [DKCurrentGraphicsStyle styledDisabledControlTextColor]);
        if (0 < [label length] && nil != image)
        {
            CGRect imageRect = CGRectMake(frameRect.origin.x, frameRect.origin.y,
                frameRect.size.height/* imageRect is square */, frameRect.size.height);
            CGRect titleRect = CGRectMake(frameRect.origin.x + frameRect.size.height, frameRect.origin.y,
                frameRect.size.width - frameRect.size.height, frameRect.size.height);
            [DKCurrentGraphicsStyle
                drawControlImage:image
                rect:imageRect
                imageScaling:[self imageScalingForSegment:segment]
                color:foregroundColor
                isEnabled:isEnabled];
            [DKCurrentGraphicsStyle
                drawControlText:label
                rect:titleRect
                controlSize:self.controlSize
                color:foregroundColor
                alignment:NSCenterTextAlignment];
        }
        else if (0 < [label length])
        {
            [DKCurrentGraphicsStyle
                drawControlText:label
                rect:frameRect
                controlSize:self.controlSize
                color:foregroundColor
                alignment:NSCenterTextAlignment];
        }
        else if (nil != image)
        {
            [DKCurrentGraphicsStyle
                drawControlImage:image
                rect:frameRect
                imageScaling:[self imageScalingForSegment:segment]
                color:foregroundColor
                isEnabled:isEnabled];
        }
    }
    else
        [self __swizzle__drawSegment:segment inFrame:frameRect withView:controlView];
#if defined(SHOW_DEBUG_VISUAL_AIDS)
    [[NSColor yellowColor] set];
    NSFrameRect(frameRect);
#endif
}
@end
