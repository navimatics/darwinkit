/*
 * DarwinKit/mac/NSTextFieldCell+StyledAppearance.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>
#import <DarwinKit/mac/DKGraphicsStyle.h>

@implementation NSTextFieldCell (StyledAppearance)
+ (void)loadStyledAppearance
{
    static BOOL done;
    if (!done)
    {
        [self
            swizzleInstanceMethod:@selector(drawWithFrame:inView:)
            withMethod:@selector(__swizzle__drawWithFrame:inView:)];
        [self
            swizzleInstanceMethod:@selector(setUpFieldEditorAttributes:)
            withMethod:@selector(__swizzle__setUpFieldEditorAttributes:)];
        [self
            swizzleInstanceMethod:@selector(textColor)
            withMethod:@selector(__swizzle__textColor)];
        [self
            swizzleInstanceMethod:@selector(backgroundColor)
            withMethod:@selector(__swizzle__backgroundColor)];
        done = YES;
    }
}
- (void)__swizzle__drawWithFrame:(NSRect)frame inView:(NSView *)controlView
{
    if ([self.controlView shouldAppearStyled])
    {
        if (self.isBezeled)
        {
            switch (self.bezelStyle)
            {
            default:
            case NSTextFieldSquareBezel:
                [DKCurrentGraphicsStyle
                    drawControlFaceWithRect:frame
                    isEnabled:self.isEnabled];
                break;
            case NSTextFieldRoundedBezel:
                [DKCurrentGraphicsStyle
                    drawControlFaceWithRect:frame
                    cornerRadius:frame.size.height / 2
                    isEnabled:self.isEnabled];
                break;
            }
        }
        else if (self.isBordered)
            [DKCurrentGraphicsStyle
                drawControlSimpleFaceWithRect:frame
                isEnabled:self.isEnabled];
        [self drawInteriorWithFrame:frame inView:controlView];
    }
    else
        [self __swizzle__drawWithFrame:frame inView:controlView];
}
- (NSText *)__swizzle__setUpFieldEditorAttributes:(NSText *)textObj
{
    textObj = [self __swizzle__setUpFieldEditorAttributes:textObj];
    if ([self.controlView shouldAppearStyled])
    {
        if ([textObj isKindOfClass:[NSTextView class]])
        {
            NSColor *foregroundColor = self.textColor;
            NSColor *backgroundColor = self.backgroundColor;
            [textObj setTextColor:foregroundColor];
            [(id)textObj setInsertionPointColor:foregroundColor];
            [textObj setBackgroundColor:backgroundColor];
        }
    }
    return textObj;
}
- (NSColor *)__swizzle__textColor
{
    if ([self.controlView shouldAppearStyled])
        return [DKCurrentGraphicsStyle getStyledColor:[self __swizzle__textColor]];
    else
        return [self __swizzle__textColor];
}
- (NSColor *)__swizzle__backgroundColor
{
    if ([self.controlView shouldAppearStyled])
        return [DKCurrentGraphicsStyle getStyledBackgroundColor:[self __swizzle__backgroundColor]];
    else
        return [self __swizzle__backgroundColor];
}
@end
