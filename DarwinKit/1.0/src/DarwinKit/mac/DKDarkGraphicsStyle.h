/*
 * DarwinKit/mac/DKDarkGraphicsStyle.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_MAC_DKDARKGRAPHICSSTYLE_H_INCLUDED
#define DARWINKIT_MAC_DKDARKGRAPHICSSTYLE_H_INCLUDED

#import <DarwinKit/mac/DKGradientGraphicsStyle.h>

#ifdef __cplusplus
extern "C" {
#endif

@interface DKDarkGraphicsStyle : DKGradientGraphicsStyle
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_MAC_DKDARKGRAPHICSSTYLE_H_INCLUDED
