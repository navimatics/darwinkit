/*
 * DarwinKit/shared/NSString+SafeUTF8.m
 *
 * Copyright 2008-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>

/*
 * libicu
 */
#define U_STRING_NOT_TERMINATED_WARNING -124
#define U_BUFFER_OVERFLOW_ERROR 15
#define U_ZERO_ERROR 0
#define U_SUCCESS(x) ((x)<=U_ZERO_ERROR)
#define U_FAILURE(x) ((x)>U_ZERO_ERROR)
const char *u_errorName(int32_t code);
char *u_strToUTF8WithSub(
    char *dest, int32_t destCapacity, int32_t *pDestLength,
    const UniChar *src, int32_t srcLength,
    int32_t subchar, int32_t *pNumSubstitutions,
    int32_t *pErrorCode);
UniChar *u_strFromUTF8WithSub(
    UniChar *dest, int32_t destCapacity, int32_t *pDestLength,
    const char *src, int32_t srcLength,
    int32_t subchar, int32_t *pNumSubstitutions,
    int32_t *pErrorCode);

/*
 * NSString (SafeUTF8)
 */
#define REPLACEMENT_CHAR 0xFFFD
#define CHUNK_LENGTH 1024
@implementation NSString (SafeUTF8)
+ (id)safeStringWithUTF8String:(const char *)chrptr
{
    size_t chrlen = strlen(chrptr);
    if (0 == chrlen)
        return @"";
    int32_t uchlen = 0;
    int32_t status = U_ZERO_ERROR;
    u_strFromUTF8WithSub(
        NULL, 0, &uchlen,
        chrptr, (int32_t)chrlen,
        REPLACEMENT_CHAR, NULL,
        &status);
    if (U_FAILURE(status) && U_BUFFER_OVERFLOW_ERROR != status)
    {
        NSLog(@"%s u_strFromUTF8WithSub: %s\n\t%s", __PRETTY_FUNCTION__, u_errorName(status), chrptr);
        return @"";
    }
    UniChar *uchptr = malloc(uchlen * sizeof(UniChar));
    status = U_ZERO_ERROR;
    u_strFromUTF8WithSub(
        uchptr, uchlen, &uchlen,
        chrptr, (int32_t)chrlen,
        REPLACEMENT_CHAR, NULL,
        &status);
    if (U_FAILURE(status))
    {
        free(uchptr);
        NSLog(@"%s u_strFromUTF8WithSub: %s\n\t%s", __PRETTY_FUNCTION__, u_errorName(status), chrptr);
        return @"";
    }
    CFStringRef cfstr = CFStringCreateWithCharactersNoCopy(NULL, uchptr, uchlen, kCFAllocatorMalloc);
    if (NULL == cfstr)
    {
        free(uchptr);
        NSLog(@"%s CFStringCreateWithCharactersNoCopy: error\n\t%s", __PRETTY_FUNCTION__, chrptr);
        return @"";
    }
    return [(NSString *)cfstr autorelease];
}
- (const char *)safeUTF8String
{
    CFIndex cfslen = CFStringGetLength((CFStringRef)self);
    if (0 == cfslen)
        return "";
    const char *chrptr = CFStringGetCStringPtr((CFStringRef)self, kCFStringEncodingUTF8);
    if (NULL != chrptr)
        return [[NSData dataWithBytes:chrptr length:cfslen + 1] bytes];
    NSMutableData *data = [NSMutableData data];
    const UniChar *uchptr = CFStringGetCharactersPtr((CFStringRef)self);
    UniChar uchbuf[CHUNK_LENGTH];
    BOOL fast = NULL != uchptr;
    CFIndex cfsidx = 0;
    CFIndex datidx = 0;
    while (0 < cfslen)
    {
        const UniChar *chnptr;
        CFIndex chnlen = cfslen < CHUNK_LENGTH ? cfslen : CHUNK_LENGTH;
        cfslen -= chnlen;
        if (fast)
        {
            chnptr = uchptr;
            uchptr += chnlen;
        }
        else
        {
            chnptr = uchbuf;
            CFStringGetCharacters((CFStringRef)self, CFRangeMake(cfsidx, chnlen), uchbuf);
            cfsidx += chnlen;
        }
        int32_t datlen = (int32_t)chnlen * 3;
        int32_t status = U_ZERO_ERROR;
        [data setLength:datidx + datlen];
        u_strToUTF8WithSub(
            [data mutableBytes] + datidx, datlen, &datlen,
            chnptr, (int32_t)chnlen,
            REPLACEMENT_CHAR, NULL,
            &status);
        if (U_FAILURE(status))
        {
            NSLog(@"%s u_strToUTF8WithSub: %s\n\t%@", __PRETTY_FUNCTION__, u_errorName(status), self);
            break;
        }
        datidx += datlen;
    }
    [data setLength:datidx + 1];
    char *datptr = [data mutableBytes];
    datptr[datidx] = '\0';
    return datptr;
}
@end
