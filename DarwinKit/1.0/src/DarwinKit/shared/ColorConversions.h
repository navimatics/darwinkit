/*
 * DarwinKit/shared/ColorConversions.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_SHARED_COLORCONVERSIONS_H_INCLUDED
#define DARWINKIT_SHARED_COLORCONVERSIONS_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

#if defined(__LP64__)
typedef double ColorFloat;
#else
typedef float ColorFloat;
#endif

void srgb_to_ciexyz(const ColorFloat rgb[3], ColorFloat xyz[3]);
void ciexyz_to_srgb(const ColorFloat xyz[3], ColorFloat rgb[3]);

void ciexyz_to_cielab(const ColorFloat xyz[3], ColorFloat lab[3]);
void cielab_to_ciexyz(const ColorFloat lab[3], ColorFloat xyz[3]);

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_COLORCONVERSIONS_H_INCLUDED
