/*
 * DarwinKit/shared/DKMultiDelegate.m
 *
 * Copyright 2008-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>

@implementation DKMultiDelegate
{
    CFMutableArrayRef _delegates;
}
- (id)init
{
    _delegates = CFArrayCreateMutable(NULL, 1, NULL);
    return self;
}
- (id)initWithDelegate:(id)del
{
    _delegates = CFArrayCreateMutable(NULL, 1, NULL);
    CFArrayAppendValue(_delegates, del);
    return self;
}
- (void)dealloc
{
    CFRelease(_delegates);
    [super dealloc];
}
- (void)addDelegate:(id)del
{
    CFArrayAppendValue(_delegates, del);
}
- (void)removeDelegate:(id)del
{
    CFRange range = CFRangeMake(0, CFArrayGetCount(_delegates));
    CFIndex index = CFArrayGetFirstIndexOfValue(_delegates, range, del);
    if (-1 != index)
        CFArrayRemoveValueAtIndex(_delegates, index);
}
- (BOOL)conformsToProtocol:(Protocol *)protocol
{
    if ([super conformsToProtocol:protocol])
        return YES;
    CFRange range = CFRangeMake(0, CFArrayGetCount(_delegates));
    id values[range.length];
    CFArrayGetValues(_delegates, range, (void *)values);
    for (CFIndex i = 0; range.length > i; i++)
    {
        id del = values[i];
        if ([del conformsToProtocol:protocol])
            return YES; /* at least one of our delegates conforms */
    }
    return NO;
}
- (BOOL)respondsToSelector:(SEL)sel
{
    if ([super respondsToSelector:sel])
        return YES;
    CFRange range = CFRangeMake(0, CFArrayGetCount(_delegates));
    id values[range.length];
    CFArrayGetValues(_delegates, range, (void *)values);
    for (CFIndex i = 0; range.length > i; i++)
    {
        id del = values[i];
        if ([del respondsToSelector:sel])
            return YES; /* at least one of our delegates responds */
    }
    return NO;
}
- (NSMethodSignature *)methodSignatureForSelector:(SEL)sel
{
    CFRange range = CFRangeMake(0, CFArrayGetCount(_delegates));
    id values[range.length];
    CFArrayGetValues(_delegates, range, (void *)values);
    for (CFIndex i = 0; range.length > i; i++)
    {
        id del = values[i];
        NSMethodSignature *sig = [del methodSignatureForSelector:sel];
        if (NULL != sig)
            return sig; /* assume that all delegates return same method signature */
    }
    return NULL;
}
- (void)forwardInvocation:(NSInvocation *)invocation
{
    SEL sel = [invocation selector];
    CFRange range = CFRangeMake(0, CFArrayGetCount(_delegates));
    id values[range.length];
    CFArrayGetValues(_delegates, range, (void *)values);
    for (CFIndex i = 0; range.length > i; i++)
    {
        id del = values[i];
        if ([del respondsToSelector:sel])
            [invocation invokeWithTarget:del];
    }
}
@end
