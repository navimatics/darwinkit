/*
 * DarwinKit/shared/NSObject+AssociatedData.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>
#import <objc/runtime.h>

@implementation NSObject (AssociatedData)
- (void *)getAssociatedData:(size_t *)sizeKey
{
    static size_t instanceSize = 0;
    id assocObj = objc_getAssociatedObject(self, sizeKey);
    if (nil == assocObj)
    {
        Class NSObjectClass = [NSObject class];
        instanceSize = class_getInstanceSize(NSObjectClass);
        assocObj = class_createInstance(NSObjectClass, *sizeKey);
        objc_setAssociatedObject(self, sizeKey, assocObj, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        [assocObj release];
    }
    return (char *)assocObj + instanceSize;
}
- (id)getAssociatedObject:(Class)cls
{
    id assocObj = objc_getAssociatedObject(self, cls);
    if (nil == assocObj)
    {
        assocObj = [[cls alloc] init];
        objc_setAssociatedObject(self, cls, assocObj, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        [assocObj release];
    }
    return assocObj;
}
@end
