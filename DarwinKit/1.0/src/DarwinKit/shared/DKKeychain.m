/*
 * DarwinKit/shared/DKKeychain.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>

@implementation DKKeychain
+ (DKKeychain *)defaultKeychain
{
    static DKKeychain *keychain;
    if (nil == keychain)
        keychain = [[DKKeychain alloc] init];
    return keychain;
}
- (NSString *)passwordForAccount:(NSString *)accountName onService:(NSString *)serviceName
{
    NSData *passwordData = [self passwordDataForAccount:accountName onService:serviceName];
    if (nil == passwordData)
        return nil;
    return [[[NSString alloc] initWithData:passwordData encoding:NSUTF8StringEncoding] autorelease];
}
- (NSData *)passwordDataForAccount:(NSString *)accountName onService:(NSString *)serviceName
{
    NSDictionary *query = [NSDictionary dictionaryWithObjectsAndKeys:
        kSecClassGenericPassword, kSecClass,
        serviceName, kSecAttrService,
        accountName, kSecAttrAccount,
        kSecMatchLimitOne, kSecMatchLimit,
        [NSNumber numberWithBool:YES], kSecReturnData,
        nil];
    CFTypeRef result = NULL;
    OSStatus status = SecItemCopyMatching((CFDictionaryRef)query, &result);
    if (errSecSuccess != status)
        return nil;
    return [(id)result autorelease];
}
- (BOOL)setPassword:(NSString *)password forAccount:(NSString *)accountName onService:(NSString *)serviceName
{
    return [self setPasswordData:[password dataUsingEncoding:NSUTF8StringEncoding] forAccount:accountName onService:serviceName];
}
- (BOOL)setPasswordData:(NSData *)passwordData forAccount:(NSString *)accountName onService:(NSString *)serviceName
{
    NSMutableDictionary *query = [NSMutableDictionary dictionaryWithObjectsAndKeys:
        kSecClassGenericPassword, kSecClass,
        serviceName, kSecAttrService,
        accountName, kSecAttrAccount,
        passwordData, kSecValueData,
        nil];
    OSStatus status = SecItemAdd((CFDictionaryRef)query, NULL);
    if (errSecDuplicateItem == status)
    {
        [query removeObjectForKey:kSecValueData];
        NSDictionary *update = [NSDictionary dictionaryWithObjectsAndKeys:
            passwordData, kSecValueData,
            nil];
        status = SecItemUpdate((CFDictionaryRef)query, (CFDictionaryRef)update);
    }
    return errSecSuccess == status;
}
- (BOOL)removePasswordForAccount:(NSString *)accountName onService:(NSString *)serviceName
{
    NSDictionary *query = [NSDictionary dictionaryWithObjectsAndKeys:
        kSecClassGenericPassword, kSecClass,
        serviceName, kSecAttrService,
        accountName, kSecAttrAccount,
        nil];
    OSStatus status = SecItemDelete((CFDictionaryRef)query);
    return errSecSuccess == status;
}
@end
