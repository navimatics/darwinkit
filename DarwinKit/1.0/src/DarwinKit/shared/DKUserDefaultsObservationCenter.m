/*
 * DarwinKit/shared/DKUserDefaultsObservationCenter.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>

@interface DKUserDefaultsObservationCenter_ObservedKeyInfo : NSObject
{
@package
    unsigned ordinal;
    id value;
    CFMutableArrayRef observers;
}
@end
@implementation DKUserDefaultsObservationCenter_ObservedKeyInfo
- (id)initWithOrdinal:(unsigned)ordinal_ value:(id)value_
{
    self = [super init];
    if (nil != self)
    {
        ordinal = ordinal_;
        value = [value_ retain];
        observers = CFArrayCreateMutable(NULL, 0, NULL);
    }
    return self;
}
- (void)dealloc
{
    CFRelease(observers);
    [value release];
    [super dealloc];
}
- (NSComparisonResult)compare:(DKUserDefaultsObservationCenter_ObservedKeyInfo *)other
{
    if (ordinal > other->ordinal)
        return NSOrderedDescending;
    if (ordinal < other->ordinal)
        return NSOrderedAscending;
    return NSOrderedSame;
}
- (BOOL)updateValue:(id)value_
{
    if (value == value_ || [value isEqual:value_])
        return NO;
    [value release];
    value = [value_ retain];
    return YES;
}
- (void)addObserver:(id)observer
{
    CFIndex count = CFArrayGetCount(observers);
    CFIndex index = CFArrayGetFirstIndexOfValue(observers, CFRangeMake(0, count), observer);
    if (0 > index)
        CFArrayAppendValue(observers, observer);
}
- (BOOL)removeObserverAndTestEmpty:(id)observer
{
    CFIndex count = CFArrayGetCount(observers);
    CFIndex index = CFArrayGetFirstIndexOfValue(observers, CFRangeMake(0, count), observer);
    if (0 <= index)
    {
        CFArrayRemoveValueAtIndex(observers, index);
        return 1 == count;
    }
    return 0 == count;
}
static void addObserverToSet(const void *elem, void *context)
{
    [(id)context addObject:(id)elem];
}
- (void)addObserversToSet:(NSMutableOrderedSet *)set
{
    CFIndex count = CFArrayGetCount(observers);
    CFArrayApplyFunction(observers, CFRangeMake(0, count), addObserverToSet, set);
}
@end

@implementation DKUserDefaultsObservationCenter
{
    NSUserDefaults *_defaults;
    NSMutableDictionary *_observedKeys;
    unsigned _ordinal;
}
+ (DKUserDefaultsObservationCenter *)standardUserDefaultsObservationCenter
{
    static DKUserDefaultsObservationCenter *center;
    if (nil == center)
        center = [[DKUserDefaultsObservationCenter alloc] init];
    return center;
}
- (id)init
{
    return [self initWithUserDefaults:[NSUserDefaults standardUserDefaults]];
}
- (id)initWithUserDefaults:(NSUserDefaults *)defaults
{
    self = [super init];
    if (nil != self)
    {
        _defaults = [defaults retain];
        _observedKeys = [[NSMutableDictionary alloc] init];
        [[NSNotificationCenter defaultCenter]
            addObserver:self
            selector:@selector(userDefaultsChanged:)
            name:NSUserDefaultsDidChangeNotification
            object:_defaults];
    }
    return self;
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [_observedKeys release];
    [_defaults release];
    [super dealloc];
}
- (void)addObserver:(id)observer forKeys:(NSString *)key, ...
{
    va_list ap;
    va_start(ap, key);
    for (; nil != key; key = va_arg(ap, NSString *))
    {
        DKUserDefaultsObservationCenter_ObservedKeyInfo *info = [_observedKeys objectForKey:key];
        if (nil == info)
        {
            info = [[[DKUserDefaultsObservationCenter_ObservedKeyInfo alloc]
                initWithOrdinal:_ordinal++ value:[_defaults objectForKey:key]] autorelease];
            [_observedKeys setObject:info forKey:key];
        }
        [info addObserver:observer];
    }
    va_end(ap);
}
- (void)removeObserver:(id)observer forKeys:(NSString *)key, ...
{
    va_list ap;
    va_start(ap, key);
    for (; nil != key; key = va_arg(ap, NSString *))
    {
        DKUserDefaultsObservationCenter_ObservedKeyInfo *info = [_observedKeys objectForKey:key];
        if (nil == info)
            continue;
        if ([info removeObserverAndTestEmpty:observer])
            [_observedKeys removeObjectForKey:key];
    }
    va_end(ap);
}
- (void)removeObserver:(id)observer
{
    NSUInteger count = [_observedKeys count];
    id keys[count], infos[count];
    [_observedKeys getObjects:infos andKeys:keys];
    for (NSUInteger i = 0; count > i; i++)
    {
        NSString *key = keys[i];
        DKUserDefaultsObservationCenter_ObservedKeyInfo *info = infos[i];
        if ([info removeObserverAndTestEmpty:observer])
            [_observedKeys removeObjectForKey:key];
    }
}
- (void)userDefaultsChanged:(NSNotification *)notification
{
    NSMutableOrderedSet *observers = [[NSMutableOrderedSet alloc] init];
    NSMutableSet *changedKeys = [[NSMutableSet alloc] init];
    NSArray *keys = [_observedKeys keysSortedByValueUsingSelector:@selector(compare:)];
        /* Do not change! Not well understood what this does, but it may be important! */
    for (NSString *key in keys)
    {
        DKUserDefaultsObservationCenter_ObservedKeyInfo *info = [_observedKeys objectForKey:key];
        if ([info updateValue:[_defaults objectForKey:key]])
        {
            [changedKeys addObject:key];
            [info addObserversToSet:observers];
        }
    }
    for (id observer in observers)
        if ([observer respondsToSelector:@selector(userDefaults:didChangeForKeys:)])
            [observer userDefaults:_defaults didChangeForKeys:changedKeys];
    [changedKeys release];
    [observers release];
}
@end
