/*
 * DarwinKit/shared/ColorConversions.c
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#include <DarwinKit/shared/ColorConversions.h>
#include <math.h>

/* See http://www.w3.org/Graphics/Color/sRGB.html */
void srgb_to_ciexyz(const ColorFloat rgb[3], ColorFloat xyz[3])
{
    ColorFloat rgbl[3] = { rgb[0], rgb[1], rgb[2] };
    if (rgbl[0] <= 0.03928)
        rgbl[0] = rgbl[0] / 12.92;
    else
        rgbl[0] = pow(((rgbl[0] + 0.055) / 1.055), 2.4);
    if (rgbl[1] <= 0.03928)
        rgbl[1] = rgbl[1] / 12.92;
    else
        rgbl[1] = pow(((rgbl[1] + 0.055) / 1.055), 2.4);
    if (rgbl[2] <= 0.03928)
        rgbl[2] = rgbl[2] / 12.92;
    else
        rgbl[2] = pow(((rgbl[2] + 0.055) / 1.055), 2.4);
    xyz[0] = rgbl[0] * 0.4124 + rgbl[1] * 0.3576 + rgbl[2] * 0.1805;
    xyz[1] = rgbl[0] * 0.2126 + rgbl[1] * 0.7152 + rgbl[2] * 0.0722;
    xyz[2] = rgbl[0] * 0.0193 + rgbl[1] * 0.1192 + rgbl[2] * 0.9505;
}
void ciexyz_to_srgb(const ColorFloat xyz[3], ColorFloat rgb[3])
{
    rgb[0] = xyz[0] *  3.2410 + xyz[1] * -1.5374 + xyz[2] * -0.4986;
    rgb[1] = xyz[0] * -0.9692 + xyz[1] *  1.8760 + xyz[2] *  0.0416;
    rgb[2] = xyz[0] *  0.0556 + xyz[1] * -0.2040 + xyz[2] *  1.0570;
    if (rgb[0] <= 0.00304)
        rgb[0] = 12.92 * rgb[0];
    else
        rgb[0] = 1.055 * pow(rgb[0], 1 / 2.4) - 0.055;
    if (rgb[1] <= 0.00304)
        rgb[1] = 12.92 * rgb[1];
    else
        rgb[1] = 1.055 * pow(rgb[1], 1 / 2.4) - 0.055;
    if (rgb[2] <= 0.00304)
        rgb[2] = 12.92 * rgb[2];
    else
        rgb[2] = 1.055 * pow(rgb[2], 1 / 2.4) - 0.055;
    if (rgb[0] < 0)
        rgb[0] = 0;
    else if (rgb[0] > 1)
        rgb[0] = 1;
    if (rgb[1] < 0)
        rgb[1] = 0;
    else if (rgb[1] > 1)
        rgb[1] = 1;
    if (rgb[2] < 0)
        rgb[2] = 0;
    else if (rgb[2] > 1)
        rgb[2] = 1;
}

/* See http://en.wikipedia.org/wiki/Lab_color_space */
void ciexyz_to_cielab(const ColorFloat xyz[3], ColorFloat lab[3])
{
    ColorFloat xyzn[3] = { xyz[0] / 0.9505, xyz[1] / 1.0000, xyz[2] / 1.0890 };
    if (xyzn[0] > (6.0 / 29.0) * (6.0 / 29.0) * (6.0 / 29.0))
        xyzn[0] = pow(xyzn[0], 1 / 3.0);
    else
        xyzn[0] = (1 / 3.0) * (29.0 / 6.0) * (29.0 / 6.0) * xyzn[0] + 4.0 / 29.0;
    if (xyzn[1] > (6.0 / 29.0) * (6.0 / 29.0) * (6.0 / 29.0))
        xyzn[1] = pow(xyzn[1], 1 / 3.0);
    else
        xyzn[1] = (1 / 3.0) * (29.0 / 6.0) * (29.0 / 6.0) * xyzn[1] + 4.0 / 29.0;
    if (xyzn[2] > (6.0 / 29.0) * (6.0 / 29.0) * (6.0 / 29.0))
        xyzn[2] = pow(xyzn[2], 1 / 3.0);
    else
        xyzn[2] = (1 / 3.0) * (29.0 / 6.0) * (29.0 / 6.0) * xyzn[2] + 4.0 / 29.0;
    lab[0] = (116 * xyzn[1]) - 16;
    lab[1] = 500 * (xyzn[0] - xyzn[1]);
    lab[2] = 200 * (xyzn[1] - xyzn[2]);
}
void cielab_to_ciexyz(const ColorFloat lab[3], ColorFloat xyz[3])
{
    xyz[1] = (lab[0] + 16) / 116;
    xyz[0] = xyz[1] + lab[1] / 500;
    xyz[2] = xyz[1] - lab[2] / 200;
    if (xyz[0] > 6.0 / 29.0)
        xyz[0] = pow(xyz[0], 3.0);
    else
        xyz[0] = 3.0 * (6.0 / 29.0) * (6.0 / 29.0) * (xyz[0] - 4.0 / 29.0);
    if (xyz[1] > 6.0 / 29.0)
        xyz[1] = pow(xyz[1], 3.0);
    else
        xyz[1] = 3.0 * (6.0 / 29.0) * (6.0 / 29.0) * (xyz[1] - 4.0 / 29.0);
    if (xyz[2] > 6.0 / 29.0)
        xyz[2] = pow(xyz[2], 3.0);
    else
        xyz[2] = 3.0 * (6.0 / 29.0) * (6.0 / 29.0) * (xyz[2] - 4.0 / 29.0);
    xyz[0] *= 0.9505;
    xyz[1] *= 1.0000;
    xyz[2] *= 1.0890;
}

#if defined(DARWINKIT_COLORCONVERSIONS_TEST)
#include <stdio.h>
#include <string.h>
void print_conversions(ColorFloat srgb[3])
{
    ColorFloat rgb[3], xyz[3], lab[3];
    memcpy(rgb, srgb, sizeof rgb);
    srgb_to_ciexyz(rgb, xyz); ciexyz_to_cielab(xyz, lab);
    printf("%f %f %f -> %f %f %f -> %f %f %f\n",
        rgb[0], rgb[1], rgb[2], xyz[0], xyz[1], xyz[2], lab[0], lab[1], lab[2]);
    cielab_to_ciexyz(lab, xyz); ciexyz_to_srgb(xyz, rgb);
    printf("%f %f %f <- %f %f %f <- %f %f %f\n",
        rgb[0], rgb[1], rgb[2], xyz[0], xyz[1], xyz[2], lab[0], lab[1], lab[2]);
}
int main()
{
    ColorFloat wht[3] = { 1.0, 1.0, 1.0 };
    ColorFloat blk[3] = { 0.0, 0.0, 0.0 };
    ColorFloat red[3] = { 1.0, 0.0, 0.0 };
    ColorFloat grn[3] = { 0.0, 1.0, 0.0 };
    ColorFloat blu[3] = { 0.0, 0.0, 1.0 };
    ColorFloat red_1_4[3] = { 0.25, 0.0, 0.0 };
    ColorFloat grn_1_4[3] = { 0.0, 0.25, 0.0 };
    ColorFloat blu_1_4[3] = { 0.0, 0.0, 0.25 };
    ColorFloat red_1_2[3] = { 0.5, 0.0, 0.0 };
    ColorFloat grn_1_2[3] = { 0.0, 0.5, 0.0 };
    ColorFloat blu_1_2[3] = { 0.0, 0.0, 0.5 };
    ColorFloat red_3_4[3] = { 0.75, 0.0, 0.0 };
    ColorFloat grn_3_4[3] = { 0.0, 0.75, 0.0 };
    ColorFloat blu_3_4[3] = { 0.0, 0.0, 0.75 };
    print_conversions(wht);
    print_conversions(blk);
    print_conversions(red);
    print_conversions(grn);
    print_conversions(blu);
    print_conversions(red_1_4);
    print_conversions(grn_1_4);
    print_conversions(blu_1_4);
    print_conversions(red_1_2);
    print_conversions(grn_1_2);
    print_conversions(blu_1_2);
    print_conversions(red_3_4);
    print_conversions(grn_3_4);
    print_conversions(blu_3_4);
}
#endif
