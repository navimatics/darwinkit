/*
 * DarwinKit/ios/UIApplication+TouchViewing.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>

#define TOUCH_RADIUS                    15

@implementation UIApplication (TouchViewing)
static size_t AssociatedDataKey = sizeof(BOOL);
static UIWindow *touchWindow;
static UIColor *touchColor;
static int enableTouchViewingCount = -1; /* >= 0: enabled; < 0: disabled */
static UIView *addTouchView(UITouch *touch);
static UIView *getTouchView(UITouch *touch);
static void removeTouchView(UITouch *touch);
static void addTouchViewAnimated(UIView *touchView);
static void removeTouchViewAnimated(UIView *touchView);
static UIView *addTouchView(UITouch *touch)
{
    CGRect rect = CGRectMake(0, 0, TOUCH_RADIUS * 2, TOUCH_RADIUS * 2);
    UIImageView *touchView = [[[UIImageView alloc] initWithFrame:rect] autorelease];
    touchView.tag = touch.hash;
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, [[UIScreen mainScreen] scale]);
    UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:rect];
    [touchColor setFill];
    [path fill];
    touchView.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    addTouchViewAnimated(touchView);
    return touchView;
}
static UIView *getTouchView(UITouch *touch)
{
    return [touchWindow viewWithTag:touch.hash];
}
static void removeTouchView(UITouch *touch)
{
    UIView *touchView = [touchWindow viewWithTag:touch.hash];
    touchView.tag = 0;
        /* mark touch view no longer associated with touch */
    removeTouchViewAnimated(touchView);
}
static void addTouchViewAnimated(UIView *touchView)
{
    [touchWindow addSubview:touchView];
    touchView.transform = CGAffineTransformMakeScale(0, 0);
    [UIView
        animateWithDuration:0.2
        delay:0
        options:0
        animations:^
        {
            touchView.transform = CGAffineTransformMakeScale(1, 1);
        }
        completion:^(BOOL finished)
        {
            *(BOOL *)[touchView getAssociatedData:&AssociatedDataKey] = YES;
                /* mark touch view has initial animation complete */
            if (0 != touchView.tag)
            {
                [UIView
                    animateWithDuration:0.2
                    delay:0
                    options:UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse
                    animations:^
                    {
                        touchView.transform = CGAffineTransformMakeScale(0.7, 0.7);
                    }
                    completion:NULL];
            }
            else /* 0 == touchView.tag: touch view no longer associated with touch */
                removeTouchViewAnimated(touchView);
        }];
}
static void removeTouchViewAnimated(UIView *touchView)
{
    if (*(BOOL *)[touchView getAssociatedData:&AssociatedDataKey])
        /* if touch view has initial animation complete */
        [UIView
            animateWithDuration:0.2
            delay:0
            options:0
            animations:^
            {
                touchView.transform = CGAffineTransformMakeScale(0, 0);
            }
            completion:^(BOOL finished)
            {
                [touchView removeFromSuperview];
            }];
}
+ (void)loadTouchViewing
{
    static BOOL done;
    if (!done)
    {
        [self
            swizzleInstanceMethod:@selector(sendEvent:)
            withMethod:@selector(__swizzle__sendEvent:)];
        touchWindow = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        touchWindow.windowLevel = UIWindowLevelStatusBar + 1;
        touchWindow.userInteractionEnabled = NO;
        touchColor = [[UIColor colorWithRed:1.0 green:0 blue:0 alpha:0.3] retain];
        done = YES;
    }
}
+ (void)enableTouchViewing:(BOOL)flag
{
    if (flag)
        enableTouchViewingCount++;
    else
        enableTouchViewingCount--;
    touchWindow.hidden = 0 > enableTouchViewingCount;
}
+ (void)setTouchViewingColor:(UIColor *)color
{
    [touchColor release];
    touchColor = [color retain];
}
- (void)__swizzle__sendEvent:(UIEvent *)event
{
    [self __swizzle__sendEvent:event];
    if (0 <= enableTouchViewingCount)
        for (UITouch *touch in [event allTouches])
        {
            switch (touch.phase)
            {
            case UITouchPhaseBegan:
                {
                    UIView *touchView = addTouchView(touch);
                    touchView.center = [touch locationInView:touchWindow];
                }
                break;
            case UITouchPhaseMoved:
            case UITouchPhaseStationary:
                {
                    UIView *touchView = getTouchView(touch);
                    touchView.center = [touch locationInView:touchWindow];
                }
                break;
            case UITouchPhaseEnded:
            case UITouchPhaseCancelled:
                removeTouchView(touch);
                break;
            }
        }
}
@end
