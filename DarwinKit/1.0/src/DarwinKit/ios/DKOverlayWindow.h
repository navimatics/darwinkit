/*
 * DarwinKit/ios/DKOverlayWindow.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_IOS_DKOVERLAYWINDOW_H_INCLUDED
#define DARWINKIT_IOS_DKOVERLAYWINDOW_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

@interface DKOverlayWindow : UIWindow
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_IOS_DKOVERLAYWINDOW_H_INCLUDED
