/*
 * DarwinKit/ios/DKRatingView.m
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>

NSString *const kRatingViewStateUnrated = @"kRatingViewStateUnrated";
NSString *const kRatingViewStateRated = @"kRatingViewStateRated";
NSString *const kRatingViewStateUserRated = @"kRatingViewStateUserRated";

enum
{
    StateUnknownIndex = 0,
    StateUnratedIndex,
    StateRatedIndex,
    StateUserRatedIndex,
    StateCount,
};
static ssize_t indexFromState(NSString *state)
{
    if ([state isEqualToString:kRatingViewStateUnrated])
        return StateUnratedIndex;
    else if ([state isEqualToString:kRatingViewStateRated])
        return StateRatedIndex;
    else if ([state isEqualToString:kRatingViewStateUserRated])
        return StateUserRatedIndex;
    else
        return StateUnknownIndex;
}

@implementation DKRatingView
{
    UIImage *_starImages[StateCount];
    NSUInteger _stars;
    float _rating;
    float _userRating;
    float _touchBeganUserRating;
    BOOL _usesFractionalStarsForUserRating;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (nil != self)
        _stars = 5;
    return self;
}
- (id)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (nil != self)
        _stars = 5;
    return self;
}
- (void)dealloc
{
    for (size_t i = 0; StateCount > i; i++)
        [_starImages[i] release];
    [super dealloc];
}
- (void)setMultipleTouchEnabled:(BOOL)value
{
    [super setMultipleTouchEnabled:NO];
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self touchesReceived:touches withEvent:event phase:UITouchPhaseBegan];
}
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self touchesReceived:touches withEvent:event phase:UITouchPhaseMoved];
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self touchesReceived:touches withEvent:event phase:UITouchPhaseEnded];
}
- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self touchesReceived:touches withEvent:event phase:UITouchPhaseCancelled];
}
- (void)touchesReceived:(NSSet *)touches withEvent:(UIEvent *)event phase:(UITouchPhase)phase
{
    switch (phase)
    {
    case UITouchPhaseBegan:
        _touchBeganUserRating = self.userRating;
        break;
    case UITouchPhaseMoved:
    case UITouchPhaseEnded:
        {
            CGRect bounds = self.bounds;
            if (0 == bounds.size.width)
                return;
            CGPoint point = [[touches anyObject] locationInView:self];
            self.userRating = point.x / bounds.size.width;
        }
        break;
    case UITouchPhaseCancelled:
        self.userRating = _touchBeganUserRating;
        break;
    default:
        break;
    }
}
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    float rating = 0 < _userRating ? _userRating : _rating;
    CGRect bounds = self.bounds;
    CGSize starsize = bounds.size;
    starsize.width /= _stars;
    CGRect cliprect = bounds;
    cliprect.size.width *= rating;
    CGContextSaveGState(context);
    CGContextClipToRect(context, cliprect);
    ssize_t index = 0 < _userRating ? StateUserRatedIndex : StateRatedIndex;
    for (size_t i = 0, n = (int)ceilf(_stars * rating); n > i; i++)
        [_starImages[index] drawInRect:CGRectMake(starsize.width * i, 0,
            starsize.width, starsize.height)];
    CGContextRestoreGState(context);
    cliprect.origin.x = cliprect.size.width;
    cliprect.size.width = bounds.size.width - cliprect.size.width;
    CGContextClipToRect(context, cliprect);
    index = StateUnratedIndex;
    for (size_t i = (int)floorf(_stars * rating), n = _stars; n > i; i++)
        [_starImages[index] drawInRect:CGRectMake(starsize.width * i, 0,
            starsize.width, starsize.height)];
}
- (UIImage *)starImageForState:(NSString *)state
{
    return _starImages[indexFromState(state)];
}
- (void)setStarImage:(UIImage *)image forState:(NSString *)state
{
    ssize_t index = indexFromState(state);
    if (StateUnknownIndex == index)
        return;
    [_starImages[index] release];
    _starImages[index] = [image retain];
    [self setNeedsDisplay];
}
- (NSUInteger)stars
{
    return _stars;
}
- (void)setStars:(NSUInteger)value
{
    if (_stars != value)
    {
        _stars = value;
        [self setNeedsDisplay];
    }
}
- (float)rating
{
    return _rating;
}
- (void)setRating:(float)value
{
    if (_rating != value && 0 <= value && value <= 1.0)
    {
        _rating = value;
        [self setNeedsDisplay];
    }
}
- (float)userRating
{
    return _userRating;
}
- (void)setUserRating:(float)value
{
    if (!_usesFractionalStarsForUserRating)
        value = ceilf(value * _stars) / _stars;
    if (_userRating != value && 0 <= value && value <= 1.0)
    {
        _userRating = value;
        [self setNeedsDisplay];
    }
}
@synthesize usesFractionalStarsForUserRating = _usesFractionalStarsForUserRating;
@end
