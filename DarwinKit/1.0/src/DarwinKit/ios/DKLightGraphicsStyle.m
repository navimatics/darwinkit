/*
 * DarwinKit/ios/DKLightGraphicsStyle.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>
#import <DarwinKit/ios/DKLightGraphicsStyle.h>

@implementation DKLightGraphicsStyle
{
    UIColor *_styledBackgroundColor;
    UIColor *_styledContentBackgroundColor;
    UIColor *_styledTextColor;
    UIColor *_styledHighlightedTextColor;
    UIColor *_styledPlaceholderTextColor;
}
+ (void)load
{
    [self registerGraphicsStyleClass:self forName:@"Light"];
    [self registerGraphicsStyleClass:self forName:@""]; /* default graphics style */
}
- (id)init
{
    self = [super init];
    if (nil != self)
    {
        _styledBackgroundColor = [[UIColor groupTableViewBackgroundColor] retain];
        _styledContentBackgroundColor = [[UIColor whiteColor] retain];
        _styledTextColor = [[UIColor darkTextColor] retain];
        _styledHighlightedTextColor = [[UIColor darkTextColor] retain];
        _styledPlaceholderTextColor = [[UIColor lightGrayColor] retain];
    }
    return self;
}
- (void)dealloc
{
    [_styledPlaceholderTextColor release];
    [_styledHighlightedTextColor release];
    [_styledTextColor release];
    [_styledContentBackgroundColor release];
    [_styledBackgroundColor release];
    [super dealloc];
}
- (UIActionSheetStyle)styledActionSheetStyle
{
    return UIActionSheetStyleDefault;
}
- (UIBarStyle)styledBarStyle
{
    return UIBarStyleDefault;
}
- (UIKeyboardAppearance)styledKeyboardAppearance
{
    return UIKeyboardAppearanceLight;
}
- (UIColor *)styledBackgroundColor
{
    return _styledBackgroundColor;
}
- (UIColor *)styledContentBackgroundColor
{
    return _styledContentBackgroundColor;
}
- (UIColor *)styledTextColor
{
    return _styledTextColor;
}
- (UIColor *)styledHighlightedTextColor
{
    return _styledHighlightedTextColor;
}
- (UIColor *)styledPlaceholderTextColor
{
    return _styledPlaceholderTextColor;
}
- (UIColor *)getStyledBackgroundColor:(UIColor *)color
{
    return color;
}
- (UIColor *)getStyledTextColor:(UIColor *)color
{
    return color;
}
@end
