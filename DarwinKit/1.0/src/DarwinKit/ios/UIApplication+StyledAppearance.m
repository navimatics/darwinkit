/*
 * DarwinKit/ios/UIApplication+StyledAppearance.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>
#import <DarwinKit/ios/DKGraphicsStyle.h>

@implementation UIApplication (StyledAppearance)
static NSString *_name;
+ (void)loadStyledAppearance
{
    static BOOL done;
    if (!done)
    {
        [DKBackgroundView loadStyledAppearance];
        [DKContentView loadStyledAppearance];
        [UIActionSheet loadStyledAppearance];
        [UILabel loadStyledAppearance];
        [UINavigationBar loadStyledAppearance];
        [UIPopoverController loadStyledAppearance];
        [UISearchBar loadStyledAppearance];
        [UITabBar loadStyledAppearance];
        [UITableView loadStyledAppearance];
        [UITableViewCell loadStyledAppearance];
        [UITextField loadStyledAppearance];
        [UITextView loadStyledAppearance];
        [UIToolbar loadStyledAppearance];
        [UIView loadStyledAppearance];
        _name = [[[NSUserDefaults standardUserDefaults] stringForKey:@"DKGraphicsStyle"] copy];
        if (nil != _name)
            [DKGraphicsStyle setCurrentStyleWithName:_name];
        done = YES;
    }
}
+ (NSString *)styledAppearance
{
    return _name;
}
+ (void)setStyledAppearance:(NSString *)name
{
    if (nil == name)
        name = @"";
    if (![_name isEqualToString:name])
    {
        [_name release];
        _name = [name copy];
        [[NSUserDefaults standardUserDefaults] setObject:_name forKey:@"DKGraphicsStyle"];
        [DKGraphicsStyle setCurrentStyleWithName:_name];
        for (UIWindow *window in [self sharedApplication].windows)
        {
            /* Remove and re-add all window subviews; this will force a redraw. */
            NSArray *subviews = window.subviews;
            for (UIView *view in subviews)
                [view removeFromSuperview];
            for (UIView *view in subviews)
                [window addSubview:view];
        }
    }
}
@end
