/*
 * DarwinKit/ios/DKMenuView.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>
#import <DarwinKit/ios/DKUtility.h>

#define BackgroundColor                 [UIColor colorWithWhite:0.20 alpha:0.9]
#define ButtonTitleColor                [UIColor whiteColor]
#define ButtonTitleFont                 [UIFont systemFontOfSize:14]
#define ButtonImageInsets               UIEdgeInsetsMake(0, 0, 0, +8)
#define ButtonHorzMargin                20
#define ButtonVertMargin                4
#define ButtonGap                       1
#define ArrowGap                        1
#define ArrowSize                       CGSizeMake(22, 12)
#define CornerRadii                     CGSizeMake(8, 8)

@interface DKMenuView () <UIGestureRecognizerDelegate>
@end
@implementation DKMenuView
{
    id _target; /* weak */
    CFMutableArrayRef _actions;
    int _arrowDirection;
    CGRect _arrowRect;
    BOOL _animated;
}
@synthesize buttonTitleColor = _buttonTitleColor;
@synthesize buttonTitleFont = _buttonTitleFont;
@synthesize selectedButtonIndex = _selectedButtonIndex;
@synthesize dismissedButtonIndex = _dismissedButtonIndex;
- (id)initWithTarget:(id)target
{
    self = [super initWithFrame:CGRectZero];
    if (nil != self)
    {
        _target = target;
        _actions = CFArrayCreateMutable(NULL, 0, NULL);
        _selectedButtonIndex = -1;
        _dismissedButtonIndex = -1;
        self.backgroundColor = BackgroundColor;
        self.buttonTitleColor = ButtonTitleColor;
        self.buttonTitleFont = ButtonTitleFont;
        [[NSNotificationCenter defaultCenter]
            addObserver:self
            selector:@selector(willChangeStatusBarOrientation:)
            name:UIApplicationWillChangeStatusBarOrientationNotification
            object:nil];
    }
    return self;
}
- (id)initWithTarget:(id)target buttonTitlesAndActions:(NSString *)buttonTitle, ...
{
    self = [self initWithTarget:target];
    if (nil != self)
    {
        va_list ap;
        va_start(ap, buttonTitle);
        for (; nil != buttonTitle; buttonTitle = va_arg(ap, NSString *))
        {
            SEL action = va_arg(ap, SEL);
            [self addButtonWithTitle:buttonTitle action:action];
        }
        va_end(ap);
    }
    return self;
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [_buttonTitleFont release];
    [_buttonTitleColor release];
    if (NULL != _actions)
        CFRelease(_actions);
    [super dealloc];
}
- (void)setBackgroundColor:(UIColor *)backgroundColor
{
    [super setBackgroundColor:backgroundColor];
    self.opaque = NO;
    self.layer.opaque = NO;
}
- (NSInteger)addButtonWithTitle:(NSString *)buttonTitle action:(SEL)action
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.tintColor = _buttonTitleColor;
    button.titleLabel.font = _buttonTitleFont;
    button.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    button.titleLabel.textAlignment = NSTextAlignmentCenter;
    button.imageEdgeInsets = ButtonImageInsets;
    [button setTitle:buttonTitle forState:UIControlStateNormal];
    [button setTitleColor:_buttonTitleColor forState:UIControlStateNormal];
    [button setImage:[DKUtility imageNamed:@"BtnCheckmarkTemplate"] forState:UIControlStateSelected];
    return [self addButton:button action:action];
}
- (NSInteger)addButton:(UIButton *)button action:(SEL)action
{
    CGRect buttonFrame = button.frame;
    buttonFrame.size = [button sizeThatFits:CGSizeZero];
    buttonFrame.size.width += ButtonHorzMargin * 2;
    buttonFrame.size.height += ButtonVertMargin * 2;
    button.frame = buttonFrame;
    [button
        addTarget:self
        action:@selector(buttonAction:)
        forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:button];
    if (NULL != _actions)
        CFArrayAppendValue(_actions, action);
    return [self.subviews count] - 1;
}
- (void)showFromRect:(CGRect)rect inView:(UIView *)view animated:(BOOL)animated
{
    [self dismissMenuNow];
    UIWindow *window = view.window;
    UIView *topView = view;
    for (;;)
    {
        UIView *superview = topView.superview;
        if (nil == superview || window == superview)
            break;
        topView = superview;
    }
    if (nil == window || topView == window)
    {
        NSLog(@"%s requires a view with a non-nil window", __PRETTY_FUNCTION__);
        return;
    }
    UIControl *contentView = [[[UIControl alloc] initWithFrame:CGRectZero] autorelease];
    contentView.transform = topView.transform;
    contentView.center = topView.center;
    contentView.bounds = topView.bounds;
    contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    contentView.autoresizesSubviews = YES;
    contentView.opaque = NO;
    contentView.backgroundColor = [UIColor clearColor];
    [contentView
        addTarget:self
        action:@selector(dismissFromContainerView:)
        forControlEvents:UIControlEventTouchUpInside];
    [window addSubview:contentView];
    _arrowRect.origin = CGPointZero;
    _arrowRect.size = ArrowSize;
    CGRect frame = CGRectZero;
    NSArray *subviews = self.subviews;
    for (UIButton *button in subviews)
    {
        if (0 != frame.size.width)
            frame.size.width += ButtonGap;
        CGRect buttonFrame = button.frame;
        buttonFrame.origin.x = frame.size.width;
        buttonFrame.origin.y = 0;
        frame.size.width += buttonFrame.size.width;
        frame.size.height = MAX(frame.size.height, buttonFrame.size.height);
        button.frame = buttonFrame;
    }
    for (UIButton *button in subviews)
    {
        CGRect buttonFrame = button.frame;
        buttonFrame.size.height = frame.size.height;
        button.frame = buttonFrame;
    }
    if (0 <= _selectedButtonIndex && _selectedButtonIndex < [subviews count])
        [[subviews objectAtIndex:_selectedButtonIndex] setSelected:YES];
    frame.size.height += _arrowRect.size.height;
    CGRect contentBounds = contentView.bounds;
    rect = [contentView convertRect:rect fromView:view];
    frame.origin.x = rect.origin.x + (rect.size.width - frame.size.width) / 2;
    if (0 > frame.origin.x)
        frame.origin.x = 0;
    else if (frame.origin.x + frame.size.width > contentBounds.size.width)
        frame.origin.x = contentBounds.size.width - frame.size.width;
    CGFloat arrowMinX = MAX(frame.origin.x, rect.origin.x);
    CGFloat arrowMaxX = MIN(frame.origin.x + frame.size.width, rect.origin.x + rect.size.width);
    _arrowRect.origin.x = (arrowMinX + arrowMaxX - _arrowRect.size.width) / 2;
    if (rect.origin.y - frame.size.height >= 0)
        _arrowDirection = +1; /* down arrow */
    else if (rect.origin.y + rect.size.height + frame.size.height <= contentBounds.size.height)
        _arrowDirection = -1; /* up arrow */
    else
        _arrowDirection = 0; /* no arrow */
    if (+1 == _arrowDirection)
    {
        frame.origin.y = rect.origin.y - frame.size.height;
        _arrowRect.origin.y = frame.origin.y + frame.size.height - _arrowRect.size.height;
        _arrowRect.origin.y += ArrowGap;
        _arrowRect.size.height -= ArrowGap;
    }
    else if (-1 == _arrowDirection)
    {
        frame.origin.y = rect.origin.y + rect.size.height;
        for (UIButton *button in subviews)
        {
            CGRect buttonFrame = button.frame;
            buttonFrame.origin.y = _arrowRect.size.height;
            button.frame = buttonFrame;
        }
        _arrowRect.origin.y = frame.origin.y;
        _arrowRect.size.height -= ArrowGap;
    }
    else
    {
        frame.size.height -= _arrowRect.size.height;
        frame.origin.y = rect.origin.y + (rect.size.height - frame.size.height) / 2;
    }
    self.frame = frame;
    self.alpha = 0;
    [contentView addSubview:self];
    _arrowRect = [self convertRect:_arrowRect fromView:contentView];
    _animated = animated;
    [UIView animateWithDuration:animated ? 0.2 : 0
        animations:^
        {
            self.alpha = 1;
        }
        completion:NULL];
}
- (void)dismissMenuAnimated:(BOOL)animated
{
    UIView *contentView = self.superview;
    [UIView animateWithDuration:animated ? 0.2 : 0
        animations:^
        {
            self.alpha = 0;
        }
        completion:^(BOOL finished)
        {
            [contentView removeFromSuperview];
            [self removeFromSuperview];
        }];
}
- (void)dismissMenuNow
{
    UIView *contentView = self.superview;
    [contentView removeFromSuperview];
    [self removeFromSuperview];
}
- (void)buttonAction:(id)sender
{
    if (NULL == _actions)
        return;
    NSArray *subviews = self.subviews;
    NSInteger buttonIndex = [subviews indexOfObject:sender];
    if (NSNotFound != buttonIndex)
    {
        if (0 <= _selectedButtonIndex && _selectedButtonIndex < [subviews count])
        {
            [[subviews objectAtIndex:_selectedButtonIndex] setSelected:NO];
            _selectedButtonIndex = buttonIndex;
            [sender setSelected:YES];
        }
        _dismissedButtonIndex = buttonIndex;
        if (0 <= buttonIndex && buttonIndex < CFArrayGetCount(_actions))
        {
            SEL action = (SEL)CFArrayGetValueAtIndex(_actions, buttonIndex);
            if (NULL != action && [_target respondsToSelector:action])
                [_target performSelector:action withObject:self];
        }
    }
    [self dismissMenuAnimated:_animated];
}
- (void)dismissFromContainerView:(id)sender
{
    [self dismissMenuAnimated:_animated];
}
- (void)willChangeStatusBarOrientation:(NSNotification *)notification
{
    [self dismissMenuNow];
}
- (void)drawLayer:(CALayer *)layer inContext:(CGContextRef)ctx
{
    UIGraphicsPushContext(ctx);
    [self drawRect:[self bounds]];
    UIGraphicsPopContext();
}
- (void)drawRect:(CGRect)rect
{
    [self.backgroundColor setFill];
    UIRectCorner leftCorners = UIRectCornerTopLeft | UIRectCornerBottomLeft;
    UIRectCorner rightCorners = UIRectCornerTopRight | UIRectCornerBottomRight;
    CGRect bounds = self.bounds;
    CGFloat arrowMinX = MAX(bounds.origin.x, _arrowRect.origin.x);
    CGFloat arrowMaxX = MIN(bounds.origin.x + bounds.size.width, _arrowRect.origin.x + _arrowRect.size.width);
    if (+1 == _arrowDirection) /* down arrow */
    {
        if (arrowMinX < CornerRadii.width)
            leftCorners = UIRectCornerTopLeft;
        else if (arrowMaxX > bounds.size.width - CornerRadii.width)
            rightCorners = UIRectCornerTopRight;
        UIBezierPath *path = [UIBezierPath bezierPath];
        [path moveToPoint:CGPointMake(arrowMinX, _arrowRect.origin.y)];
        [path addLineToPoint:CGPointMake(
            _arrowRect.origin.x + _arrowRect.size.width / 2, _arrowRect.origin.y + _arrowRect.size.height)];
        [path addLineToPoint:CGPointMake(arrowMaxX, _arrowRect.origin.y)];
        [path fill];
    }
    else if (-1 == _arrowDirection) /* up arrow */
    {
        if (arrowMinX < CornerRadii.width)
            leftCorners = UIRectCornerBottomLeft;
        else if (arrowMaxX > bounds.size.width - CornerRadii.width)
            rightCorners = UIRectCornerBottomRight;
        UIBezierPath *path = [UIBezierPath bezierPath];
        [path moveToPoint:CGPointMake(arrowMinX, _arrowRect.origin.y + _arrowRect.size.height)];
        [path addLineToPoint:CGPointMake(
            _arrowRect.origin.x + _arrowRect.size.width / 2, _arrowRect.origin.y)];
        [path addLineToPoint:CGPointMake(arrowMaxX, _arrowRect.origin.y + _arrowRect.size.height)];
        [path fill];
    }
    NSArray *subviews = self.subviews;
    NSInteger buttonIndex = 0, buttonMaxIndex = [subviews count] - 1;
    for (UIButton *button in subviews)
    {
        CGRect buttonFrame = button.frame;
        UIRectCorner corners = 0;
        if (0 == buttonIndex)
            corners |= leftCorners;
        if (buttonMaxIndex == buttonIndex)
            corners |= rightCorners;
        [[UIBezierPath
            bezierPathWithRoundedRect:buttonFrame
            byRoundingCorners:corners
            cornerRadii:CornerRadii] fill];
        buttonIndex++;
    }
}
@end
