/*
 * DarwinKit/ios/UIPopoverController+StyledAppearance.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>
#import <DarwinKit/ios/DKGraphicsStyle.h>

@implementation UIPopoverController (StyledAppearance)
+ (void)loadStyledAppearance
{
    static BOOL done;
    if (!done)
    {
        [self
            swizzleInstanceMethod:@selector(initWithContentViewController:)
            withMethod:@selector(__swizzle__initWithContentViewController:)];
        [self
            swizzleInstanceMethod:@selector(dealloc)
            withMethod:@selector(__swizzle__dealloc)];
        done = YES;
    }
}
- (id)__swizzle__initWithContentViewController:(UIViewController *)controller
{
    self = [self __swizzle__initWithContentViewController:controller];
    if (nil != self)
    {
        [DKGraphicsStyle addDelegate:self];
        self.backgroundColor = [DKCurrentGraphicsStyle styledBackgroundColor];
    }
    return self;
}
- (void)__swizzle__dealloc
{
    [DKGraphicsStyle removeDelegate:self];
    [self __swizzle__dealloc];
}
- (void)graphicsStyleDidChange:(DKGraphicsStyle *)graphicsStyle
{
    self.backgroundColor = [DKCurrentGraphicsStyle styledBackgroundColor];
}
@end
