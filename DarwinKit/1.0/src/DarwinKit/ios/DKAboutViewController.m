/*
 * DarwinKit/ios/DKAboutViewController.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>
#import <DarwinKit/ios/DKUtility.h>

#define ApplicationIconControl          'I'
#define ApplicationNameControl          'N'
#define ApplicationVersionControl       'V'
#define CopyrightControl                'C'
#define CompanyLogoControl              'L'
#define InformationButtonControl        'B'
#define InformationViewControl          'W'

@interface DKAboutViewController ()
@property (readwrite, retain, nonatomic) IBOutlet UIView *informationView;
- (IBAction)informationButtonAction:(id)sender;
@end
@implementation DKAboutViewController
{
    NSDictionary *_options;
}
- (id)init
{
    return [self initWithOptions:nil];
}
- (id)initWithOptions:(NSDictionary *)options
{
    self = [super initWithNibName:@"DKAboutView" bundle:[DKUtility bundle]];
    if (nil != self)
        _options = [options retain];
    return self;
}
- (void)dealloc
{
    self.informationView = nil;
    [_options release];
    [super dealloc];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([self respondsToSelector:@selector(topLayoutGuide)])
        [self.view
            addConstraints:[NSLayoutConstraint
                constraintsWithVisualFormat:@"V:[guide][view]"
                options:0
                metrics:nil
                views:[NSDictionary
                    dictionaryWithObjectsAndKeys:
                        self.topLayoutGuide, @"guide",
                        [[self.view subviews] objectAtIndex:0], @"view",
                        nil]]];
    /* based on the Mac version, which was based on Technical Note TN2179 */
    self.title = [[DKUtility bundle]
        localizedStringForKey:@"DKAboutViewController.title" value:@"About" table:nil];
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *appIcon = [_options objectForKey:@"ApplicationIcon"];
    if (nil == appIcon)
    {
        appIcon = [[[[bundle objectForInfoDictionaryKey:@"CFBundleIcons"]
            objectForKey:@"CFBundlePrimaryIcon"] objectForKey:@"CFBundleIconFiles"] lastObject];
        if (nil == appIcon)
            appIcon = [[bundle objectForInfoDictionaryKey:@"CFBundleIconFiles"] lastObject];
        if (nil == appIcon)
            appIcon = [bundle objectForInfoDictionaryKey:@"CFBundleIconFile"];
        if (nil == appIcon)
            appIcon = @"AppIcon";
    }
    NSString *appName = [_options objectForKey:@"ApplicationName"];
    if (nil == appName)
    {
        appName = [bundle objectForInfoDictionaryKey:@"CFBundleName"];
        if (nil == appName)
            appName = [[NSProcessInfo processInfo] processName];
    }
    NSString *appBuild = [_options objectForKey:@"Version"];
    if (nil == appBuild)
    {
        appBuild = [bundle objectForInfoDictionaryKey:@"MyBuildRevision"];
        if (nil == appBuild)
            appBuild = [bundle objectForInfoDictionaryKey:@"CFBundleVersion"];
    }
    NSString *appVersion = [_options objectForKey:@"ApplicationVersion"];
    if (nil == appVersion)
    {
        appVersion = [bundle objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
        if (nil == appVersion)
            appVersion = appBuild;
    }
    if ([appVersion isEqualToString:appBuild])
        appBuild = nil;
    if (nil != appVersion &&
        0 == [appVersion rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]].location)
        appVersion = [@"Version " stringByAppendingString:appVersion];
    if (nil != appVersion && nil != appBuild)
        appVersion = [NSString stringWithFormat:@"%@ (%@)", appVersion, appBuild];
    NSString *copyright = [_options objectForKey:@"Copyright"];
    if (nil == copyright)
        copyright = [bundle objectForInfoDictionaryKey:@"NSHumanReadableCopyright"];
    BOOL hasCredits =
        [bundle URLForResource:@"Credits" withExtension:@"html"] ||
        [bundle URLForResource:@"Credits" withExtension:@"pdf"];
    UIView *contentView = self.view;
    UIView *appIconView = [contentView viewWithTag:ApplicationIconControl];
    appIconView.layer.cornerRadius = round(appIconView.bounds.size.width / 4.5); /* approx. iOS7 shape */
    appIconView.layer.masksToBounds = YES;
    [(id)appIconView setImage:[UIImage imageNamed:appIcon]];
    [(id)[contentView viewWithTag:ApplicationNameControl] setText:appName];
    [(id)[contentView viewWithTag:ApplicationVersionControl] setText:appVersion];
    [(id)[contentView viewWithTag:CopyrightControl] setText:copyright];
    [(id)[contentView viewWithTag:InformationButtonControl] setHidden:!hasCredits];
}
- (IBAction)informationButtonAction:(id)sender
{
    NSBundle *bundle = [NSBundle mainBundle];
    NSURL *url;
    if ((url = [bundle URLForResource:@"Credits" withExtension:@"html"]) ||
        (url = [bundle URLForResource:@"Credits" withExtension:@"pdf"]))
    {
        UIViewController *controller = [[UIViewController alloc] init];
        controller.view = self.informationView;
        controller.title = [[DKUtility bundle]
            localizedStringForKey:@"DKAboutViewController.informationTitle" value:@"Information" table:nil];
        [(id)[controller.view viewWithTag:InformationViewControl]
            loadRequest:[NSURLRequest requestWithURL:url]];
        [self.navigationController pushViewController:controller animated:YES];
        [controller release];
    }
}
- (IBAction)doneButtonAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (void)presentOverViewController:(UIViewController *)controller
    animated:(BOOL)animated
{
    UIBarButtonItem *doneItem = [[UIBarButtonItem alloc]
        initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonAction:)];
    self.navigationItem.rightBarButtonItem = doneItem;
    [doneItem release];
    UINavigationController *navigationController = [[UINavigationController alloc]
        initWithRootViewController:self];
    navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
    [controller presentViewController:navigationController animated:animated completion:NULL];
    [navigationController release];
}
@end
