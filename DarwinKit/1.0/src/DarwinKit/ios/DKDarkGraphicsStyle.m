/*
 * DarwinKit/ios/DKDarkGraphicsStyle.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>
#import <DarwinKit/ios/DKDarkGraphicsStyle.h>

@implementation DKDarkGraphicsStyle
{
    UIColor *_styledBackgroundColor;
    UIColor *_styledContentBackgroundColor;
    UIColor *_styledTextColor;
    UIColor *_styledHighlightedTextColor;
    UIColor *_styledPlaceholderTextColor;
    NSDictionary *_styledBackgroundColors;
    NSDictionary *_styledTextColors;
}
+ (void)load
{
    [self registerGraphicsStyleClass:self forName:@"Dark"];
}
- (id)init
{
    self = [super init];
    if (nil != self)
    {
        _styledBackgroundColor = [[UIColor alloc] initWithWhite:0.25 alpha:1.0];
        _styledContentBackgroundColor = [[UIColor alloc] initWithWhite:0.15 alpha:1.0];
        _styledTextColor = [[UIColor lightTextColor] retain];
        _styledHighlightedTextColor = [[UIColor darkTextColor] retain];
        _styledPlaceholderTextColor = [[UIColor darkGrayColor] retain];
        _styledBackgroundColors = [[NSDictionary alloc] initWithObjectsAndKeys:
            _styledBackgroundColor, [UIColor groupTableViewBackgroundColor],
            _styledContentBackgroundColor, [UIColor whiteColor],
            _styledContentBackgroundColor, [UIColor colorWithWhite:1 alpha:1],
            _styledContentBackgroundColor, [UIColor colorWithRed:1 green:1 blue:1 alpha:1],
            nil];
        _styledTextColors = [[NSDictionary alloc] initWithObjectsAndKeys:
            _styledTextColor, [UIColor darkTextColor],
            _styledTextColor, [UIColor blackColor],
            _styledTextColor, [UIColor colorWithRed:0 green:0 blue:0 alpha:1],
            nil];
    }
    return self;
}
- (void)dealloc
{
    [_styledTextColors release];
    [_styledBackgroundColors release];
    [_styledPlaceholderTextColor release];
    [_styledHighlightedTextColor release];
    [_styledTextColor release];
    [_styledContentBackgroundColor release];
    [_styledBackgroundColor release];
    [super dealloc];
}
- (UIActionSheetStyle)styledActionSheetStyle
{
    return UIActionSheetStyleBlackOpaque;
}
- (UIBarStyle)styledBarStyle
{
    return UIBarStyleBlack;
}
- (UIKeyboardAppearance)styledKeyboardAppearance
{
    return UIKeyboardAppearanceDark;
}
- (UIColor *)styledBackgroundColor
{
    return _styledBackgroundColor;
}
- (UIColor *)styledContentBackgroundColor
{
    return _styledContentBackgroundColor;
}
- (UIColor *)styledTextColor
{
    return _styledTextColor;
}
- (UIColor *)styledHighlightedTextColor
{
    return _styledHighlightedTextColor;
}
- (UIColor *)styledPlaceholderTextColor
{
    return _styledPlaceholderTextColor;
}
- (UIColor *)getStyledBackgroundColor:(UIColor *)color
{
    UIColor *result = [_styledBackgroundColors objectForKey:color];
    if (nil == result)
        result = color;
    return result;
}
- (UIColor *)getStyledTextColor:(UIColor *)color
{
    UIColor *result = [_styledTextColors objectForKey:color];
    if (nil == result)
        result = color;
    return result;
}
@end
