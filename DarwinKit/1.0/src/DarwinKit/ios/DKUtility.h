/*
 * DarwinKit/ios/DKUtility.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_IOS_DKUTILITY_H_INCLUDED
#define DARWINKIT_IOS_DKUTILITY_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

@interface DKUtility : NSObject
+ (NSBundle *)bundle;
+ (UIImage *)imageNamed:(NSString *)name;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_IOS_DKUTILITY_H_INCLUDED
