/*
 * DarwinKit/ios/DKActionViewController.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>
#import <DarwinKit/ios/DKPopoverViewController+Subclass.h>

@implementation DKActionViewController
{
    NSMutableArray *_enabledGestureRecognizers;
}
- (void)dealloc
{
    [_enabledGestureRecognizers release];
    [super dealloc];
}
- (void)phoneIdiomPresentOverViewController:(UIViewController *)controller animated:(BOOL)animated
{
    UIViewController *containerViewController = [self containerViewController];
    if (nil != containerViewController.parentViewController)
        return;
    [controller addChildViewController:containerViewController];
    [self phoneIdiomDisableGestureRecognizers:controller];
    UIView *controllerView = controller.view;
    CGRect contentFrame = controllerView.bounds;
    UIControl *contentView = [[[UIControl alloc] initWithFrame:contentFrame] autorelease];
    contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    contentView.autoresizesSubviews = YES;
    contentView.opaque = NO;
    contentView.backgroundColor = [UIColor colorWithWhite:0 alpha:0];
    [contentView
        addTarget:self
        action:@selector(phoneIdiomDismissViewControllerFromContainerView:)
        forControlEvents:UIControlEventTouchUpInside];
    UIView *view = containerViewController.view;
    CGRect viewFrame = view.frame;
    viewFrame.origin.y = contentFrame.size.height - viewFrame.size.height;
    viewFrame.size.width = contentFrame.size.width;
    CGRect initialViewFrame = viewFrame;
    initialViewFrame.origin.y = contentFrame.size.height;
    view.frame = initialViewFrame;
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    [controllerView addSubview:contentView];
    if (![controller shouldAutomaticallyForwardAppearanceMethods])
        [containerViewController beginAppearanceTransition:YES animated:animated];
    [contentView addSubview:view];
    [UIView animateWithDuration:animated ? 0.4 : 0 animations:^
        {
            contentView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
            view.frame = viewFrame;
        }
        completion:^(BOOL finished)
        {
            if (![controller shouldAutomaticallyForwardAppearanceMethods])
                [containerViewController endAppearanceTransition];
            [containerViewController didMoveToParentViewController:controller];
                /* doing this at completion avoids multiple didMoveToParentViewController: messages] */
        }];
}
- (void)phoneIdiomDismissViewControllerAnimated:(BOOL)animated
{
    UIViewController *containerViewController = [self containerViewController];
    if (nil == containerViewController.parentViewController)
        return;
    UIViewController *controller = containerViewController.parentViewController;
    [containerViewController willMoveToParentViewController:nil];
    UIView *view = containerViewController.view;
    UIView *contentView = view.superview;
    if (![controller shouldAutomaticallyForwardAppearanceMethods])
        [containerViewController beginAppearanceTransition:NO animated:animated];
    [UIView animateWithDuration:animated ? 0.4 : 0 animations:^
        {
            contentView.backgroundColor = [UIColor colorWithWhite:0 alpha:0];
            CGRect contentFrame = contentView.bounds;
            CGRect viewFrame = view.frame;
            viewFrame.origin.y = contentFrame.size.height;
            view.frame = viewFrame;
        }
        completion:^(BOOL finished)
        {
            [view removeFromSuperview];
            [contentView removeFromSuperview];
            if (![controller shouldAutomaticallyForwardAppearanceMethods])
                [containerViewController endAppearanceTransition];
            [self phoneIdiomEnableGestureRecognizers];
            [containerViewController
                performSelector:@selector(removeFromParentViewController) withObject:nil afterDelay:0];
                /* doing this at completion avoids multiple didMoveToParentViewController: messages] */
        }];
}
- (void)phoneIdiomDismissViewControllerFromContainerView:(id)sender
{
    [self phoneIdiomDismissViewControllerAnimated:YES];
}
- (void)phoneIdiomEnableGestureRecognizers
{
    for (UIGestureRecognizer *recognizer in _enabledGestureRecognizers)
        recognizer.enabled = YES;
    [_enabledGestureRecognizers release];
    _enabledGestureRecognizers = nil;
}
- (void)phoneIdiomDisableGestureRecognizers:(UIViewController *)controller
{
    [_enabledGestureRecognizers release];
    _enabledGestureRecognizers = [[NSMutableArray alloc] init];
    for (UIGestureRecognizer *recognizer in controller.view.gestureRecognizers)
        if (recognizer.enabled)
        {
            [_enabledGestureRecognizers addObject:recognizer];
            recognizer.enabled = NO;
        }
}
#if 0
- (void)viewWillAppear:(BOOL)animated
{
    NSLog(@"viewWillAppear: %d", animated);
    [super viewWillAppear:animated];
}
- (void)viewDidAppear:(BOOL)animated
{
    NSLog(@"viewDidAppear: %d", animated);
    [super viewDidAppear:animated];
}
- (void)viewWillDisappear:(BOOL)animated
{
    NSLog(@"viewWillDisappear: %d", animated);
    [super viewWillDisappear:animated];
}
- (void)viewDidDisappear:(BOOL)animated
{
    NSLog(@"viewDidDisappear: %d", animated);
    [super viewDidDisappear:animated];
}
- (void)willMoveToParentViewController:(UIViewController *)parentController
{
    NSLog(@"willMoveToParentViewController: %@", parentController);
    [super willMoveToParentViewController:parentController];
}
- (void)didMoveToParentViewController:(UIViewController *)parentController
{
    NSLog(@"didMoveToParentViewController: %@", parentController);
    [super didMoveToParentViewController:parentController];
}
#endif
@end
