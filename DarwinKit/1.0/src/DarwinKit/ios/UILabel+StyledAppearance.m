/*
 * DarwinKit/ios/UILabel+StyledAppearance.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>
#import <DarwinKit/ios/DKGraphicsStyle.h>

@implementation UILabel (StyledAppearance)
+ (void)loadStyledAppearance
{
    static BOOL done;
    if (!done)
    {
        [self
            swizzleInstanceMethod:@selector(drawTextInRect:)
            withMethod:@selector(__swizzle__drawTextInRect:)];
        done = YES;
    }
}
- (void)__swizzle__drawTextInRect:(CGRect)rect
{
    if ([self shouldAppearStyled])
    {
        UIColor *textColor = self.textColor;
        self.textColor = !self.highlighted ?
            [DKCurrentGraphicsStyle getStyledTextColor:textColor] :
            [DKCurrentGraphicsStyle styledHighlightedTextColor];
        [self __swizzle__drawTextInRect:rect];
        self.textColor = textColor;
    }
    else
        [self __swizzle__drawTextInRect:rect];
}
@end
