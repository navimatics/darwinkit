/*
 * DarwinKit/ios/DKProgressHUD.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>
#import <DarwinKit/ios/DKOverlayWindow.h>
#import <DarwinKit/ios/DKUtility.h>

#define DefaultSpacing                  [NSNumber numberWithFloat:20.0f]

@implementation DKProgressHUD
{
    UIWindow *_window;
    BOOL _animated;
}
+ (DKProgressHUD *)sharedInstance
{
    static DKProgressHUD *sharedInstance;
    if (nil == sharedInstance)
        sharedInstance = [[self alloc] init];
    return sharedInstance;
}
+ (NSTimeInterval)durationFromMessage:(NSString *)message
{
    return MAX(1.0, MIN(5.0, message.length * 0.06 + 0.3));
}
+ (void)showWithMessage:(NSString *)message allowUserInteraction:(BOOL)flag
{
    DKProgressHUD *instance = [DKProgressHUD sharedInstance];
    instance.showsActivityIndicator = YES;
    instance.image = nil;
    instance.message = message;
    instance.userInteractionEnabled = !flag;
    [instance showWithDuration:INFINITY animated:YES];
}
+ (void)showSuccessWithMessage:(NSString *)message allowUserInteraction:(BOOL)flag
{
    DKProgressHUD *instance = [DKProgressHUD sharedInstance];
    instance.showsActivityIndicator = NO;
    instance.image = [DKUtility imageNamed:@"DKProgressHUDSuccess"];
    instance.message = message;
    instance.userInteractionEnabled = !flag;
    [instance showWithDuration:[self durationFromMessage:message] animated:YES];
}
+ (void)showFailureWithMessage:(NSString *)message allowUserInteraction:(BOOL)flag
{
    DKProgressHUD *instance = [DKProgressHUD sharedInstance];
    instance.showsActivityIndicator = NO;
    instance.image = [DKUtility imageNamed:@"DKProgressHUDFailure"];
    instance.message = message;
    instance.userInteractionEnabled = !flag;
    [instance showWithDuration:[self durationFromMessage:message] animated:YES];
}
- (id)init
{
    self = [super initWithFrame:CGRectZero];
    if (nil != self)
    {
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        self.opaque = NO;
        self.backgroundColor = [UIColor clearColor];
        NSArray *nibObjects = [[UINib nibWithNibName:@"DKProgressHUDContentView" bundle:[DKUtility bundle]]
            instantiateWithOwner:self options:nil];
        UIView *contentView = [nibObjects lastObject]/* there is only one */;
        contentView.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:contentView];
        [self addConstraint:[NSLayoutConstraint
            constraintWithItem:self
            attribute:NSLayoutAttributeCenterX
            relatedBy:NSLayoutRelationEqual
            toItem:contentView
            attribute:NSLayoutAttributeCenterX
            multiplier:1
            constant:0]];
        [self addConstraint:[NSLayoutConstraint
            constraintWithItem:self
            attribute:NSLayoutAttributeCenterY
            relatedBy:NSLayoutRelationEqual
            toItem:contentView
            attribute:NSLayoutAttributeCenterY
            multiplier:1
            constant:0]];
        [[NSNotificationCenter defaultCenter]
            addObserver:self
            selector:@selector(didChangeStatusBarOrientation:)
            name:UIApplicationDidChangeStatusBarOrientationNotification
            object:nil];
    }
    return self;
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [_window release]; _window = nil;
    self.image = nil;
    self.message = nil;
    [super dealloc];
}
- (void)didChangeStatusBarOrientation:(NSNotification *)notification
{
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    CGFloat angle = 0;
    switch (orientation)
    {
    case UIInterfaceOrientationPortrait:
        break;
    case UIInterfaceOrientationPortraitUpsideDown:
        angle = M_PI;
        break;
    case UIInterfaceOrientationLandscapeLeft:
        angle = -M_PI_2;
        break;
    case UIInterfaceOrientationLandscapeRight:
        angle = +M_PI_2;
        break;
    }
    self.transform = CGAffineTransformMakeRotation(angle);
}
- (void)showWithDuration:(NSTimeInterval)duration animated:(BOOL)animated
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(dismissNow) object:nil];
    if (self.showsActivityIndicator)
    {
        UIActivityIndicatorView *activityIndicator = (id)[self viewWithClass:[UIActivityIndicatorView class] tag:'A'];
        UIImageView *imageView = (id)[self viewWithClass:[UIImageView class] tag:'I'];
        [activityIndicator startAnimating];
        imageView.hidden = YES;
        imageView.image = nil;
    }
    else
    {
        UIActivityIndicatorView *activityIndicator = (id)[self viewWithClass:[UIActivityIndicatorView class] tag:'A'];
        UIImageView *imageView = (id)[self viewWithClass:[UIImageView class] tag:'I'];
        [activityIndicator stopAnimating];
        imageView.hidden = NO;
        imageView.image = self.image;
    }
    UILabel *label = (id)[self viewWithClass:[UILabel class] tag:'L'];
    label.text = @"";
    label.text = self.message;
    if (nil == _window)
    {
        [[NSNotificationCenter defaultCenter]
            postNotificationName:DKProgressHUDWillAppearNotification object:self];
        _window = [[DKOverlayWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        _window.windowLevel = (UIWindowLevelNormal + UIWindowLevelAlert) / 2.0;
        _window.userInteractionEnabled = self.userInteractionEnabled;
        self.center = _window.center;
        self.bounds = _window.bounds;
        [_window addSubview:self];
        [_window makeKeyAndVisible];
        [self didChangeStatusBarOrientation:nil];
        UIView *contentView = [self.subviews lastObject];
        contentView.alpha = 0;
        _window.backgroundColor = [UIColor clearColor];
        _animated = animated;
        [UIView animateWithDuration:animated ? 0.4 : 0
            animations:^
            {
                contentView.alpha = 1;
                if (self.userInteractionEnabled)
                    _window.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
            }
            completion:^(BOOL finished)
            {
                [[NSNotificationCenter defaultCenter]
                    postNotificationName:DKProgressHUDDidAppearNotification object:self];
            }];
    }
    else if (_window.userInteractionEnabled != self.userInteractionEnabled)
    {
        _window.userInteractionEnabled = self.userInteractionEnabled;
        _animated = animated;
        [UIView animateWithDuration:animated ? 0.4 : 0
            animations:^
            {
                if (!self.userInteractionEnabled)
                    _window.backgroundColor = [UIColor clearColor];
                else
                    _window.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
            }
            completion:NULL];
    }
    if (INFINITY != duration)
        [self performSelector:@selector(dismissNow) withObject:nil afterDelay:duration];
}
- (void)dismissAnimated:(BOOL)animated
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(dismissNow) object:nil];
    UIView *contentView = [self.subviews lastObject];
    [[NSNotificationCenter defaultCenter]
        postNotificationName:DKProgressHUDWillDisappearNotification object:self];
    [UIView animateWithDuration:animated ? 0.4 : 0
        animations:^
        {
            contentView.alpha = 0;
            _window.backgroundColor = [UIColor clearColor];
        }
        completion:^(BOOL finished)
        {
            _window.hidden = YES;
            [self removeFromSuperview];
            [_window release]; _window = nil;
            [[NSNotificationCenter defaultCenter]
                postNotificationName:DKProgressHUDDidDisappearNotification object:self];
        }];
}
- (void)dismissNow
{
    [self dismissAnimated:_animated];
}
@synthesize showsActivityIndicator = _showsActivityIndicator;
@synthesize image = _image;
@synthesize message = _message;
@end

NSString *const DKProgressHUDWillAppearNotification = @"DKProgressHUDWillAppearNotification";
NSString *const DKProgressHUDDidAppearNotification = @"DKProgressHUDDidAppearNotification";
NSString *const DKProgressHUDWillDisappearNotification = @"DKProgressHUDWillDisappearNotification";
NSString *const DKProgressHUDDidDisappearNotification = @"DKProgressHUDDidDisappearNotification";
