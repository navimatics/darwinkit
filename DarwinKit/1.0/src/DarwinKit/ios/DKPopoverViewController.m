/*
 * DarwinKit/ios/DKPopoverViewController.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>

@interface DKPopoverViewController () <UIPopoverControllerDelegate>
@end
@implementation DKPopoverViewController
{
    UIPopoverController *_popover;
}
- (void)dealloc
{
    [_popover release];
    [super dealloc];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.preferredContentSize = self.view.frame.size;
}
- (void)presentOverViewController:(UIViewController *)controller
    animated:(BOOL)animated
{
    if (UIUserInterfaceIdiomPad == UI_USER_INTERFACE_IDIOM())
    {
        UIView *controllerView = controller.view;
        CGRect bounds = controllerView.bounds;
        bounds = CGRectMake(CGRectGetMidX(bounds) - 0.5, CGRectGetMidY(bounds) - 0.5, 1, 1);
        [self padIdiomPresentFromRect:bounds item:controllerView animated:YES];
    }
    else
        [self phoneIdiomPresentOverViewController:controller animated:animated];
}
- (void)presentOverViewController:(UIViewController *)controller
    fromRect:(CGRect)rect animated:(BOOL)animated
{
    if (UIUserInterfaceIdiomPad == UI_USER_INTERFACE_IDIOM())
        [self padIdiomPresentFromRect:rect item:controller.view animated:YES];
    else
        [self phoneIdiomPresentOverViewController:controller animated:animated];
}
- (void)presentOverViewController:(UIViewController *)controller
    fromBarButtonItem:(UIBarButtonItem *)item animated:(BOOL)animated
{
    if (UIUserInterfaceIdiomPad == UI_USER_INTERFACE_IDIOM())
        [self padIdiomPresentFromRect:CGRectNull item:item animated:YES];
    else
        [self phoneIdiomPresentOverViewController:controller animated:animated];
}
- (void)dismissViewControllerAnimated:(BOOL)animated completion: (void (^)(void))completion
{
    if (nil != completion)
        NSLog(@"-[DKPopoverViewController dismissViewControllerAnimated:completion:] only supports NULL completion");
    if (UIUserInterfaceIdiomPad == UI_USER_INTERFACE_IDIOM())
        [self padIdiomDismissViewControllerAnimated:animated];
    else
        [self phoneIdiomDismissViewControllerAnimated:animated];
}
- (UIViewController *)containerViewController
{
    return self;
}
- (UIPopoverController *)padIdiomPopoverController
{
    return _popover;
}
- (void)padIdiomPresentFromRect:(CGRect)rect item:(id)item animated:(BOOL)animated
{
    if (nil == _popover)
    {
        _popover = [[UIPopoverController alloc] initWithContentViewController:[self containerViewController]];
        _popover.delegate = self;
    }
    [UIPopoverController enablePassthroughViews:NO];
    if (!CGRectIsNull(rect))
        [_popover
            presentPopoverFromRect:rect
            inView:item
            permittedArrowDirections:UIPopoverArrowDirectionAny
            animated:animated];
    else
        [_popover
            presentPopoverFromBarButtonItem:item
            permittedArrowDirections:UIPopoverArrowDirectionAny
            animated:animated];
    [UIPopoverController enablePassthroughViews:YES];
}
- (void)padIdiomDismissViewControllerAnimated:(BOOL)animated
{
    [_popover dismissPopoverAnimated:animated];
    [_popover release];
    _popover = nil;
}
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popover
{
    [_popover release];
    _popover = nil;
}
- (void)phoneIdiomPresentOverViewController:(UIViewController *)controller animated:(BOOL)animated
{
    UIViewController *containerViewController = [self containerViewController];
    if (nil != containerViewController.presentingViewController)
        return;
    [controller presentViewController:containerViewController animated:animated completion:NULL];
}
- (void)phoneIdiomDismissViewControllerAnimated:(BOOL)animated
{
    UIViewController *containerViewController = [self containerViewController];
    if (nil == containerViewController.presentingViewController)
        return;
    if (self == containerViewController)
        [super dismissViewControllerAnimated:animated completion:NULL];
    else
        [containerViewController dismissViewControllerAnimated:animated completion:NULL];
}
@end
