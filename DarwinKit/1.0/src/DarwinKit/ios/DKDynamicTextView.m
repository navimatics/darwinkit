/*
 * DarwinKit/ios/DKDynamicTextView.m
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>

@implementation DKDynamicTextView
{
    NSUInteger _minNumberOfLines, _maxNumberOfLines;
    CGFloat _minHeight, _maxHeight;
    BOOL _adjustsSizeToFit;
}
- (id)initWithFrame:(CGRect)frame textContainer:(NSTextContainer *)textContainer
{
    self = [super initWithFrame:frame textContainer:textContainer];
    if (nil != self)
        [self commonInit];
    return self;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (nil != self)
        [self commonInit];
    return self;
}
- (id)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (nil != self)
        [self commonInit];
    return self;
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super dealloc];
}
- (void)commonInit
{
    [[NSNotificationCenter defaultCenter]
        addObserver:self
        selector:@selector(maybeSizeToFit)
        name:UITextViewTextDidChangeNotification
        object:self];
    self.scrollEnabled = NO;
    _minNumberOfLines = 1;
    _maxNumberOfLines = 5;
    _adjustsSizeToFit = NO;
}
- (CGFloat)computeHeightForWidth:(CGFloat)width
{
#if 1
    NSLayoutManager *layoutManager = self.layoutManager;
    NSTextContainer *textContainer = self.textContainer;
    CGSize textContainerSize = textContainer.size;
    textContainer.size = CGSizeMake(width, CGFLOAT_MAX);
    [layoutManager ensureLayoutForTextContainer:textContainer];
    CGRect rect = [layoutManager usedRectForTextContainer:textContainer];
    textContainer.size = textContainerSize;
    UIEdgeInsets inset = self.textContainerInset;
    return ceil(rect.size.height + inset.top + inset.bottom);
#else
    /* The size returned is constrained to the screen size */
    return ceil([super sizeThatFits:CGSizeMake(width, CGFLOAT_MAX)].height);
#endif
}
- (void)computeMinMaxHeight
{
    if (0 == _minHeight || 0 == _maxHeight)
    {
        static unichar lines[] =
        {
            '-', '\n', '\n', '\n',
            '\n', '\n', '\n', '\n',
            '\n', '\n', '\n', '\n',
            '\n', '\n', '\n', '\n',
            '\n', '\n', '\n', '\n',
            '\n', '\n', '\n', '\n',
            '\n', '\n', '\n', '\n',
            '\n', '\n', '\n', '\n',
        };
        NSString *text = [super text];
        [super setText:[NSString
            stringWithCharacters:lines
            length:32 > _minNumberOfLines ? _minNumberOfLines : 32]];
        _minHeight = [self computeHeightForWidth:0];
        if (NSUIntegerMax > _maxNumberOfLines)
        {
            [super setText:[NSString
                stringWithCharacters:lines
                length:32 > _maxNumberOfLines ? _maxNumberOfLines : 32]];
            _maxHeight = [self computeHeightForWidth:0];
        }
        else
            _maxHeight = +INFINITY;
        [super setText:text];
    }
}
- (void)didMoveToWindow
{
    [super didMoveToWindow];
    if (nil != self.window)
        [self performSelector:@selector(maybeSizeToFit) withObject:nil afterDelay:0];
}
- (CGSize)sizeThatFits:(CGSize)size
{
    if (nil == self.window)
        return CGSizeZero;
    [self computeMinMaxHeight];
    size.height = [self computeHeightForWidth:size.width];
    if (!self.hasText)
        size.height = _minHeight;
    else if (size.height < _minHeight)
        size.height = _minHeight;
    else if (size.height > _maxHeight)
        size.height = _maxHeight;
    return size;
}
- (void)sizeToFit
{
    /* do not call super! */
    if (nil == self.window)
        return;
    CGRect bounds = self.bounds;
    CGSize size = [self sizeThatFits:bounds.size];
    if (bounds.size.height != size.height)
    {
        bounds.size.height = size.height;
        self.bounds = bounds;
    }
    CGFloat height = [self computeHeightForWidth:bounds.size.width];
    self.scrollEnabled = height > _maxHeight;
}
- (void)maybeSizeToFit
{
    if (_adjustsSizeToFit)
        [self sizeToFit];
}
- (void)setText:(NSString *)value
{
    [super setText:value];
    [self maybeSizeToFit];
}
- (void)setFont:(UIFont *)value
{
    [super setFont:value];
    _minHeight = _maxHeight = 0;
    [self maybeSizeToFit];
}
- (void)setMinNumberOfLines:(NSUInteger)value
{
    if (_minNumberOfLines == value)
        return;
    _minNumberOfLines = value;
    _minHeight = _maxHeight = 0;
    [self maybeSizeToFit];
}
- (void)setMaxNumberOfLines:(NSUInteger)value
{
    if (_maxNumberOfLines == value)
        return;
    _maxNumberOfLines = value;
    _minHeight = _maxHeight = 0;
    [self maybeSizeToFit];
}
- (void)setAdjustsSizeToFit:(BOOL)value
{
    if (_adjustsSizeToFit == value)
        return;
    _adjustsSizeToFit = value;
    [self maybeSizeToFit];
}
@synthesize minNumberOfLines = _minNumberOfLines, maxNumberOfLines = _maxNumberOfLines;
@synthesize adjustsSizeToFit = _adjustsSizeToFit;
@end
