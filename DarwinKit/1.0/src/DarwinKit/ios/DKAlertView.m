/*
 * DarwinKit/ios/DKAlertView.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>

@interface DKAlertView () <UIAlertViewDelegate>
@end
@implementation DKAlertView
{
    id _target; /* weak */
    CFMutableArrayRef _actions;
    id _context;
}
@synthesize context = _context;
- (id)initWithTitle:(NSString *)title message:(NSString *)message
    target:(id)target buttonTitlesAndActions:(NSString *)buttonTitle, ...
{
    self = [super
        initWithTitle:title
        message:message
        delegate:self
        cancelButtonTitle:nil
        otherButtonTitles:nil];
    if (nil != self)
    {
        _target = target;
        _actions = CFArrayCreateMutable(NULL, 0, NULL);
        va_list ap;
        va_start(ap, buttonTitle);
        for (; nil != buttonTitle; buttonTitle = va_arg(ap, NSString *))
        {
            SEL action = va_arg(ap, SEL);
            [self addButtonWithTitle:buttonTitle action:action];
        }
        va_end(ap);
    }
    return self;
}
- (void)dealloc
{
    self.context = nil;
    if (NULL != _actions)
        CFRelease(_actions);
    [super dealloc];
}
- (NSInteger)addButtonWithTitle:(NSString *)buttonTitle
{
    NSInteger result = [super addButtonWithTitle:buttonTitle];
    if (NULL != _actions)
        CFArrayAppendValue(_actions, NULL);
    return result;
}
- (NSInteger)addButtonWithTitle:(NSString *)buttonTitle action:(SEL)action
{
    NSInteger result = [super addButtonWithTitle:buttonTitle];
    if (NULL != _actions)
        CFArrayAppendValue(_actions, action);
    return result;
}
- (void)alertView:(UIAlertView *)sender clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (NULL == _actions)
        return;
    if (0 <= buttonIndex && buttonIndex < CFArrayGetCount(_actions))
    {
        SEL action = (SEL)CFArrayGetValueAtIndex(_actions, buttonIndex);
        if (NULL != action && [_target respondsToSelector:action])
            [_target performSelector:action withObject:self];
    }
}
@end
