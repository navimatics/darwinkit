/*
 * DarwinKit/ios/DKCustomSegue.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>

@implementation DKCustomSegue
- (void)perform
{
    if ([self.sourceViewController respondsToSelector:@selector(performHandlerForCustomSegue:)])
        [self.sourceViewController performHandlerForCustomSegue:self];
}
@end
