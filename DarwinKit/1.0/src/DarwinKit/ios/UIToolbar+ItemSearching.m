/*
 * DarwinKit/ios/UIToolbar+ItemSearching.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>

@implementation UIToolbar (ItemSearching)
- (UIBarItem *)itemWithTag:(int)tag
{
    for (UIBarItem *item in self.items)
        if (tag == item.tag)
            return item;
    return nil;
}
- (UIBarItem *)itemWithView:(UIView *)view
{
    for (UIBarItem *item in self.items)
        if ([item respondsToSelector:@selector(customView)])
            if (view == [(id)item customView])
                return item;
    return nil;
}
@end
