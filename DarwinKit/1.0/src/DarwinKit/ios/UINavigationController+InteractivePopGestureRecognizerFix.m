/*
 * DarwinKit/ios/UINavigationController+InteractivePopGestureRecognizerFix.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>

@implementation UINavigationController (InteractivePopGestureRecognizerFix)
+ (void)loadInteractivePopGestureRecognizerFix
{
    static BOOL done;
    if (!done)
    {
        [[NSNotificationCenter defaultCenter]
            addObserver:self
            selector:@selector(__helper__onViewControllerViewWillAppear:)
            name:UIViewControllerViewWillAppearNotification
            object:nil];
        done = YES;
    }
}
+ (void)__helper__onViewControllerViewWillAppear:(NSNotification *)notification
{
    if ([notification.object isKindOfClass:[UINavigationController class]])
        [notification.object interactivePopGestureRecognizer].enabled = NO;
}
@end
