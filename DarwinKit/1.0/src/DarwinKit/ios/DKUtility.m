/*
 * DarwinKit/ios/DKUtility.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>
#import <DarwinKit/ios/DKUtility.h>

@implementation DKUtility
+ (NSBundle *)bundle
{
    static NSBundle *bundle;
    if (nil == bundle)
    {
        NSString *path = [[[NSBundle mainBundle] resourcePath]
            stringByAppendingPathComponent:@"DarwinKit_ios.bundle"];
        bundle = [NSBundle bundleWithPath:path];
    }
    return bundle;
}
+ (UIImage *)imageNamed:(NSString *)name
{
    NSBundle *bundle = [self bundle];
    NSString *path = [bundle pathForResource:name ofType:@"png"];
    if (nil == path)
        path = [bundle pathForResource:name ofType:@"jpg"];
    if (nil == path)
        path = [bundle pathForResource:name ofType:@"tiff"];
    UIImage *image = [UIImage imageWithContentsOfFile:path];
    if ([name hasSuffix:@"Template"])
        image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    return image;
}
@end
