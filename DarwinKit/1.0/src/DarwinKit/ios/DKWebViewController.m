/*
 * DarwinKit/ios/DKWebViewController.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>
#import <DarwinKit/ios/DKUtility.h>

@interface DKWebViewController ()
@property (readwrite, retain, nonatomic) IBOutlet UIWebView *webView;
@end
@implementation DKWebViewController
- (id)initWithURL:(NSURL *)url
{
    self = [super initWithNibName:@"DKWebView" bundle:[DKUtility bundle]];
    if (nil != self)
    {
        UIView *view = self.view; /* load view and setup outlet connections */
        if ([self respondsToSelector:@selector(topLayoutGuide)])
            [view
                addConstraints:[NSLayoutConstraint
                    constraintsWithVisualFormat:@"V:[guide][view]"
                    options:0
                    metrics:nil
                    views:[NSDictionary
                        dictionaryWithObjectsAndKeys:
                            self.topLayoutGuide, @"guide",
                            [[self.view subviews] objectAtIndex:0], @"view",
                            nil]]];
        if (nil != url)
            [self.webView loadRequest:[NSURLRequest requestWithURL:url]];
    }
    return self;
}
- (void)dealloc
{
    self.webView = nil;
    [super dealloc];
}
- (IBAction)doneButtonAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (void)presentOverViewController:(UIViewController *)controller
    animated:(BOOL)animated
{
    UIBarButtonItem *doneItem = [[UIBarButtonItem alloc]
        initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonAction:)];
    self.navigationItem.rightBarButtonItem = doneItem;
    [doneItem release];
    UINavigationController *navigationController = [[UINavigationController alloc]
        initWithRootViewController:self];
    navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
    [controller presentViewController:navigationController animated:animated completion:NULL];
    [navigationController release];
}
@end
