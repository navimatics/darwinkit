/*
 * DarwinKit/ios/DKProgressView.m
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>

#define STRIP_WIDTH_FACTOR              2.0
#define PATTERN_WIDTH_FACTOR            2.0

@interface DKProgressView_SubLayer : CALayer
@end
@implementation DKProgressView_SubLayer
- (id<CAAction>)actionForKey:(NSString *)key
{
    return [self.superlayer actionForKey:key];
}
@end

@implementation DKProgressView
{
    CALayer *sublayer; /* weak */
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    [self resetSublayer];
}
- (void)setProgressTintColor:(UIColor *)value
{
    [super setProgressTintColor:value];
    [self resetSublayer];
}
- (void)setProgress:(float)progress
{
    [super setProgress:progress];
    [self showSublayer];
}
- (void)setProgress:(float)progress animated:(BOOL)animated
{
    [super setProgress:progress animated:animated];
    [self showSublayer];
}
- (void)resetSublayer
{
    CGRect bounds = self.bounds;
    CGFloat stripWidth = bounds.size.height * STRIP_WIDTH_FACTOR;
    bounds.size.width += stripWidth * PATTERN_WIDTH_FACTOR;
    if (sublayer.bounds.size.width == bounds.size.width &&
        sublayer.bounds.size.height == bounds.size.height)
        return;
    [sublayer removeFromSuperlayer];
    sublayer = [[[DKProgressView_SubLayer alloc] init] autorelease];
    sublayer.hidden = YES;
    sublayer.opaque = NO;
    sublayer.contentsScale = [[UIScreen mainScreen] scale];
    UIGraphicsBeginImageContextWithOptions(bounds.size, NO, sublayer.contentsScale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [self.tintColor set];
    [self.progressTintColor set];
    for (CGFloat x = 0; bounds.size.width > x; x += stripWidth * PATTERN_WIDTH_FACTOR)
    {
        CGContextMoveToPoint(context, x, 0);
        CGContextAddLineToPoint(context, x + stripWidth, 0);
        CGContextAddLineToPoint(context, x + stripWidth * 2, bounds.size.height);
        CGContextAddLineToPoint(context, x + stripWidth, bounds.size.height);
        CGContextClosePath(context);
    }
    CGContextFillPath(context);
    sublayer.frame = CGRectOffset(bounds, -stripWidth * PATTERN_WIDTH_FACTOR, 0);
    sublayer.contents = (id)UIGraphicsGetImageFromCurrentImageContext().CGImage;
    UIGraphicsEndImageContext();
    [self.layer addSublayer:sublayer];
    self.layer.masksToBounds = YES;
    [self showSublayer];
}
- (void)showSublayer
{
    BOOL show = 0 == self.progress;
    if (nil == sublayer || sublayer.hidden == !show)
        return;
    sublayer.hidden = !show;
    [sublayer removeAnimationForKey:@"position.x"];
    if (show)
    {
        CGRect bounds = self.bounds;
        CGFloat stripWidth = bounds.size.height * STRIP_WIDTH_FACTOR;
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position.x"];
        animation.byValue = [NSNumber numberWithDouble:stripWidth * PATTERN_WIDTH_FACTOR];
        animation.repeatCount = HUGE_VALF;
        animation.duration = 0.5;
        [sublayer addAnimation:animation forKey:@"position.x"];
    }
}
@end
