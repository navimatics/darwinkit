/*
 * DarwinKit/ios/UIPopoverController+PassthroughViewsFix.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>

@implementation UIPopoverController (PassthroughViewsFix)
static size_t AssociatedDataKey = sizeof(BOOL);
static int enablePassthroughViewsCount = 0; /* >= 0: enabled; < 0: disabled */
+ (void)load
{
    [self
        swizzleInstanceMethod:@selector(presentPopoverFromBarButtonItem:permittedArrowDirections:animated:)
        withMethod:@selector(__swizzle__presentPopoverFromBarButtonItem:permittedArrowDirections:animated:)];
}
+ (void)enablePassthroughViews:(BOOL)flag
{
    if (flag)
        enablePassthroughViewsCount++;
    else
        enablePassthroughViewsCount--;
}
- (void)__swizzle__presentPopoverFromBarButtonItem:(UIBarButtonItem *)item
    permittedArrowDirections:(UIPopoverArrowDirection)directions
    animated:(BOOL)animated
{
    [self __swizzle__presentPopoverFromBarButtonItem:item permittedArrowDirections:directions animated:animated];
    BOOL *p = [self getAssociatedData:&AssociatedDataKey];
    if (0 > enablePassthroughViewsCount || *p)
    {
        self.passthroughViews = nil;
        *p = YES;
    }
}
@end
