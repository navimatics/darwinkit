/*
 * DarwinKit/ios/DKKeyboardNotificationListener.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>

@implementation DKKeyboardNotificationListener
{
    DKMultiDelegate *_multiDelegate;
    BOOL _isKeyboardVisible;
    CGRect _keyboardFrame;
}
+ (DKKeyboardNotificationListener *)sharedListener
{
    static DKKeyboardNotificationListener *listener;
    if (nil == listener)
        listener = [[DKKeyboardNotificationListener alloc] init];
    return listener;
}
- (id)init
{
    self = [super init];
    if (nil != self)
    {
        _multiDelegate = [[DKMultiDelegate alloc] init];
        _isKeyboardVisible = NO;
        _keyboardFrame = CGRectNull;
        [[NSNotificationCenter defaultCenter]
            addObserver:self
            selector:@selector(keyboardWillShow:)
            name:UIKeyboardWillShowNotification
            object:nil];
        [[NSNotificationCenter defaultCenter]
            addObserver:self
            selector:@selector(keyboardDidShow:)
            name:UIKeyboardDidShowNotification
            object:nil];
        [[NSNotificationCenter defaultCenter]
            addObserver:self
            selector:@selector(keyboardWillHide:)
            name:UIKeyboardWillHideNotification
            object:nil];
        [[NSNotificationCenter defaultCenter]
            addObserver:self
            selector:@selector(keyboardDidHide:)
            name:UIKeyboardDidHideNotification
            object:nil];
    }
    return self;
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [_multiDelegate release];
    [super dealloc];
}
- (void)addDelegate:(id)delegate
{
    [_multiDelegate addDelegate:delegate];
}
- (void)removeDelegate:(id)delegate
{
    [_multiDelegate removeDelegate:delegate];
}
- (BOOL)isKeyboardVisible
{
    return _isKeyboardVisible;
}
- (CGRect)keyboardFrame
{
    return _keyboardFrame;
}
- (void)keyboardWillShow:(NSNotification *)notification;
{
    if ([_multiDelegate respondsToSelector:@selector(keyboardWillShow:)])
        [(id)_multiDelegate keyboardWillShow:notification];
}
- (void)keyboardDidShow:(NSNotification *)notification;
{
    _isKeyboardVisible = YES;
    [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&_keyboardFrame];
    if ([_multiDelegate respondsToSelector:@selector(keyboardDidShow:)])
        [(id)_multiDelegate keyboardDidShow:notification];
}
- (void)keyboardWillHide:(NSNotification *)notification;
{
    if ([_multiDelegate respondsToSelector:@selector(keyboardWillHide:)])
        [(id)_multiDelegate keyboardWillHide:notification];
}
- (void)keyboardDidHide:(NSNotification *)notification;
{
    _isKeyboardVisible = NO;
    _keyboardFrame = CGRectNull;
    if ([_multiDelegate respondsToSelector:@selector(keyboardDidHide:)])
        [(id)_multiDelegate keyboardDidHide:notification];
}
@end
