/*
 * DarwinKit/ios/UIViewController+Notifications.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>

NSString *const UIViewControllerViewWillAppearNotification = @"UIViewControllerViewWillAppearNotification";
NSString *const UIViewControllerViewDidAppearNotification = @"UIViewControllerViewDidAppearNotification";
NSString *const UIViewControllerViewWillDisappearNotification = @"UIViewControllerViewWillDisappearNotification";
NSString *const UIViewControllerViewDidDisappearNotification = @"UIViewControllerViewDidDisappearNotification";

@implementation UIViewController (Notifications)
+ (void)loadNotifications
{
    static BOOL done;
    if (!done)
    {
        [self
            swizzleInstanceMethod:@selector(viewWillAppear:)
            withMethod:@selector(__swizzle__viewWillAppear:)];
        [self
            swizzleInstanceMethod:@selector(viewDidAppear:)
            withMethod:@selector(__swizzle__viewDidAppear:)];
        [self
            swizzleInstanceMethod:@selector(viewWillDisappear:)
            withMethod:@selector(__swizzle__viewWillDisappear:)];
        [self
            swizzleInstanceMethod:@selector(viewDidDisappear:)
            withMethod:@selector(__swizzle__viewDidDisappear:)];
        done = YES;
    }
}
- (void)__swizzle__viewWillAppear:(BOOL)animated
{
    [self __swizzle__viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter]
        postNotificationName:UIViewControllerViewWillAppearNotification object:self];
}
- (void)__swizzle__viewDidAppear:(BOOL)animated
{
    [self __swizzle__viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter]
        postNotificationName:UIViewControllerViewDidAppearNotification object:self];
}
- (void)__swizzle__viewWillDisappear:(BOOL)animated
{
    [self __swizzle__viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter]
        postNotificationName:UIViewControllerViewWillDisappearNotification object:self];
}
- (void)__swizzle__viewDidDisappear:(BOOL)animated
{
    [self __swizzle__viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter]
        postNotificationName:UIViewControllerViewDidDisappearNotification object:self];
}
@end
