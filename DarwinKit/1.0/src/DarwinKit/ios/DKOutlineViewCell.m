/*
 * DarwinKit/ios/DKOutlineViewCell.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>
#import <DarwinKit/ios/DKOutlineView+Private.h>
#import <DarwinKit/ios/DKUtility.h>

#define DisclosureButtonDim             20
#define IndentationMax                  30

@implementation DKOutlineViewCell
{
    BOOL _showsDisclosureButton:1, _disclosed:1;
    UIButton *_disclosureButton;
}
@synthesize showsDisclosureButton = _showsDisclosureButton;
- (void)dealloc
{
    [_disclosureButton release];
    [super dealloc];
}
- (BOOL)showsDisclosureButton
{
    return _showsDisclosureButton;
}
- (void)setShowsDisclosureButton:(BOOL)value
{
    _showsDisclosureButton = value;
    if (nil == _disclosureButton)
        [self setNeedsLayout];
    else
        _disclosureButton.hidden = !_showsDisclosureButton;
}
- (BOOL)isDisclosed
{
    return _disclosed;
}
- (void)setDisclosed:(BOOL)value
{
    _disclosureButton.selected = _disclosed = value;
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    CGFloat indentation = self.indentationLevel * self.indentationWidth;
    if (indentation > IndentationMax)
        indentation = IndentationMax;
    UIView *contentView = self.contentView;
    CGRect contentViewFrame = contentView.frame;
    contentViewFrame.origin.x = indentation + DisclosureButtonDim;
    contentViewFrame.size.width -= contentViewFrame.origin.x;
    contentView.frame = contentViewFrame;
    if (_showsDisclosureButton)
    {
        if (nil == _disclosureButton)
        {
            _disclosureButton = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
            [_disclosureButton
                setImage:[DKUtility imageNamed:@"BtnDisclosure0Template"] forState:UIControlStateNormal];
            [_disclosureButton
                setImage:[DKUtility imageNamed:@"BtnDisclosure1Template"] forState:UIControlStateSelected];
            [_disclosureButton
                addTarget:self
                action:@selector(disclosureButtonAction:)
                forControlEvents:UIControlEventTouchUpInside];
            _disclosureButton.selected = _disclosed;
        }
        UIView *contentSuperview = contentView.superview;
        CGRect contentSuperviewBounds = contentSuperview.bounds;
        _disclosureButton.frame = CGRectIntegral(CGRectMake(
            indentation, (contentSuperviewBounds.size.height - DisclosureButtonDim) / 2,
            DisclosureButtonDim, DisclosureButtonDim));
        [contentSuperview insertSubview:_disclosureButton aboveSubview:contentView];
    }
}
- (void)disclosureButtonAction:(id)sender
{
    _disclosureButton.selected = _disclosed = !_disclosed;
    DKOutlineView *outlineView = (DKOutlineView *)[self superviewWithClass:[DKOutlineView class]];
    [outlineView disclosureButtonActionForCell:self];
}
@end
