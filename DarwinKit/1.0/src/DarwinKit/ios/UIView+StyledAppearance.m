/*
 * DarwinKit/ios/UIView+StyledAppearance.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>
#import <DarwinKit/ios/DKOverlayWindow.h>

@implementation UIView (StyledAppearance)
+ (void)loadStyledAppearance
{
    static BOOL done;
    if (!done)
    {
        [self
            swizzleInstanceMethod:@selector(willMoveToWindow:)
            withMethod:@selector(__swizzle__willMoveToWindow:)];
        done = YES;
    }
}
- (void)__swizzle__willMoveToWindow:(UIWindow *)window
{
    UIWindow *mainWindow = nil;
    NSArray *windows = [[UIApplication sharedApplication] windows];
    if (0 < [windows count])
        mainWindow = [windows objectAtIndex:0];
    if (nil != mainWindow && mainWindow == window)
        [self setNeedsDisplay];
    [self __swizzle__willMoveToWindow:window];
}
- (BOOL)shouldAppearStyled
{
    UIWindow *mainWindow = nil;
    NSArray *windows = [[UIApplication sharedApplication] windows];
    if (0 < [windows count])
        mainWindow = [windows objectAtIndex:0];
    UIWindow *window = self.window;
    return (nil != mainWindow && mainWindow == window) ||
        [window isKindOfClass:[DKOverlayWindow class]];
}
@end
