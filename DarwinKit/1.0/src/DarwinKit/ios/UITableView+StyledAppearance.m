/*
 * DarwinKit/ios/UITableView+StyledAppearance.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>
#import <DarwinKit/ios/DKGraphicsStyle.h>

@implementation UITableView (StyledAppearance)
+ (void)loadStyledAppearance
{
    static BOOL done;
    if (!done)
    {
        [self
            swizzleInstanceMethod:@selector(initWithFrame:style:)
            withMethod:@selector(__swizzle__initWithFrame:style:)];
        [self
            swizzleInstanceMethod:@selector(initWithCoder:)
            withMethod:@selector(__swizzle__initWithCoder:)];
        [self
            swizzleInstanceMethod:@selector(dealloc)
            withMethod:@selector(__swizzle__dealloc)];
        done = YES;
    }
}
- (id)__swizzle__initWithFrame:(CGRect)frame style:(UITableViewStyle)style
{
    self = [self __swizzle__initWithFrame:frame style:style];
    if (nil != self)
    {
        [DKGraphicsStyle addDelegate:self];
        self.backgroundColor = UITableViewStylePlain == self.style ?
            [DKCurrentGraphicsStyle styledContentBackgroundColor] :
            [DKCurrentGraphicsStyle styledBackgroundColor];
    }
    return self;
}
- (id)__swizzle__initWithCoder:(NSCoder *)coder
{
    self = [self __swizzle__initWithCoder:coder];
    if (nil != self)
    {
        [DKGraphicsStyle addDelegate:self];
        self.backgroundColor = UITableViewStylePlain == self.style ?
            [DKCurrentGraphicsStyle styledContentBackgroundColor] :
            [DKCurrentGraphicsStyle styledBackgroundColor];
    }
    return self;
}
- (void)__swizzle__dealloc
{
    [DKGraphicsStyle removeDelegate:self];
    [self __swizzle__dealloc];
}
- (void)graphicsStyleDidChange:(DKGraphicsStyle *)graphicsStyle
{
    self.backgroundColor = UITableViewStylePlain == self.style ?
        [DKCurrentGraphicsStyle styledContentBackgroundColor] :
        [DKCurrentGraphicsStyle styledBackgroundColor];
}
@end
