/*
 * DarwinKit/ios/DKOutlineView.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>

@implementation DKOutlineView
{
    CFMutableDictionaryRef _sectionTrees;
}
@synthesize childrenKeyPath = _childrenKeyPath;
@synthesize disclosedKeyPath = _disclosedKeyPath;
@synthesize childrenAreLeavesKeyPath = _childrenAreLeavesKeyPath;
- (void)dealloc
{
    if (NULL != _sectionTrees)
        CFRelease(_sectionTrees);
    [_childrenAreLeavesKeyPath release];
    [_childrenKeyPath release];
    [_disclosedKeyPath release];
    [super dealloc];
}
- (id)treeForSection:(NSInteger)section
{
    if (NULL != _sectionTrees)
        return CFDictionaryGetValue(_sectionTrees, (void *)section);
    else
        return nil;
}
- (void)setTree:(id)tree forSection:(NSInteger)section
{
    if (NULL == _sectionTrees)
        _sectionTrees = CFDictionaryCreateMutable(NULL, 0, 0, &kCFTypeDictionaryValueCallBacks);
    CFDictionarySetValue(_sectionTrees, (void *)section, tree);
}
- (void)removeTreeForSection:(NSInteger)section
{
    CFDictionaryRemoveValue(_sectionTrees, (void *)section);
}
- (NSInteger)numberOfItemsInSection:(NSInteger)section
{
    id tree = [self treeForSection:section];
    if (nil != tree)
        return [self numberOfItemsInTree:tree];
    else
        return 0;
}
- (NSInteger)numberOfItemsInTree:(id)tree
{
    NSArray *children = [tree valueForKeyPath:_childrenKeyPath];
    NSInteger count = [children count];
    for (id child in children)
        if ([[child valueForKeyPath:_disclosedKeyPath] boolValue])
            count += [self numberOfItemsInTree:child];
    return count;
}
- (id)itemForRowAtIndexPath:(NSIndexPath *)indexPath level:(NSInteger *)plevel
{
    NSInteger section = indexPath.section;
    id tree = [self treeForSection:section];
    if (nil != tree)
    {
        NSInteger count = 0, level;
        if (NULL == plevel)
            plevel = &level;
        *plevel = 0;
        return [self
            itemForRow:indexPath.row
            inTree:tree
            currentCount:&count
            currentLevel:plevel];
    }
    else
        return nil;
}
- (id)itemForRow:(NSInteger)row inTree:(id)tree currentCount:(NSInteger *)pcount currentLevel:(NSInteger *)plevel
{
    NSArray *children = [tree valueForKeyPath:_childrenKeyPath];
    if (nil != _childrenAreLeavesKeyPath && [[tree valueForKeyPath:_childrenAreLeavesKeyPath] boolValue])
    {
        NSInteger count = [children count];
        if (*pcount <= row && row < *pcount + count)
            return [children objectAtIndex:row - *pcount];
        else
        {
            *pcount += count;
            return nil;
        }
    }
    for (id child in children)
    {
        if (*pcount == row)
            return child;
        (*pcount)++;
        if ([[child valueForKeyPath:_disclosedKeyPath] boolValue])
        {
            (*plevel)++;
            id item = [self itemForRow:row inTree:child currentCount:pcount currentLevel:plevel];
            if (nil != item)
                return item;
            (*plevel)--;
        }
    }
    return nil;
}
- (void)disclosureButtonActionForCell:(DKOutlineViewCell *)cell
{
    NSIndexPath *indexPath = [self indexPathForCell:cell];
    if (nil != indexPath)
    {
        BOOL disclosed = cell.disclosed;
        id item = [self itemForRowAtIndexPath:indexPath level:NULL];
        [item setValue:[NSNumber numberWithBool:disclosed] forKeyPath:_disclosedKeyPath];
        NSInteger count = [self numberOfItemsInTree:item];
        NSMutableArray *indexPaths = [NSMutableArray arrayWithCapacity:count];
        for (NSInteger section = indexPath.section, row = indexPath.row + 1, endRow = row + count;
            endRow > row; row++)
            [indexPaths addObject:[NSIndexPath indexPathForRow:row inSection:section]];
        [self beginUpdates];
        if (disclosed)
            [self insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
        else
            [self deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationBottom];
        /* Workaround iOS7 issue:
         * In iOS7 the separator may not get drawn when inserting/deleting rows next
         * to the selected item. Reloading the expanded/collapsed row fixes the issue.
         */
        if (![self.indexPathsForSelectedRows containsObject:indexPath])
            [self
                reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                withRowAnimation:UITableViewRowAnimationNone];
        /* Workaround iOS7 issue end */
        [self endUpdates];
        if ([self.delegate respondsToSelector:@selector(outlineView:disclosureButtonTappedForRowWithIndexPath:)])
            [(id)self.delegate outlineView:self disclosureButtonTappedForRowWithIndexPath:indexPath];
    }
}
@end
