/*
 * DarwinKit/ios/UITextField+StyledAppearance.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>
#import <DarwinKit/ios/DKGraphicsStyle.h>

@implementation UITextField (StyledAppearance)
+ (void)loadStyledAppearance
{
    static BOOL done;
    if (!done)
    {
        [DKGraphicsStyle addDelegate:self];
        [self
            swizzleInstanceMethod:@selector(drawPlaceholderInRect:)
            withMethod:@selector(__swizzle__drawPlaceholderInRect:)];
        [self
            swizzleInstanceMethod:@selector(setTextColor:)
            withMethod:@selector(__swizzle__setTextColor:)];
        done = YES;
    }
}
+ (void)graphicsStyleDidChange:(DKGraphicsStyle *)graphicsStyle
{
    [[self appearance] setTextColor:[UIColor darkTextColor]];
    [[self appearance] setKeyboardAppearance:[DKCurrentGraphicsStyle styledKeyboardAppearance]];
}
- (void)__swizzle__setTextColor:(UIColor *)color
{
    [self __swizzle__setTextColor:[DKCurrentGraphicsStyle getStyledTextColor:color]];
}
- (void)__swizzle__drawPlaceholderInRect:(CGRect)rect
{
    if ([self shouldAppearStyled])
    {
        [[DKCurrentGraphicsStyle styledPlaceholderTextColor] set];
        [self.placeholder
            drawInRect:rect
            withFont:self.font
            lineBreakMode:NSLineBreakByTruncatingTail
            alignment:self.textAlignment];
    }
    else
        [self __swizzle__drawPlaceholderInRect:rect];
}
@end
