/*
 * DarwinKit/ios/UIActionSheet+StyledAppearance.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>
#import <DarwinKit/ios/DKGraphicsStyle.h>

@implementation UIActionSheet (StyledAppearance)
+ (void)loadStyledAppearance
{
    static BOOL done;
    if (!done)
    {
        [DKGraphicsStyle addDelegate:self];
        done = YES;
    }
}
+ (void)graphicsStyleDidChange:(DKGraphicsStyle *)graphicsStyle
{
    [[self appearance] setActionSheetStyle:[DKCurrentGraphicsStyle styledActionSheetStyle]];
}
@end
