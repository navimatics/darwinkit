/*
 * DarwinKit/ios/UIView+ViewSearching.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>

@implementation UIView (ViewSearching)
- (UIView *)superviewWithClass:(Class)cls
{
    UIView *superview = self.superview;
    while (nil != superview && ![superview isKindOfClass:cls])
        superview = superview.superview;
    return superview;
}
- (UIView *)viewWithClass:(Class)cls
{
    if ([self isKindOfClass:cls])
        return self;
    for (UIView *subview in self.subviews)
    {
        UIView *view = [subview viewWithClass:cls];
        if (nil != view)
            return view;
    }
    return nil;
}
- (UIView *)viewWithClass:(Class)cls tag:(int)tag
{
    if ([self isKindOfClass:cls] && self.tag == tag)
        return self;
    for (UIView *subview in self.subviews)
    {
        UIView *view = [subview viewWithClass:cls tag:tag];
        if (nil != view)
            return view;
    }
    return nil;
}
@end
