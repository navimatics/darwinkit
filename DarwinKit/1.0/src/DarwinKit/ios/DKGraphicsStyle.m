/*
 * DarwinKit/ios/DKGraphicsStyle.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>
#import <DarwinKit/ios/DKGraphicsStyle.h>

DKGraphicsStyle *DKCurrentGraphicsStyle;
@implementation DKGraphicsStyle
static NSMutableDictionary *_graphicsStyleClasses;
static DKMultiDelegate *_multiDelegate;
+ (void)load
{
    _graphicsStyleClasses = [[NSMutableDictionary alloc] init];
    _multiDelegate = [[DKMultiDelegate alloc] init];
}
+ (void)registerGraphicsStyleClass:(Class)cls forName:(NSString *)name
{
    [_graphicsStyleClasses setObject:cls forKey:name];
}
+ (DKGraphicsStyle *)currentStyle
{
    return DKCurrentGraphicsStyle;
}
+ (void)setCurrentStyleWithName:(NSString *)name
{
    [DKCurrentGraphicsStyle release];
    DKCurrentGraphicsStyle = [[[_graphicsStyleClasses objectForKey:name] alloc] init];
    if (nil == DKCurrentGraphicsStyle)
        DKCurrentGraphicsStyle = [[[_graphicsStyleClasses objectForKey:@""] alloc] init];
    [(id)_multiDelegate graphicsStyleDidChange:DKCurrentGraphicsStyle];
}
+ (void)addDelegate:(id)delegate
{
    [_multiDelegate addDelegate:delegate];
}
+ (void)removeDelegate:(id)delegate
{
    [_multiDelegate removeDelegate:delegate];
}
@end
