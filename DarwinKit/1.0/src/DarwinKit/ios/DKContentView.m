/*
 * DarwinKit/ios/DKContentView.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>
#import <DarwinKit/ios/DKGraphicsStyle.h>

@implementation DKContentView
+ (void)loadStyledAppearance
{
    static BOOL done;
    if (!done)
    {
        [DKGraphicsStyle addDelegate:self];
        done = YES;
    }
}
+ (void)graphicsStyleDidChange:(DKGraphicsStyle *)graphicsStyle
{
    [[self appearance] setBackgroundColor:[DKCurrentGraphicsStyle styledContentBackgroundColor]];
}
@end
