/*
 * DarwinKit/ios/DKSearchViewController.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>
#import <DarwinKit/ios/DKPopoverViewController+Subclass.h>

@implementation DKSearchViewController
{
    UISearchBar *_searchBar;
    NSMutableArray *_enabledGestureRecognizers;
}
- (void)dealloc
{
    [_enabledGestureRecognizers release];
    [_searchBar release];
    [super dealloc];
}
- (void)presentOverViewController:(UIViewController *)controller
    fromSearchBar:(UISearchBar *)searchBar animated:(BOOL)animated
{
    if (nil == _searchBar)
        _searchBar = [searchBar retain];
    if (UIUserInterfaceIdiomPad == UI_USER_INTERFACE_IDIOM())
    {
        UIToolbar *toolbar = (UIToolbar *)[searchBar superviewWithClass:[UIToolbar class]];
        UIBarItem *item = [toolbar itemWithView:searchBar];
        if (nil != item)
            [self padIdiomPresentFromRect:CGRectNull item:item animated:YES];
        else
            [self padIdiomPresentFromRect:searchBar.bounds item:searchBar animated:YES];
        [self padIdiomPopoverController].passthroughViews = [NSArray arrayWithObject:searchBar];
    }
    else
        [self phoneIdiomPresentOverViewController:controller animated:animated];
}
- (void)dismissViewControllerAnimated:(BOOL)animated completion: (void (^)(void))completion
{
    [super dismissViewControllerAnimated:animated completion:completion];
    [_searchBar release];
    _searchBar = nil;
}
- (UIViewController *)containerViewController
{
    if (0 < [_searchBar.scopeButtonTitles count] && !_searchBar.showsScopeBar)
    {
        UINavigationController *navigationController = self.navigationController;
        if (nil == navigationController)
        {
            navigationController = [[[UINavigationController alloc] initWithRootViewController:self] autorelease];
            UISegmentedControl *scopeBar = [[UISegmentedControl alloc]
                initWithItems:_searchBar.scopeButtonTitles];
            scopeBar.selectedSegmentIndex = _searchBar.selectedScopeButtonIndex;
            CGRect navigationBounds = navigationController.navigationBar.bounds;
            CGRect scopeBarFrame = scopeBar.frame;
            scopeBarFrame.origin.x = navigationBounds.origin.x;
            scopeBarFrame.size.width = navigationBounds.size.width;
            scopeBar.frame = scopeBarFrame;
            [scopeBar
                addTarget:self
                action:@selector(searchScopeDidChange:)
                forControlEvents:UIControlEventValueChanged];
            self.navigationItem.titleView = scopeBar;
            [scopeBar release];
        }
        return navigationController;
    }
    else
        return self;
}
- (void)phoneIdiomPresentOverViewController:(UIViewController *)controller animated:(BOOL)animated
{
    if (nil == _searchBar)
    {
        [super phoneIdiomPresentOverViewController:controller animated:animated];
        return;
    }
    UIViewController *containerViewController = [self containerViewController];
    if (nil != containerViewController.parentViewController)
        return;
    [controller addChildViewController:containerViewController];
    [self phoneIdiomDisableGestureRecognizers:controller];
    UIView *controllerView = controller.view;
    CGRect contentFrame = controllerView.bounds;
    CGRect searchBarFrame = _searchBar.frame;
    CGRect viewFrame = contentFrame;
    viewFrame.origin.y = searchBarFrame.origin.y + searchBarFrame.size.height;
    viewFrame.size.height -= viewFrame.origin.y;
    CGRect initialViewFrame = viewFrame;
    initialViewFrame.origin.y = contentFrame.size.height;
    UIView *view = containerViewController.view;
    view.frame = initialViewFrame;
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    if (![controller shouldAutomaticallyForwardAppearanceMethods])
        [containerViewController beginAppearanceTransition:YES animated:animated];
    [controllerView addSubview:view];
    [UIView animateWithDuration:animated ? 0.4 : 0 animations:^
        {
            view.frame = viewFrame;
        }
        completion:^(BOOL finished)
        {
            if (![controller shouldAutomaticallyForwardAppearanceMethods])
                [containerViewController endAppearanceTransition];
            [containerViewController didMoveToParentViewController:controller];
                /* doing this at completion avoids multiple didMoveToParentViewController: messages] */
        }];
}
- (void)phoneIdiomDismissViewControllerAnimated:(BOOL)animated
{
    if (nil == _searchBar)
    {
        [super phoneIdiomDismissViewControllerAnimated:animated];
        return;
    }
    UIViewController *containerViewController = [self containerViewController];
    if (nil == containerViewController.parentViewController)
        return;
    UIViewController *controller = containerViewController.parentViewController;
    [containerViewController willMoveToParentViewController:nil];
    UIView *view = containerViewController.view;
    UIView *controllerView = view.superview;
    if (![controller shouldAutomaticallyForwardAppearanceMethods])
        [containerViewController beginAppearanceTransition:NO animated:animated];
    [UIView animateWithDuration:animated ? 0.4 : 0 animations:^
        {
            CGRect contentFrame = controllerView.bounds;
            CGRect viewFrame = view.frame;
            viewFrame.origin.y = contentFrame.size.height;
            view.frame = viewFrame;
        }
        completion:^(BOOL finished)
        {
            [view removeFromSuperview];
            if (![controller shouldAutomaticallyForwardAppearanceMethods])
                [containerViewController endAppearanceTransition];
            [self phoneIdiomEnableGestureRecognizers];
            [containerViewController
                performSelector:@selector(removeFromParentViewController) withObject:nil afterDelay:0];
                /* doing this at completion avoids multiple didMoveToParentViewController: messages] */
        }];
}
- (void)phoneIdiomEnableGestureRecognizers
{
    for (UIGestureRecognizer *recognizer in _enabledGestureRecognizers)
        recognizer.enabled = YES;
    [_enabledGestureRecognizers release];
    _enabledGestureRecognizers = nil;
}
- (void)phoneIdiomDisableGestureRecognizers:(UIViewController *)controller
{
    [_enabledGestureRecognizers release];
    _enabledGestureRecognizers = [[NSMutableArray alloc] init];
    for (UIGestureRecognizer *recognizer in controller.view.gestureRecognizers)
        if (recognizer.enabled)
        {
            [_enabledGestureRecognizers addObject:recognizer];
            recognizer.enabled = NO;
        }
}
- (void)searchScopeDidChange:(id)sender
{
    _searchBar.selectedScopeButtonIndex = [sender selectedSegmentIndex];
    if ([_searchBar.delegate respondsToSelector:@selector(searchBar:selectedScopeButtonIndexDidChange:)])
        [_searchBar.delegate
            searchBar:_searchBar selectedScopeButtonIndexDidChange:_searchBar.selectedScopeButtonIndex];
}
#if 0
- (void)viewWillAppear:(BOOL)animated
{
    NSLog(@"viewWillAppear: %d", animated);
    [super viewWillAppear:animated];
}
- (void)viewDidAppear:(BOOL)animated
{
    NSLog(@"viewDidAppear: %d", animated);
    [super viewDidAppear:animated];
}
- (void)viewWillDisappear:(BOOL)animated
{
    NSLog(@"viewWillDisappear: %d", animated);
    [super viewWillDisappear:animated];
}
- (void)viewDidDisappear:(BOOL)animated
{
    NSLog(@"viewDidDisappear: %d", animated);
    [super viewDidDisappear:animated];
}
- (void)willMoveToParentViewController:(UIViewController *)parentController
{
    NSLog(@"willMoveToParentViewController: %@", parentController);
    [super willMoveToParentViewController:parentController];
}
- (void)didMoveToParentViewController:(UIViewController *)parentController
{
    NSLog(@"didMoveToParentViewController: %@", parentController);
    [super didMoveToParentViewController:parentController];
}
#endif
@end
