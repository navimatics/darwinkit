/*
 * DKFileOpenSaveViewController.m
 *
 * Copyright 2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>
#import <DarwinKit/ios/DKUtility.h>

#define TableInset                      15

@interface DKFileOpenSaveRootViewController : UITableViewController
@end

@interface DKFileOpenSaveViewController ()
@property (readwrite, retain, nonatomic) NSURL *URL;
@end
@implementation DKFileOpenSaveViewController
{
    id<DKFileOpenSaveViewControllerDelegate> _delegate;
}
@synthesize prompt = _prompt;
@synthesize message = _message;
@synthesize cancelButtonTitle = _cancelButtonTitle;
@synthesize doneButtonTitle = _doneButtonTitle;
@synthesize directoryURL = _directoryURL;
@synthesize allowedFileTypes = _allowedFileTypes;
@synthesize fileTypeIcons = _fileTypeIcons;
@synthesize showsFileTypes = _showsFileTypes;
@synthesize showsFileInformation = _showsFileInformation;
@synthesize allowsFileDelete = _allowsFileDelete;
@synthesize URL = _URL;
- (id)initWithDelegate:(id<DKFileOpenSaveViewControllerDelegate>)delegate
{
    DKFileOpenSaveRootViewController *rootViewController = [[[DKFileOpenSaveRootViewController alloc]
        init] autorelease];
    self = [super initWithRootViewController:rootViewController];
    if (nil != self)
    {
        _delegate = delegate;
        self.directoryURL = [[NSFileManager defaultManager]
            URLForDirectory:NSDocumentDirectory
            inDomain:NSUserDomainMask
            appropriateForURL:nil
            create:YES
            error:NULL];
    }
    return self;
}
- (void)dealloc
{
    self.URL = nil;
    self.fileTypeIcons = nil;
    self.allowedFileTypes = nil;
    self.directoryURL = nil;
    self.doneButtonTitle = nil;
    self.cancelButtonTitle = nil;
    self.message = nil;
    self.prompt = nil;
    [super dealloc];
}
- (NSMutableArray *)directoryContents
{
    NSArray *urls = [[NSFileManager defaultManager]
        contentsOfDirectoryAtURL:self.directoryURL
        includingPropertiesForKeys:[NSArray arrayWithObjects:
            NSURLIsDirectoryKey, NSURLFileSizeKey, NSURLContentModificationDateKey, nil]
        options:NSDirectoryEnumerationSkipsHiddenFiles
        error:NULL];
    NSMutableArray *contents = [NSMutableArray arrayWithCapacity:[urls count]];
    for (NSURL *url in urls)
    {
        id isdir = nil;
        if ([url isFileURL] &&
            [url getResourceValue:&isdir forKey:NSURLIsDirectoryKey error:NULL] && ![isdir boolValue])
        {
            if (nil != _allowedFileTypes)
            {
                NSString *pathExtension = [[url path] pathExtension];
                for (NSString *type in _allowedFileTypes)
                    if (NSOrderedSame == [type caseInsensitiveCompare:pathExtension])
                    {
                        [contents addObject:url];
                        break;
                    }
            }
            else
                [contents addObject:url];
        }
    }
    [contents sortUsingComparator:^(id obj1, id obj2)
    {
        return [[obj1 path] localizedStandardCompare:[obj2 path]];
    }];
    return contents;
}
- (void)setFileName:(NSString *)name
{
    if (nil == name)
    {
        self.URL = nil;
        return;
    }
    if (0 < [_allowedFileTypes count])
    {
        BOOL found = NO;
        NSString *pathExtension = [name pathExtension];
        for (NSString *type in _allowedFileTypes)
            if (NSOrderedSame == [type caseInsensitiveCompare:pathExtension])
            {
                found = YES;
                break;
            }
        if (!found)
            name = [name stringByAppendingPathExtension:[_allowedFileTypes objectAtIndex:0]];
    }
    NSString *path = [[self.directoryURL path] stringByAppendingPathComponent:name];
    self.URL = [NSURL fileURLWithPath:path isDirectory:NO];
}
- (id)rootViewControllerContents
{
    return nil;
}
- (void)dismissWithResult:(NSInteger)result
{
    [self dismissViewControllerAnimated:YES completion:NULL];
    if ([_delegate respondsToSelector:@selector(fileOpenSaveViewController:dismissedWithResult:)])
        [_delegate fileOpenSaveViewController:self dismissedWithResult:result];
}
@end

@implementation DKFileOpenViewController
- (id)rootViewControllerContents
{
    return [self directoryContents];
}
@end

@implementation DKFileSaveViewController
@end

@interface DKFileOpenSaveRootViewController_TextFieldCell : UITableViewCell
@end
@implementation DKFileOpenSaveRootViewController_TextFieldCell
- (id)initWithReuseIdentifier:(NSString *)ident
{
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ident];
    if (nil != self)
    {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        UITextField *textField = [[UITextField alloc] initWithFrame:CGRectZero];
        //textField.backgroundColor = [UIColor redColor];
        textField.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        textField.font = [UIFont systemFontOfSize:[UIFont systemFontSize]];
        [self.contentView addSubview:textField];
        [textField release];
    }
    return self;
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    UITextField *textField = self.textField;
    CGFloat height = textField.intrinsicContentSize.height;
    CGRect frame = self.contentView.bounds;
    frame.origin.y = (frame.size.height - height) / 2;
    frame.size.height = height;
    frame = CGRectIntegral(frame);
    frame = CGRectInset(frame, TableInset, 0);
    textField.frame = frame;
}
- (UITextField *)textField
{
    return (id)[self.contentView viewWithClass:[UITextField class]];
}
@end
@implementation DKFileOpenSaveRootViewController
{
    NSDateFormatter *_dateFormatter;
    NSMutableArray *_contents;
    NSString *_text;
}
- (id)init
{
    return [super initWithStyle:UITableViewStyleGrouped];
}
- (void)dealloc
{
    [_text release];
    [_contents release];
    [_dateFormatter release];
    [super dealloc];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [_dateFormatter release];
    _dateFormatter = [[NSDateFormatter alloc] init];
    _dateFormatter.dateStyle = NSDateFormatterMediumStyle;
    _dateFormatter.timeStyle = NSDateFormatterShortStyle;
    _dateFormatter.doesRelativeDateFormatting = YES;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    UINavigationItem *navigationItem = self.navigationItem;
    DKFileOpenSaveViewController *controller = (id)self.navigationController;
    if (nil == navigationItem.title)
        navigationItem.title = controller.title;
    if (nil == self.navigationItem.leftBarButtonItem)
    {
        if (nil == controller.cancelButtonTitle)
            navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc]
                initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                target:self
                action:@selector(cancelButtonAction:)] autorelease];
        else
            navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc]
                initWithTitle:controller.cancelButtonTitle
                style:UIBarButtonItemStylePlain
                target:self
                action:@selector(cancelButtonAction:)] autorelease];
    }
    if (nil == self.navigationItem.rightBarButtonItem)
    {
        if (nil == controller.doneButtonTitle)
            navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc]
                initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                target:self
                action:@selector(doneButtonAction:)] autorelease];
        else
            navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc]
                initWithTitle:controller.doneButtonTitle
                style:UIBarButtonItemStyleDone
                target:self
                action:@selector(doneButtonAction:)] autorelease];
    }
    [_contents release];
    _contents = [[controller rootViewControllerContents] retain];
    if (nil == _contents)
    {
        /* Hack to make the keyboard appear when the view controller appears.
         * Create an offscreen text field and make it first responder. This will cause the keyboard
         * to appear with our view controller.
         *
         * See http://stackoverflow.com/questions/2658261/uitextfield-subview-of-uitableviewcell-to-become-first-responder
         */
        UITextField *offscreenTextField = [[[UITextField alloc]
            initWithFrame:CGRectMake(-100, -100, 10, 10)] autorelease];
        offscreenTextField.tag = 0x4F464653/* big-endian "OFFS" */;
        [self.view addSubview:offscreenTextField];
        [offscreenTextField becomeFirstResponder];
    }
    else if (0 == [_contents count])
        [self setupTableHeaderView];
    [self updateDoneButtonEnabledStatus];
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (nil == _contents)
    {
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        [[(id)cell textField] becomeFirstResponder];
        /* Remove the offscreen text field */
        [[self.view viewWithClass:[UITextField class] tag:0x4F464653/* big-endian "OFFS" */] removeFromSuperview];
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (nil == _contents)
        return 1;
    else
        return [_contents count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    if (nil == _contents)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"Save"];
        if (nil == cell)
        {
            cell = [[[DKFileOpenSaveRootViewController_TextFieldCell alloc]
                initWithReuseIdentifier:@"Save"] autorelease];
            UITextField *textField = [(id)cell textField];
            textField.placeholder = [[DKUtility bundle]
                localizedStringForKey:@"DKFileOpenSaveRootViewController.FileNamePlaceholderText"
                value:@""
                table:nil];
            [textField
                addTarget:self
                action:@selector(textFieldDidChange:)
                forControlEvents:UIControlEventEditingChanged];
        }
        [[(id)cell textField] setText:_text];
    }
    else
    {
        DKFileOpenSaveViewController *controller = (id)self.navigationController;
        cell = [tableView dequeueReusableCellWithIdentifier:@"Open"];
        if (nil == cell)
            cell = [[[UITableViewCell alloc]
                initWithStyle:controller.showsFileInformation ? UITableViewCellStyleSubtitle : UITableViewCellStyleDefault
                reuseIdentifier:@"Open"] autorelease];
        NSURL *url = [_contents objectAtIndex:indexPath.row];
        NSString *name = [[url path] lastPathComponent];
        NSString *pathExtension = [name pathExtension];
        if (!controller.showsFileTypes)
            name = [name stringByDeletingPathExtension];
        id size = nil, date = nil;
        [url getResourceValue:&size forKey:NSURLFileSizeKey error:NULL];
        [url getResourceValue:&date forKey:NSURLContentModificationDateKey error:NULL];
        cell.imageView.image = [[controller fileTypeIcons] objectForKey:[pathExtension lowercaseString]];
        if (nil == cell.imageView.image)
            cell.imageView.image = [[controller fileTypeIcons] objectForKey:@""]; /* default if any */
        cell.textLabel.text = name;
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ • %@",
            [NSByteCountFormatter
                stringFromByteCount:[size longLongValue] countStyle:NSByteCountFormatterCountStyleFile],
            [_dateFormatter stringFromDate:date]];
    }
    return cell;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *prompt = [(id)self.navigationController prompt];
    if (nil == _contents)
        return nil != prompt ? prompt : [[DKUtility bundle]
            localizedStringForKey:@"DKFileOpenSaveRootViewController.SavePrompt"
            value:@""
            table:nil];
    else if (0 < [_contents count])
        return nil != prompt ? prompt : [[DKUtility bundle]
            localizedStringForKey:@"DKFileOpenSaveRootViewController.OpenPrompt"
            value:@""
            table:nil];
    else
        return nil;
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    if (nil == _contents || 0 < [_contents count])
        return [(id)self.navigationController message];
    else
        return nil; /* message displayed in tableHeaderView */
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (nil == _contents)
        return;
    else
    {
        DKFileOpenSaveViewController *controller = (id)self.navigationController;
        controller.URL = [_contents objectAtIndex:indexPath.row];
        [self updateDoneButtonEnabledStatus];
    }
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (nil == _contents)
        return NO;
    else
        return [(id)self.navigationController allowsFileDelete];
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
    forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (nil == _contents)
        return;
    else
    {
        NSURL *url = [_contents objectAtIndex:indexPath.row];
        [[NSFileManager defaultManager] removeItemAtURL:url error:NULL];
        [_contents removeObjectAtIndex:indexPath.row];
        if (0 == [_contents count])
        {
            [self setupTableHeaderView];
            [self.tableView reloadData];
        }
        else
            [tableView
                deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                withRowAnimation:UITableViewRowAnimationLeft];
        [self updateDoneButtonEnabledStatus];
    }
}
- (void)setupTableHeaderView
{
    if (0 == [_contents count])
    {
        DKFileOpenSaveViewController *controller = (id)self.navigationController;
        NSString *text = [[DKUtility bundle]
            localizedStringForKey:@"DKFileOpenSaveRootViewController.NoFilesFound"
            value:@"NO FILES FOUND"
            table:nil];
        text = [text stringByAppendingFormat:@"\n\n%@", controller.message];
        CGRect frame = self.tableView.bounds;
        frame.size.height *= 2.0 / 3.0;
            /* this will place our text at 1/3 of the table view height from the top */
        frame = CGRectIntegral(frame);
        UIView *tableHeaderView = [[[UIView alloc] initWithFrame:frame] autorelease];
        tableHeaderView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        frame = CGRectInset(frame, TableInset, 0);
        UILabel *label = [[[UILabel alloc] initWithFrame:frame] autorelease];
        label.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        //label.backgroundColor = [UIColor redColor];
        label.numberOfLines = 0;
        label.font = [UIFont systemFontOfSize:[UIFont systemFontSize]];
        label.textAlignment = NSTextAlignmentCenter;
        label.text = text;
        [tableHeaderView addSubview:label];
        self.tableView.tableHeaderView = tableHeaderView;
    }
}
- (void)textFieldDidChange:(id)sender
{
    [_text release];
    _text = [[sender text] copy];
    [self updateDoneButtonEnabledStatus];
}
- (void)updateDoneButtonEnabledStatus
{
    if (nil == _contents)
        self.navigationItem.rightBarButtonItem.enabled = 0 < [_text length];
    else
        self.navigationItem.rightBarButtonItem.enabled = nil != [self.tableView indexPathForSelectedRow];
}
- (void)cancelButtonAction:(id)sender
{
    DKFileOpenSaveViewController *controller = (id)self.navigationController;
    controller.URL = nil; /* clear controller.URL on Cancel */
    [controller dismissWithResult:0];
}
- (void)doneButtonAction:(id)sender
{
    DKFileOpenSaveViewController *controller = (id)self.navigationController;
    if (nil == _contents)
        [controller setFileName:_text];
    [controller dismissWithResult:1];
}
@end
