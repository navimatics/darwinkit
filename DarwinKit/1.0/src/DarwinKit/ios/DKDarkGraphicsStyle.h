/*
 * DarwinKit/ios/DKDarkGraphicsStyle.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_IOS_DKDARKGRAPHICSSTYLE_H_INCLUDED
#define DARWINKIT_IOS_DKDARKGRAPHICSSTYLE_H_INCLUDED

#import <DarwinKit/ios/DKGraphicsStyle.h>

#ifdef __cplusplus
extern "C" {
#endif

@interface DKDarkGraphicsStyle : DKGraphicsStyle
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_IOS_DKDARKGRAPHICSSTYLE_H_INCLUDED
