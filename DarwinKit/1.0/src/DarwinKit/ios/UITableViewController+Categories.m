/*
 * DarwinKit/ios/UITableViewController+Categories.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>
#import <objc/runtime.h>

@implementation UITableViewController (PrototypeCellSpecification)
static void *specKey = &specKey;
static void *heightsKey = &heightsKey;
- (NSString *)prototypeCellSpecification
{
    NSArray *sections = objc_getAssociatedObject(self, &specKey);
    NSMutableArray *rowSpecs = [NSMutableArray arrayWithCapacity:[sections count]];
    for (NSArray *rows in sections)
        [rowSpecs addObject:[rows componentsJoinedByString:@" "]];
    return [rowSpecs componentsJoinedByString:@"\n"];
}
- (void)setPrototypeCellSpecification:(NSString *)value
{
    NSArray *rowSpecs = [value componentsSeparatedByString:@"\n"];
    NSMutableArray *sections = [NSMutableArray arrayWithCapacity:[rowSpecs count]];
    for (NSString *rowSpec in rowSpecs)
        [sections addObject:[rowSpec componentsSeparatedByString:@" "]];
    objc_setAssociatedObject(self, &specKey, sections, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (NSArray *)prototypeCellIdentifierSections
{
    return objc_getAssociatedObject(self, &specKey);
}
- (NSArray *)prototypeCellIdentifiersInSection:(NSInteger)sectionIndex
{
    NSArray *sections = objc_getAssociatedObject(self, &specKey);
    if (0 <= sectionIndex && sectionIndex < [sections count])
        return [sections objectAtIndex:sectionIndex];
    return nil;
}
- (NSString *)prototypeCellIdentifierForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *sections = objc_getAssociatedObject(self, &specKey);
    NSInteger sectionIndex = indexPath.section;
    if (0 <= sectionIndex && sectionIndex < [sections count])
    {
        NSArray *rows = [sections objectAtIndex:sectionIndex];
        NSInteger rowIndex = indexPath.row;
        NSUInteger rowCount = [rows count];
        NSString *ident = [rows lastObject];
        if ([ident hasSuffix:@"*"])
        {
            if (0 <= rowIndex && rowIndex < rowCount - 1)
                return [rows objectAtIndex:rowIndex];
            else
                return [ident substringToIndex:[ident length] - 1];
        }
        else
        {
            if (0 <= rowIndex && rowIndex < rowCount)
                return [rows objectAtIndex:rowIndex];
        }
    }
    return nil;
}
- (CGFloat)prototypeCellHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *heights = objc_getAssociatedObject(self, &heightsKey);
    if (nil == heights)
    {
        heights = [NSMutableDictionary dictionary];
        objc_setAssociatedObject(self, &heightsKey, heights, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    NSString *ident = [self prototypeCellIdentifierForRowAtIndexPath:indexPath];
    if (nil == ident)
        return UITableViewAutomaticDimension;
    NSNumber *height = [heights objectForKey:ident];
    if (nil == height)
    {
        UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:ident];
        height = [NSNumber numberWithDouble:cell.bounds.size.height];
        [heights setObject:height forKey:ident];
    }
    return [height doubleValue];
}
@end

@implementation UITableViewController (EndsEditingOnTap)
static void *recognizerKey = &recognizerKey;
- (BOOL)endsEditingOnTap
{
    UITapGestureRecognizer *tap = objc_getAssociatedObject(self, &recognizerKey);
    return tap.enabled;
}
- (void)setEndsEditingOnTap:(BOOL)value
{
    UITapGestureRecognizer *tap = objc_getAssociatedObject(self, &recognizerKey);
    if (nil == tap)
    {
        tap = [[[UITapGestureRecognizer alloc]
            initWithTarget:self action:@selector(__category__tableViewTapAction:)] autorelease];
        tap.cancelsTouchesInView = NO;
        [self.tableView addGestureRecognizer:tap];
        objc_setAssociatedObject(self, &recognizerKey, tap, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    tap.enabled = value;
}
- (void)__category__tableViewTapAction:(UITapGestureRecognizer *)recognizer
{
    if (UIGestureRecognizerStateEnded == recognizer.state)
        [self.tableView endEditing:NO];
}
@end

@implementation UITableViewController (RowChecking)
static void *selectionKey = &selectionKey;
- (NSSet *)indexPathsForCheckedRowsInSection:(NSInteger)section
{
    NSMutableDictionary *dict = objc_getAssociatedObject(self, &selectionKey);
    id key = [NSNumber numberWithInteger:section];
    return [dict objectForKey:key];
}
- (void)setIndexPathsForCheckedRows:(NSSet *)indexPaths inSection:(NSInteger)section
{
    NSMutableDictionary *dict = objc_getAssociatedObject(self, &selectionKey);
    if (nil == dict)
    {
        dict = [NSMutableDictionary dictionary];
        objc_setAssociatedObject(self, &selectionKey, dict, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    id key = [NSNumber numberWithInteger:section];
    [dict
        setObject:indexPaths
        forKey:key];
}
- (BOOL)isCheckedRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *dict = objc_getAssociatedObject(self, &selectionKey);
    id key = [NSNumber numberWithInteger:indexPath.section];
    NSSet *indexPaths = [dict objectForKey:key];
    return [indexPaths containsObject:indexPath];
}
- (void)checkRowAtIndexPath:(NSIndexPath *)indexPath allowMultiple:(BOOL)flag
{
    NSMutableDictionary *dict = objc_getAssociatedObject(self, &selectionKey);
    if (nil == dict)
    {
        dict = [NSMutableDictionary dictionary];
        objc_setAssociatedObject(self, &selectionKey, dict, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    id key = [NSNumber numberWithInteger:indexPath.section];
    if (!flag)
    {
        /* exclusive selection */
        NSSet *indexPaths = [[[dict objectForKey:key] retain] autorelease];
        [dict
            setObject:[NSSet setWithObject:indexPath]
            forKey:key];
        for (NSIndexPath *indexPath in indexPaths)
            [self.tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
        [self.tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        /* inclusive selection */
        NSSet *indexPaths = [dict objectForKey:key];
        if (![indexPaths containsObject:indexPath])
        {
            [dict
                setObject:[indexPaths setByAddingObject:indexPath]
                forKey:key];
            [self.tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else
        {
            NSMutableSet *mutableIndexPaths = [NSMutableSet setWithSet:indexPaths];
            [mutableIndexPaths removeObject:indexPath];
            [dict
                setObject:mutableIndexPaths
                forKey:key];
            [self.tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
        }
    }
}
@end
