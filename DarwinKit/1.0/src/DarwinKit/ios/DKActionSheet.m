/*
 * DarwinKit/ios/DKActionSheet.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>

@interface DKActionSheet () <UIActionSheetDelegate, UIGestureRecognizerDelegate>
@end
@implementation DKActionSheet
{
    id _target; /* weak */
    CFMutableArrayRef _actions;
    UIGestureRecognizer *_recognizer;
    id _context;
}
@synthesize dismissedButtonIndex = _dismissedButtonIndex;
@synthesize context = _context;
- (id)initWithTarget:(id)target
{
    self = [super
        initWithTitle:nil
        delegate:self
        cancelButtonTitle:nil
        destructiveButtonTitle:nil
        otherButtonTitles:nil];
    if (nil != self)
    {
        _target = target;
        _actions = CFArrayCreateMutable(NULL, 0, NULL);
        _dismissedButtonIndex = -1;
    }
    return self;
}
- (id)initWithTarget:(id)target buttonTitlesAndActions:(NSString *)buttonTitle, ...
{
    self = [self initWithTarget:target];
    if (nil != self)
    {
        va_list ap;
        va_start(ap, buttonTitle);
        for (; nil != buttonTitle; buttonTitle = va_arg(ap, NSString *))
        {
            SEL action = va_arg(ap, SEL);
            [self addButtonWithTitle:buttonTitle action:action];
        }
        va_end(ap);
    }
    return self;
}
- (void)dealloc
{
    self.context = nil;
    if (nil != _recognizer)
    {
        [self.window removeGestureRecognizer:_recognizer];
            /* this should not be needed, but let's be defensive */
        [_recognizer release];
    }
    if (NULL != _actions)
        CFRelease(_actions);
    [super dealloc];
}
- (NSInteger)addButtonWithTitle:(NSString *)buttonTitle
{
    NSInteger result = [super addButtonWithTitle:buttonTitle];
    if (NULL != _actions)
        CFArrayAppendValue(_actions, NULL);
    return result;
}
- (NSInteger)addButtonWithTitle:(NSString *)buttonTitle action:(SEL)action
{
    NSInteger result = [super addButtonWithTitle:buttonTitle];
    if (NULL != _actions)
        CFArrayAppendValue(_actions, action);
    return result;
}
- (void)showFromToolbar:(UIToolbar *)view
{
    if (UIUserInterfaceIdiomPad == UI_USER_INTERFACE_IDIOM())
    {
        [UIPopoverController enablePassthroughViews:NO];
        [super showFromToolbar:view];
        [UIPopoverController enablePassthroughViews:YES];
    }
    else
        [super showFromToolbar:view];
}
- (void)showFromTabBar:(UITabBar *)view
{
    if (UIUserInterfaceIdiomPad == UI_USER_INTERFACE_IDIOM())
    {
        [UIPopoverController enablePassthroughViews:NO];
        [super showFromTabBar:view];
        [UIPopoverController enablePassthroughViews:YES];
    }
    else
        [super showFromTabBar:view];
}
- (void)showFromBarButtonItem:(UIBarButtonItem *)item animated:(BOOL)animated
{
    if (UIUserInterfaceIdiomPad == UI_USER_INTERFACE_IDIOM())
    {
        [UIPopoverController enablePassthroughViews:NO];
        [super showFromBarButtonItem:item animated:animated];
        [UIPopoverController enablePassthroughViews:YES];
    }
    else
        [super showFromBarButtonItem:item animated:animated];
}
- (void)showFromRect:(CGRect)rect inView:(UIView *)view animated:(BOOL)animated
{
    if (UIUserInterfaceIdiomPad == UI_USER_INTERFACE_IDIOM())
    {
        [UIPopoverController enablePassthroughViews:NO];
        [super showFromRect:rect inView:view animated:animated];
        [UIPopoverController enablePassthroughViews:YES];
    }
    else
        [super showFromRect:rect inView:view animated:animated];
}
- (void)showInView:(UIView *)view
{
    if (UIUserInterfaceIdiomPad == UI_USER_INTERFACE_IDIOM())
    {
        [UIPopoverController enablePassthroughViews:NO];
        [super showInView:view];
        [UIPopoverController enablePassthroughViews:YES];
    }
    else
        [super showInView:view];
}
- (void)actionSheet:(UIActionSheet *)sender didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (NULL == _actions)
        return;
    _dismissedButtonIndex = buttonIndex;
    if (0 <= buttonIndex && buttonIndex < CFArrayGetCount(_actions))
    {
        SEL action = (SEL)CFArrayGetValueAtIndex(_actions, buttonIndex);
        if (NULL != action && [_target respondsToSelector:action])
            [_target performSelector:action withObject:self];
    }
}
- (void)willMoveToWindow:(UIWindow *)window
{
    if (nil != _recognizer)
        [self.window removeGestureRecognizer:_recognizer];
    if (nil == _recognizer)
    {
        _recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
        _recognizer.delegate = self;
    }
    if (nil != _recognizer)
        [window addGestureRecognizer:_recognizer];
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)sender shouldReceiveTouch:(UITouch *)touch
{
    CGPoint point = [touch locationInView:self];
    return !CGRectContainsPoint(self.bounds, point);
}
- (void)handleTap:(UIGestureRecognizer *)sender
{
    if (UIGestureRecognizerStateEnded == sender.state)
        [self dismissWithClickedButtonIndex:self.cancelButtonIndex animated:YES];
}
@end
