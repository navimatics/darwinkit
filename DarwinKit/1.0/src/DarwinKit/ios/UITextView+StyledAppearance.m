/*
 * DarwinKit/ios/UITextView+StyledAppearance.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>
#import <DarwinKit/ios/DKGraphicsStyle.h>

@implementation UITextView (StyledAppearance)
+ (void)loadStyledAppearance
{
    static BOOL done;
    if (!done)
    {
        [DKGraphicsStyle addDelegate:self];
        [self
            swizzleInstanceMethod:@selector(setTextColor:)
            withMethod:@selector(__swizzle__setTextColor:)];
        done = YES;
    }
}
+ (void)graphicsStyleDidChange:(DKGraphicsStyle *)graphicsStyle
{
    [[self appearance] setTextColor:[UIColor darkTextColor]];
    [[self appearance] __helper__setKeyboardAppearance:[DKCurrentGraphicsStyle styledKeyboardAppearance]];
}
- (void)__swizzle__setTextColor:(UIColor *)color
{
    [self __swizzle__setTextColor:[DKCurrentGraphicsStyle getStyledTextColor:color]];
}
- (void)__helper__setKeyboardAppearance:(UIKeyboardAppearance)value
{
    [self setKeyboardAppearance:value];
}
@end
