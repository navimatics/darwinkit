/*
 * DarwinKit/ios/DKOutlineView+Private.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_IOS_DKOUTLINEVIEW_PRIVATE_H_INCLUDED
#define DARWINKIT_IOS_DKOUTLINEVIEW_PRIVATE_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

@interface DKOutlineView (Private)
- (void)disclosureButtonActionForCell:(DKOutlineViewCell *)cell;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_IOS_DKOUTLINEVIEW_PRIVATE_H_INCLUDED
