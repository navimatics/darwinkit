/*
 * DarwinKit/ios/UIBezierPath+Compatibility.m
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#import <DarwinKit/DarwinKit.h>

@implementation UIBezierPath (Compatibility)
+ (void)fillRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextFillRect(context, rect);
}
+ (void)strokeRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextStrokeRect(context, rect);
}
+ (void)clipRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextClipToRect(context, rect);
}
+ (void)strokeLineFromPoint:(CGPoint)point1 toPoint:(CGPoint)point2
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGPoint points[2] = { point1, point2 };
    CGContextStrokeLineSegments(context, points, 2);
}
- (void)lineToPoint:(CGPoint)point
{
    [self addLineToPoint:point];
}
- (void)curveToPoint:(CGPoint)endPoint
    controlPoint1:(CGPoint)controlPoint1
    controlPoint2:(CGPoint)controlPoint2
{
    [self addCurveToPoint:endPoint controlPoint1:controlPoint1 controlPoint2:controlPoint2];
}
- (void)appendBezierPath:(UIBezierPath *)path
{
    [self appendPath:path];
}
@end
