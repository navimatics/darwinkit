/*
 * DarwinKit/ios/DKGraphicsStyle.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_IOS_DKGRAPHICSSTYLE_H_INCLUDED
#define DARWINKIT_IOS_DKGRAPHICSSTYLE_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

@interface DKGraphicsStyle : NSObject
+ (void)registerGraphicsStyleClass:(Class)cls forName:(NSString *)name;
+ (DKGraphicsStyle *)currentStyle;
+ (void)setCurrentStyleWithName:(NSString *)name;
+ (void)addDelegate:(id)delegate;
+ (void)removeDelegate:(id)delegate;
@end
extern DKGraphicsStyle *DKCurrentGraphicsStyle;

@interface DKGraphicsStyle (StyledColors)
- (UIActionSheetStyle)styledActionSheetStyle;
- (UIBarStyle)styledBarStyle;
- (UIKeyboardAppearance)styledKeyboardAppearance;
- (UIColor *)styledBackgroundColor;
- (UIColor *)styledContentBackgroundColor;
- (UIColor *)styledTextColor;
- (UIColor *)styledHighlightedTextColor;
- (UIColor *)styledPlaceholderTextColor;
- (UIColor *)getStyledBackgroundColor:(UIColor *)color;
- (UIColor *)getStyledTextColor:(UIColor *)color;
@end

@protocol DKGraphicsStyleDelegate <NSObject>
@optional
- (void)graphicsStyleDidChange:(DKGraphicsStyle *)graphicsStyle;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_IOS_DKGRAPHICSSTYLE_H_INCLUDED
