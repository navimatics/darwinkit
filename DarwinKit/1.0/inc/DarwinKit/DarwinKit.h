/*
 * DarwinKit/DarwinKit.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_DARWINKIT_H_INCLUDED
#define DARWINKIT_DARWINKIT_H_INCLUDED

#import <DarwinKit/shared/Defines.h>
#import <DarwinKit/shared/DKKeychain.h>
#import <DarwinKit/shared/DKMultiDelegate.h>
#import <DarwinKit/shared/DKUserDefaultsObservationCenter.h>
#import <DarwinKit/shared/NSObject+AssociatedData.h>
#import <DarwinKit/shared/NSObject+MethodSwizzling.h>
#import <DarwinKit/shared/NSString+SafeUTF8.h>
#if defined(DARWINKIT_CONFIG_DARWIN_IOS)
#import <DarwinKit/ios/DKAboutViewController.h>
#import <DarwinKit/ios/DKAlertView.h>
#import <DarwinKit/ios/DKActionSheet.h>
#import <DarwinKit/ios/DKActionViewController.h>
#import <DarwinKit/ios/DKBackgroundView.h>
#import <DarwinKit/ios/DKContentView.h>
#import <DarwinKit/ios/DKCustomSegue.h>
#import <DarwinKit/ios/DKDynamicTextView.h>
#import <DarwinKit/ios/DKFileOpenSaveViewController.h>
#import <DarwinKit/ios/DKKeyboardNotificationListener.h>
#import <DarwinKit/ios/DKMenuView.h>
#import <DarwinKit/ios/DKOutlineView.h>
#import <DarwinKit/ios/DKOutlineViewCell.h>
#import <DarwinKit/ios/DKPopoverViewController.h>
//#import <DarwinKit/ios/DKPopoverViewController+Subclass.h>
#import <DarwinKit/ios/DKProgressHUD.h>
#import <DarwinKit/ios/DKProgressView.h>
#import <DarwinKit/ios/DKRatingView.h>
#import <DarwinKit/ios/DKSearchViewController.h>
#import <DarwinKit/ios/DKWebViewController.h>
#import <DarwinKit/ios/UIActionSheet+StyledAppearance.h>
#import <DarwinKit/ios/UIApplication+StyledAppearance.h>
#import <DarwinKit/ios/UIApplication+TouchViewing.h>
#import <DarwinKit/ios/UIBezierPath+Compatibility.h>
#import <DarwinKit/ios/UILabel+StyledAppearance.h>
#import <DarwinKit/ios/UINavigationBar+StyledAppearance.h>
#import <DarwinKit/ios/UINavigationController+InteractivePopGestureRecognizerFix.h>
#import <DarwinKit/ios/UIPopoverController+PassthroughViewsFix.h>
#import <DarwinKit/ios/UIPopoverController+StyledAppearance.h>
#import <DarwinKit/ios/UISearchBar+StyledAppearance.h>
#import <DarwinKit/ios/UITabBar+StyledAppearance.h>
#import <DarwinKit/ios/UITableView+StyledAppearance.h>
#import <DarwinKit/ios/UITableViewCell+StyledAppearance.h>
#import <DarwinKit/ios/UITableViewController+Categories.h>
#import <DarwinKit/ios/UITextField+StyledAppearance.h>
#import <DarwinKit/ios/UITextView+StyledAppearance.h>
#import <DarwinKit/ios/UIToolbar+ItemSearching.h>
#import <DarwinKit/ios/UIToolbar+StyledAppearance.h>
#import <DarwinKit/ios/UIView+StyledAppearance.h>
#import <DarwinKit/ios/UIView+ViewSearching.h>
#import <DarwinKit/ios/UIViewController+Notifications.h>
#elif defined(DARWINKIT_CONFIG_DARWIN_MAC)
#import <DarwinKit/mac/DKActionMenuButton.h>
#import <DarwinKit/mac/DKChangeableContentWindowController.h>
#import <DarwinKit/mac/DKEnumerationValueTransformer.h>
#import <DarwinKit/mac/DKPopoverViewController.h>
#import <DarwinKit/mac/NSApplication+StandardAboutPanel.h>
#import <DarwinKit/mac/NSApplication+StyledAppearance.h>
#import <DarwinKit/mac/NSBezierPath+Compatibility.h>
#import <DarwinKit/mac/NSBox+StyledAppearance.h>
#import <DarwinKit/mac/NSButtonCell+StyledAppearance.h>
#import <DarwinKit/mac/NSClipView+StyledAppearance.h>
#import <DarwinKit/mac/NSColor+ShadedColor.h>
#import <DarwinKit/mac/NSCursor+ContentsOfFile.h>
#import <DarwinKit/mac/NSImageCell+StyledAppearance.h>
#import <DarwinKit/mac/NSPopUpButtonCell+StyledAppearance.h>
#import <DarwinKit/mac/NSScrollView+StyledAppearance.h>
#import <DarwinKit/mac/NSSegmentedCell+StyledAppearance.h>
#import <DarwinKit/mac/NSStepperCell+StyledAppearance.h>
#import <DarwinKit/mac/NSTableRowView+StyledAppearance.h>
#import <DarwinKit/mac/NSTableView+StyledAppearance.h>
#import <DarwinKit/mac/NSTextFieldCell+StyledAppearance.h>
#import <DarwinKit/mac/NSTextView+StyledAppearance.h>
#import <DarwinKit/mac/NSToolbarItem+ViewItemValidation.h>
#import <DarwinKit/mac/NSView+StyledAppearance.h>
#import <DarwinKit/mac/NSView+ViewSearching.h>
#import <DarwinKit/mac/NSWindow+AutosaveFrameTopLeftFix.h>
#import <DarwinKit/mac/NSWindow+StyledAppearance.h>
#endif

#endif // DARWINKIT_DARWINKIT_H_INCLUDED
