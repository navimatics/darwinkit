/*
 * DarwinKit/mac/NSView+ViewSearching.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_MAC_NSVIEW_VIEWSEARCHING_H_INCLUDED
#define DARWINKIT_MAC_NSVIEW_VIEWSEARCHING_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

@interface NSView (ViewSearching)
- (id)viewWithClass:(Class)cls;
- (id)viewWithClass:(Class)cls tag:(int)tag;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_MAC_NSVIEW_VIEWSEARCHING_H_INCLUDED
