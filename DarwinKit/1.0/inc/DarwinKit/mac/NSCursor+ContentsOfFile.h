/*
 * DarwinKit/mac/NSCursor+ContentsOfFile.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_MAC_NSCURSOR_CONTENTSOFFILE_H_INCLUDED
#define DARWINKIT_MAC_NSCURSOR_CONTENTSOFFILE_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

@interface NSCursor (ContentsOfFile)
- (id)initWithContentsOfFile:(NSString *)path;
- (id)initWithResource:(NSString *)name;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_MAC_NSCURSOR_CONTENTSOFFILE_H_INCLUDED
