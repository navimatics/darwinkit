/*
 * DarwinKit/mac/DKChangeableContentWindowController.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_MAC_DKCHANGEABLECONTENTWINDOWCONTROLLER_H_INCLUDED
#define DARWINKIT_MAC_DKCHANGEABLECONTENTWINDOWCONTROLLER_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

DARWINKIT_APISYM
@interface DKChangeableContentWindowController : NSWindowController
- (void)setContentView:(NSView *)view;
- (void)setSelectedContentViewWithIdentifier:(NSString *)ident;
- (BOOL)contentViewShouldChangeWithView:(NSView *)view;
- (IBAction)changeContentViewFromToolbarItem:(id)sender;
- (IBAction)changeContentViewFromButton:(id)sender;
- (NSDictionary *)viewsDictionary;
- (NSString *)defaultIdentifier;
- (NSString *)identifierAutosaveName;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_MAC_DKCHANGEABLECONTENTWINDOWCONTROLLER_H_INCLUDED
