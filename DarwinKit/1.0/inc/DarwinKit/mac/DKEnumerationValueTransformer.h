/*
 * DarwinKit/mac/DKEnumerationValueTransformer.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_MAC_DKENUMERATIONVALUETRANSFORMER_H_INCLUDED
#define DARWINKIT_MAC_DKENUMERATIONVALUETRANSFORMER_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

DARWINKIT_APISYM
@interface DKEnumerationValueTransformer : NSValueTransformer
- (id)initWithIdentifiersAndValues:(NSString *)firstIdent, ...;
- (id)initWithEnumerationDictionary:(NSDictionary *)dict;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_MAC_DKENUMERATIONVALUETRANSFORMER_H_INCLUDED
