/*
 * DarwinKit/mac/DKPopoverViewController.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_MAC_DKPOPOVERVIEWCONTROLLER_H_INCLUDED
#define DARWINKIT_MAC_DKPOPOVERVIEWCONTROLLER_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

DARWINKIT_APISYM
@interface DKPopoverViewController : NSViewController <NSPopoverDelegate>
- (void)showInPopoverRelativeToRect:(NSRect)rect
    ofView:(NSView *)view
    preferredEdge:(NSRectEdge)edge;
- (void)close;
- (NSViewController *)detachableWindowViewController;
@property (readwrite, assign, nonatomic) NSPopoverBehavior behavior;
    /* default: NSPopoverBehaviorApplicationDefined */
@property (readwrite, assign, nonatomic) id<NSPopoverDelegate> popoverDelegate;
@property (readonly, nonatomic) NSPopover *popover;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_MAC_DKPOPOVERVIEWCONTROLLER_H_INCLUDED
