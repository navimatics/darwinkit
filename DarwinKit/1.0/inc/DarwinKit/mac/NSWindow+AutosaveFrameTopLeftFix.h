/*
 * DarwinKit/mac/NSWindow+AutosaveFrameTopLeftFix.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_MAC_NSWINDOW_AUTOSAVEFRAMETOPLEFTFIX_H_INCLUDED
#define DARWINKIT_MAC_NSWINDOW_AUTOSAVEFRAMETOPLEFTFIX_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

@interface NSWindow (AutosaveFrameTopLeftFix)
+ (void)loadAutosaveFrameTopLeftFix;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_MAC_NSWINDOW_AUTOSAVEFRAMETOPLEFTFIX_H_INCLUDED
