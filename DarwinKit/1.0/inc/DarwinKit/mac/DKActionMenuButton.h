/*
 * DarwinKit/mac/DKActionMenuButton.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_MAC_DKACTIONMENUBUTTON_H_INCLUDED
#define DARWINKIT_MAC_DKACTIONMENUBUTTON_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

/*
 * DKActionMenuButton is a simple subclass over NSPopUpButton that dissociates the button's
 * title/image from the popup menu (by setting usesItemFromMenu = NO in awakeFromNib).
 * This allows manipulation of the menu without affecting the button. This is especially
 * important when using bindings (e.g. contentValues).
 *
 * DKActionMenuButton sets the button's title/image to the alternate title/image and then
 * sets those to nil. Although this is a hack, it allows setting the desired title/image in IB.
 */
DARWINKIT_APISYM
@interface DKActionMenuButton : NSPopUpButton
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_MAC_DKACTIONMENUBUTTON_H_INCLUDED
