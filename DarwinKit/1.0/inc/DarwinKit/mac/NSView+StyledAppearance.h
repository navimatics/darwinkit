/*
 * DarwinKit/mac/NSView+StyledAppearance.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_MAC_NSVIEW_STYLEDAPPEARANCE_H_INCLUDED
#define DARWINKIT_MAC_NSVIEW_STYLEDAPPEARANCE_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

@interface NSView (StyledAppearance)
+ (void)loadStyledAppearance;
- (BOOL)shouldAppearStyled;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_MAC_NSVIEW_STYLEDAPPEARANCE_H_INCLUDED
