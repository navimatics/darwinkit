/*
 * DarwinKit/mac/NSColor+ShadedColor.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_MAC_NSCOLOR_SHADEDCOLOR_H_INCLUDED
#define DARWINKIT_MAC_NSCOLOR_SHADEDCOLOR_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

@interface NSColor (ShadedColor)
- (NSColor *)shadedColorWithDifference:(CGFloat)diff;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_MAC_NSCOLOR_SHADEDCOLOR_H_INCLUDED
