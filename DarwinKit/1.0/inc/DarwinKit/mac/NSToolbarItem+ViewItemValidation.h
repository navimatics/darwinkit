/*
 * DarwinKit/mac/NSToolbarItem+ViewItemValidation.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_MAC_NSTOOLBARITEM_VIEWITEMVALIDATION_H_INCLUDED
#define DARWINKIT_MAC_NSTOOLBARITEM_VIEWITEMVALIDATION_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

@interface NSToolbarItem (ViewItemValidation)
+ (void)loadViewItemValidation;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_MAC_NSTOOLBARITEM_VIEWITEMVALIDATION_H_INCLUDED
