/*
 * DarwinKit/mac/NSApplication+StandardAboutPanel.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_MAC_NSAPPLICATION_STANDARDABOUTPANEL_H_INCLUDED
#define DARWINKIT_MAC_NSAPPLICATION_STANDARDABOUTPANEL_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

@interface NSApplication (StandardAboutPanel)
+ (void)loadStandardAboutPanel;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_MAC_NSAPPLICATION_STANDARDABOUTPANEL_H_INCLUDED
