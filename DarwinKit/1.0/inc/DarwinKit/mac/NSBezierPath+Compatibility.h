/*
 * DarwinKit/mac/NSBezierPath+Compatibility.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_MAC_NSBEZIERPATH_COMPATIBILITY_H_INCLUDED
#define DARWINKIT_MAC_NSBEZIERPATH_COMPATIBILITY_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

#define DKBezierPath                    NSBezierPath

@interface NSBezierPath (Compatibility)
- (void)addLineToPoint:(CGPoint)point;
- (void)addCurveToPoint:(CGPoint)endPoint
    controlPoint1:(CGPoint)controlPoint1
    controlPoint2:(CGPoint)controlPoint2;
- (void)addArcWithCenter:(CGPoint)center
    radius:(CGFloat)radius
    startAngle:(CGFloat)startAngle
    endAngle:(CGFloat)endAngle
    clockwise:(BOOL)clockwise;
- (void)appendPath:(NSBezierPath *)path;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_MAC_NSBEZIERPATH_COMPATIBILITY_H_INCLUDED
