/*
 * DarwinKit/mac/NSBox+StyledAppearance.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_MAC_NSBOX_STYLEDAPPEARANCE_H_INCLUDED
#define DARWINKIT_MAC_NSBOX_STYLEDAPPEARANCE_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

@interface NSBox (StyledAppearance)
+ (void)loadStyledAppearance;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_MAC_NSBOX_STYLEDAPPEARANCE_H_INCLUDED
