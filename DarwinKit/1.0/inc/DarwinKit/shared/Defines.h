/*
 * DarwinKit/shared/Defines.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_SHARED_DEFINES_H_INCLUDED
#define DARWINKIT_SHARED_DEFINES_H_INCLUDED

#include <TargetConditionals.h>
#if TARGET_OS_IPHONE
#define DARWINKIT_CONFIG_DARWIN_IOS
#elif TARGET_OS_MAC
#define DARWINKIT_CONFIG_DARWIN_MAC
#endif

#if defined(DARWINKIT_CONFIG_DARWIN_IOS)
#import <UIKit/UIKit.h>
#elif defined(DARWINKIT_CONFIG_DARWIN_MAC)
#import <Cocoa/Cocoa.h>
#endif

#define DARWINKIT_APISYM                __attribute__ ((visibility("default")))

#endif // DARWINKIT_SHARED_DEFINES_H_INCLUDED
