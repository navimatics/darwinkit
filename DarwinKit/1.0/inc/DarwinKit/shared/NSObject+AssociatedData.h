/*
 * DarwinKit/shared/NSObject+AssociatedData.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_SHARED_NSOBJECT_ASSOCIATEDDATA_H_INCLUDED
#define DARWINKIT_SHARED_NSOBJECT_ASSOCIATEDDATA_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

@interface NSObject (AssociatedData)
- (void *)getAssociatedData:(size_t *)sizeKey;
- (id)getAssociatedObject:(Class)cls;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_SHARED_NSOBJECT_ASSOCIATEDDATA_H_INCLUDED
