/*
 * DarwinKit/shared/DKKeychain.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_SHARED_DKKEYCHAIN_H_INCLUDED
#define DARWINKIT_SHARED_DKKEYCHAIN_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

DARWINKIT_APISYM
@interface DKKeychain : NSObject
+ (DKKeychain *)defaultKeychain;
- (NSString *)passwordForAccount:(NSString *)accountName onService:(NSString *)serviceName;
- (NSData *)passwordDataForAccount:(NSString *)accountName onService:(NSString *)serviceName;
- (BOOL)setPassword:(NSString *)password forAccount:(NSString *)accountName onService:(NSString *)serviceName;
- (BOOL)setPasswordData:(NSData *)passwordData forAccount:(NSString *)accountName onService:(NSString *)serviceName;
- (BOOL)removePasswordForAccount:(NSString *)accountName onService:(NSString *)serviceName;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_SHARED_DKKEYCHAIN_H_INCLUDED
