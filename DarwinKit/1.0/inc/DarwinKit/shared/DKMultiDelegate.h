/*
 * DarwinKit/shared/DKMultiDelegate.h
 *
 * Copyright 2008-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_SHARED_DKMULTIDELEGATE_H_INCLUDED
#define DARWINKIT_SHARED_DKMULTIDELEGATE_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

DARWINKIT_APISYM
@interface DKMultiDelegate : NSObject
- (id)init;
- (id)initWithDelegate:(id)del;
- (void)addDelegate:(id)del;
- (void)removeDelegate:(id)del;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_SHARED_DKMULTIDELEGATE_H_INCLUDED
