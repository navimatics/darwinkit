/*
 * DarwinKit/shared/NSObject+MethodSwizzling.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_SHARED_NSOBJECT_METHODSWIZZLING_H_INCLUDED
#define DARWINKIT_SHARED_NSOBJECT_METHODSWIZZLING_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

@interface NSObject (MethodSwizzling)
+ (BOOL)swizzleInstanceMethod:(SEL)oldsel withMethod:(SEL)newsel;
+ (BOOL)swizzleClassMethod:(SEL)oldsel withMethod:(SEL)newsel;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_SHARED_NSOBJECT_METHODSWIZZLING_H_INCLUDED
