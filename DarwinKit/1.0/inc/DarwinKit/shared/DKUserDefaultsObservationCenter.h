/*
 * DarwinKit/shared/DKUserDefaultsObservationCenter.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_SHARED_DKUSERDEFAULTSOBSERVATIONCENTER_H_INCLUDED
#define DARWINKIT_SHARED_DKUSERDEFAULTSOBSERVATIONCENTER_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

DARWINKIT_APISYM
@interface DKUserDefaultsObservationCenter : NSObject
+ (DKUserDefaultsObservationCenter *)standardUserDefaultsObservationCenter;
- (id)initWithUserDefaults:(NSUserDefaults *)defaults;
- (void)addObserver:(id)observer forKeys:(NSString *)key, ...;
- (void)removeObserver:(id)observer forKeys:(NSString *)key, ...;
- (void)removeObserver:(id)observer;
@end
@protocol DKUserDefaultsObserver <NSObject>
@optional
- (void)userDefaults:(NSUserDefaults *)defaults didChangeForKeys:(NSSet *)keys;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_SHARED_DKUSERDEFAULTSOBSERVATIONCENTER_H_INCLUDED
