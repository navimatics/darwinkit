/*
 * DarwinKit/shared/NSString+SafeUTF8.h
 *
 * Copyright 2008-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_SHARED_NSSTRING_SAFEUTF8_H_INCLUDED
#define DARWINKIT_SHARED_NSSTRING_SAFEUTF8_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

@interface NSString (SafeUTF8)
+ (id)safeStringWithUTF8String:(const char *)utf8str;
- (const char *)safeUTF8String;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_SHARED_NSSTRING_SAFEUTF8_H_INCLUDED
