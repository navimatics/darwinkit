/*
 * DarwinKit/ios/UITabBar+StyledAppearance.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_IOS_UITABBAR_STYLEDAPPEARANCE_H_INCLUDED
#define DARWINKIT_IOS_UITABBAR_STYLEDAPPEARANCE_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

@interface UITabBar (StyledAppearance)
+ (void)loadStyledAppearance;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_IOS_UITABBAR_STYLEDAPPEARANCE_H_INCLUDED
