/*
 * DarwinKit/ios/UIToolbar+ItemSearching.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_IOS_UITOOLBAR_ITEMSEARCHING_H_INCLUDED
#define DARWINKIT_IOS_UITOOLBAR_ITEMSEARCHING_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

@interface UIToolbar (ItemSearching)
- (UIBarItem *)itemWithTag:(int)tag;
- (UIBarItem *)itemWithView:(UIView *)view;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_IOS_UITOOLBAR_ITEMSEARCHING_H_INCLUDED
