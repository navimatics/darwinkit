/*
 * DKFileOpenSaveViewController.h
 *
 * Copyright 2013 Navimatics Corporation. All rights reserved.
 */

@protocol DKFileOpenSaveViewControllerDelegate;
@interface DKFileOpenSaveViewController : UINavigationController
- (id)initWithDelegate:(id<DKFileOpenSaveViewControllerDelegate>)delegate;
@property (readwrite, copy, nonatomic) NSString *title;
@property (readwrite, copy, nonatomic) NSString *prompt;
@property (readwrite, copy, nonatomic) NSString *message;
@property (readwrite, copy, nonatomic) NSString *cancelButtonTitle;
@property (readwrite, copy, nonatomic) NSString *doneButtonTitle;
@property (readwrite, retain, nonatomic) NSURL *directoryURL; /* default is NSDocumentDirectory */
@property (readwrite, retain, nonatomic) NSArray *allowedFileTypes;
@property (readwrite, retain, nonatomic) NSDictionary *fileTypeIcons;
@property (readwrite, assign, nonatomic) BOOL showsFileTypes;
@property (readwrite, assign, nonatomic) BOOL showsFileInformation;
@property (readwrite, assign, nonatomic) BOOL allowsFileDelete;
@property (readonly, retain, nonatomic) NSURL *URL;
@end

@interface DKFileOpenViewController : DKFileOpenSaveViewController
@end

@interface DKFileSaveViewController : DKFileOpenSaveViewController
@end

@protocol DKFileOpenSaveViewControllerDelegate <NSObject>
@optional
- (void)fileOpenSaveViewController:(DKFileOpenSaveViewController *)controller
    dismissedWithResult:(NSInteger)result; /* 0: Cancel, 1: OK */
@end
