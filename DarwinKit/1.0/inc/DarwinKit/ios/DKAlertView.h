/*
 * DarwinKit/ios/DKAlertView.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_IOS_DKALERTVIEW_H_INCLUDED
#define DARWINKIT_IOS_DKALERTVIEW_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

DARWINKIT_APISYM
@interface DKAlertView : UIAlertView
- (id)initWithTitle:(NSString *)title message:(NSString *)message
    target:(id)target buttonTitlesAndActions:(NSString *)buttonTitle, ... NS_REQUIRES_NIL_TERMINATION;
- (NSInteger)addButtonWithTitle:(NSString *)buttonTitle action:(SEL)action;
@property (readwrite, retain, nonatomic) id context;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_IOS_DKALERTVIEW_H_INCLUDED
