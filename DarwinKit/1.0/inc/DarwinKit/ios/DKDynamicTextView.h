/*
 * DarwinKit/ios/DKDynamicTextView.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_IOS_DKDYNAMICTEXTVIEW_H_INCLUDED
#define DARWINKIT_IOS_DKDYNAMICTEXTVIEW_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

DARWINKIT_APISYM
@interface DKDynamicTextView : UITextView
- (void)commonInit;
@property (readwrite, assign, nonatomic) NSUInteger minNumberOfLines, maxNumberOfLines;
@property (readwrite, assign, nonatomic) BOOL adjustsSizeToFit;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_IOS_DKDYNAMICTEXTVIEW_H_INCLUDED
