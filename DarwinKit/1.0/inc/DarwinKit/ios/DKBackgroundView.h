/*
 * DarwinKit/ios/DKBackgroundView.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_IOS_DKBACKGROUNDVIEW_H_INCLUDED
#define DARWINKIT_IOS_DKBACKGROUNDVIEW_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

DARWINKIT_APISYM
@interface DKBackgroundView : UIView
+ (void)loadStyledAppearance;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_IOS_DKBACKGROUNDVIEW_H_INCLUDED
