/*
 * DarwinKit/ios/DKKeyboardNotificationListener.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_IOS_DKKEYBOARDNOTIFICATIONLISTENER_H_INCLUDED
#define DARWINKIT_IOS_DKKEYBOARDNOTIFICATIONLISTENER_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

DARWINKIT_APISYM
@interface DKKeyboardNotificationListener : NSObject
+ (DKKeyboardNotificationListener *)sharedListener;
- (void)addDelegate:(id)delegate;
- (void)removeDelegate:(id)delegate;
- (BOOL)isKeyboardVisible;
- (CGRect)keyboardFrame;
    /* screen coordinates */
@end

DARWINKIT_APISYM
@protocol DKKeyboardNotificationListenerDelegate <NSObject>
@optional
- (void)keyboardWillShow:(NSNotification *)notification;
- (void)keyboardDidShow:(NSNotification *)notification;
- (void)keyboardWillHide:(NSNotification *)notification;
- (void)keyboardDidHide:(NSNotification *)notification;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_IOS_DKKEYBOARDNOTIFICATIONLISTENER_H_INCLUDED
