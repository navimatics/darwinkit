/*
 * DarwinKit/ios/UINavigationController+InteractivePopGestureRecognizerFix.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_IOS_UINAVIGATIONCONTROLLER_INTERACTIVEPOPGESTURERECOGNIZERFIX_H_INCLUDED
#define DARWINKIT_IOS_UINAVIGATIONCONTROLLER_INTERACTIVEPOPGESTURERECOGNIZERFIX_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

@interface UINavigationController (InteractivePopGestureRecognizerFix)
+ (void)loadInteractivePopGestureRecognizerFix;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_IOS_UINAVIGATIONCONTROLLER_INTERACTIVEPOPGESTURERECOGNIZERFIX_H_INCLUDED
