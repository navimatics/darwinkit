/*
 * DarwinKit/ios/DKOutlineView.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_IOS_DKOUTLINEVIEW_H_INCLUDED
#define DARWINKIT_IOS_DKOUTLINEVIEW_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

DARWINKIT_APISYM
@interface DKOutlineView : UITableView
@property (readwrite, copy, nonatomic) NSString *childrenKeyPath;
@property (readwrite, copy, nonatomic) NSString *disclosedKeyPath;
@property (readwrite, copy, nonatomic) NSString *childrenAreLeavesKeyPath;
    /* This is an optional key path that is used to improve tree traversal performance.
     * When a tree node returns true for this key path, the tree traversal algorithm
     * does not need to examine each individual child to determine descendant count or
     * next descendant.
     */
- (id)treeForSection:(NSInteger)section;
- (void)setTree:(id)tree forSection:(NSInteger)section;
- (void)removeTreeForSection:(NSInteger)section;
- (NSInteger)numberOfItemsInSection:(NSInteger)section;
- (id)itemForRowAtIndexPath:(NSIndexPath *)indexPath level:(NSInteger *)plevel;
@end

DARWINKIT_APISYM
@protocol DKOutlineViewDelegate <NSObject, UITableViewDelegate>
@optional
- (void)outlineView:(DKOutlineView *)outlineView disclosureButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_IOS_DKOUTLINEVIEW_H_INCLUDED
