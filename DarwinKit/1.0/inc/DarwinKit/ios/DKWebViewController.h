/*
 * DarwinKit/ios/DKWebViewController.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_IOS_DKWEBVIEWCONTROLLER_H_INCLUDED
#define DARWINKIT_IOS_DKWEBVIEWCONTROLLER_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

DARWINKIT_APISYM
@interface DKWebViewController : UIViewController
- (id)initWithURL:(NSURL *)url;
- (void)presentOverViewController:(UIViewController *)controller
    animated:(BOOL)animated;
@property (readonly, retain, nonatomic) UIWebView *webView;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_IOS_DKWEBVIEWCONTROLLER_H_INCLUDED
