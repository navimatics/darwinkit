/*
 * DarwinKit/ios/UIPopoverController+PassthroughViewsFix.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_IOS_UIPOPOVERCONTROLLER_PASSTHROUGHVIEWSFIX_H_INCLUDED
#define DARWINKIT_IOS_UIPOPOVERCONTROLLER_PASSTHROUGHVIEWSFIX_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

@interface UIPopoverController (PassthroughViewsFix)
+ (void)enablePassthroughViews:(BOOL)flag;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_IOS_UIPOPOVERCONTROLLER_PASSTHROUGHVIEWSFIX_H_INCLUDED
