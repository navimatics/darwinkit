/*
 * DarwinKit/ios/DKAboutViewController.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_IOS_DKABOUTVIEWCONTROLLER_H_INCLUDED
#define DARWINKIT_IOS_DKABOUTVIEWCONTROLLER_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

DARWINKIT_APISYM
@interface DKAboutViewController : UIViewController
- (id)initWithOptions:(NSDictionary *)options;
- (void)presentOverViewController:(UIViewController *)controller
    animated:(BOOL)animated;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_IOS_DKABOUTVIEWCONTROLLER_H_INCLUDED
