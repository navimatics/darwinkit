/*
 * DarwinKit/ios/UIView+StyledAppearance.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_IOS_UIVIEW_STYLEDAPPEARANCE_H_INCLUDED
#define DARWINKIT_IOS_UIVIEW_STYLEDAPPEARANCE_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

@interface UIView (StyledAppearance)
+ (void)loadStyledAppearance;
- (BOOL)shouldAppearStyled;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_IOS_UIVIEW_STYLEDAPPEARANCE_H_INCLUDED
