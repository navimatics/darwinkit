/*
 * DarwinKit/ios/DKRatingView.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_IOS_DKRATINGVIEW_H_INCLUDED
#define DARWINKIT_IOS_DKRATINGVIEW_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

extern NSString *const kRatingViewStateUnrated;
extern NSString *const kRatingViewStateRated;
extern NSString *const kRatingViewStateUserRated;

DARWINKIT_APISYM
@interface DKRatingView : UIView
- (UIImage *)starImageForState:(NSString *)state;
- (void)setStarImage:(UIImage *)image forState:(NSString *)state;
@property (readwrite, assign, nonatomic) NSUInteger stars;
@property (readwrite, assign, nonatomic) float rating;
@property (readwrite, assign, nonatomic) float userRating;
@property (readwrite, assign, nonatomic) BOOL usesFractionalStarsForUserRating;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_IOS_DKRATINGVIEW_H_INCLUDED
