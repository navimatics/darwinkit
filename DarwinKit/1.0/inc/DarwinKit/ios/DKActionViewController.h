/*
 * DarwinKit/ios/DKActionViewController.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_IOS_DKACTIONVIEWCONTROLLER_H_INCLUDED
#define DARWINKIT_IOS_DKACTIONVIEWCONTROLLER_H_INCLUDED

#import <DarwinKit/ios/DKPopoverViewController.h>

#ifdef __cplusplus
extern "C" {
#endif

DARWINKIT_APISYM
@interface DKActionViewController : DKPopoverViewController
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_IOS_DKACTIONVIEWCONTROLLER_H_INCLUDED
