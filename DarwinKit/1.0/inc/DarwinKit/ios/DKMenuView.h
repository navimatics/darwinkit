/*
 * DarwinKit/ios/DKMenuView.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_IOS_DKMENUVIEW_H_INCLUDED
#define DARWINKIT_IOS_DKMENUVIEW_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

DARWINKIT_APISYM
@interface DKMenuView : UIView
- (id)initWithTarget:(id)target;
- (id)initWithTarget:(id)target buttonTitlesAndActions:(NSString *)buttonTitle, ... NS_REQUIRES_NIL_TERMINATION;
- (NSInteger)addButtonWithTitle:(NSString *)buttonTitle action:(SEL)action;
- (NSInteger)addButton:(UIButton *)button action:(SEL)action;
- (void)showFromRect:(CGRect)rect inView:(UIView *)view animated:(BOOL)animated;
- (void)dismissMenuAnimated:(BOOL)animated;
@property (readwrite, retain, nonatomic) UIColor *buttonTitleColor;
@property (readwrite, retain, nonatomic) UIFont *buttonTitleFont;
@property (readwrite, assign, nonatomic) NSInteger selectedButtonIndex;
@property (readonly, assign, nonatomic) NSInteger dismissedButtonIndex;
    /* only valid when action is triggered after menu dismissal */
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_IOS_DKMENUVIEW_H_INCLUDED
