/*
 * DarwinKit/ios/DKOutlineViewCell.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_IOS_DKOUTLINEVIEWCELL_H_INCLUDED
#define DARWINKIT_IOS_DKOUTLINEVIEWCELL_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

DARWINKIT_APISYM
@interface DKOutlineViewCell : UITableViewCell
@property (readwrite, assign, nonatomic) BOOL showsDisclosureButton;
@property (readwrite, assign, nonatomic, getter = isDisclosed) BOOL disclosed;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_IOS_DKOUTLINEVIEWCELL_H_INCLUDED
