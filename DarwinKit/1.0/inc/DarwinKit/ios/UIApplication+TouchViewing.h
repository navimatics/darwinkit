/*
 * DarwinKit/ios/UIApplication+TouchViewing.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_IOS_UIAPPLICATION_TOUCHVIEWING_H_INCLUDED
#define DARWINKIT_IOS_UIAPPLICATION_TOUCHVIEWING_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

@interface UIApplication (TouchViewing)
+ (void)loadTouchViewing;
+ (void)enableTouchViewing:(BOOL)flag;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_IOS_UIAPPLICATION_TOUCHVIEWING_H_INCLUDED
