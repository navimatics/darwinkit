/*
 * DarwinKit/ios/UIBezierPath+Compatibility.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_IOS_UIBEZIERPATH_COMPATIBILITY_H_INCLUDED
#define DARWINKIT_IOS_UIBEZIERPATH_COMPATIBILITY_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

#define DKBezierPath                    UIBezierPath

@interface UIBezierPath (Compatibility)
+ (void)fillRect:(CGRect)rect;
+ (void)strokeRect:(CGRect)rect;
+ (void)clipRect:(CGRect)rect;
+ (void)strokeLineFromPoint:(CGPoint)point1 toPoint:(CGPoint)point2;
- (void)lineToPoint:(CGPoint)point;
- (void)curveToPoint:(CGPoint)endPoint
    controlPoint1:(CGPoint)controlPoint1
    controlPoint2:(CGPoint)controlPoint2;
- (void)appendBezierPath:(UIBezierPath *)path;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_IOS_UIBEZIERPATH_COMPATIBILITY_H_INCLUDED
