/*
 * DarwinKit/ios/UIApplication+StyledAppearance.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_IOS_UIAPPLICATION_STYLEDAPPEARANCE_H_INCLUDED
#define DARWINKIT_IOS_UIAPPLICATION_STYLEDAPPEARANCE_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

@interface UIApplication (StyledAppearance)
+ (void)loadStyledAppearance;
+ (NSString *)styledAppearance;
+ (void)setStyledAppearance:(NSString *)name;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_IOS_UIAPPLICATION_STYLEDAPPEARANCE_H_INCLUDED
