/*
 * DarwinKit/ios/DKPopoverViewController.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_IOS_DKPOPOVERVIEWCONTROLLER_H_INCLUDED
#define DARWINKIT_IOS_DKPOPOVERVIEWCONTROLLER_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

DARWINKIT_APISYM
@interface DKPopoverViewController : UIViewController
- (void)presentOverViewController:(UIViewController *)controller
    animated:(BOOL)animated;
- (void)presentOverViewController:(UIViewController *)controller
    fromRect:(CGRect)rect animated:(BOOL)animated;
- (void)presentOverViewController:(UIViewController *)controller
    fromBarButtonItem:(UIBarButtonItem *)item animated:(BOOL)animated;
- (UIViewController *)containerViewController;
- (void)dismissViewControllerAnimated:(BOOL)animated completion: (void (^)(void))completion;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_IOS_DKPOPOVERVIEWCONTROLLER_H_INCLUDED
