/*
 * DarwinKit/ios/DKPopoverViewController+Subclass.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_IOS_DKPOPOVERVIEWCONTROLLER_SUBCLASS_H_INCLUDED
#define DARWINKIT_IOS_DKPOPOVERVIEWCONTROLLER_SUBCLASS_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

@interface DKPopoverViewController (Subclass)
- (UIPopoverController *)padIdiomPopoverController;
- (void)padIdiomPresentFromRect:(CGRect)rect item:(id)item animated:(BOOL)animated;
- (void)padIdiomDismissViewControllerAnimated:(BOOL)animated;
- (void)phoneIdiomPresentOverViewController:(UIViewController *)controller animated:(BOOL)animated;
- (void)phoneIdiomDismissViewControllerAnimated:(BOOL)animated;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_IOS_DKPOPOVERVIEWCONTROLLER_SUBCLASS_H_INCLUDED
