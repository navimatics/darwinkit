/*
 * DarwinKit/ios/DKProgressView.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_IOS_DKPROGRESSVIEW_H_INCLUDED
#define DARWINKIT_IOS_DKPROGRESSVIEW_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

DARWINKIT_APISYM
@interface DKProgressView : UIProgressView
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_IOS_DKPROGRESSVIEW_H_INCLUDED
