/*
 * DarwinKit/ios/DKProgressHUD.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_IOS_DKPROGRESSHUD_H_INCLUDED
#define DARWINKIT_IOS_DKPROGRESSHUD_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

DARWINKIT_APISYM
@interface DKProgressHUD : UIView
+ (void)showWithMessage:(NSString *)message allowUserInteraction:(BOOL)flag;
+ (void)showSuccessWithMessage:(NSString *)message allowUserInteraction:(BOOL)flag;
+ (void)showFailureWithMessage:(NSString *)message allowUserInteraction:(BOOL)flag;
- (id)init;
- (void)showWithDuration:(NSTimeInterval)duration animated:(BOOL)animated;
- (void)dismissAnimated:(BOOL)animated;
@property (readwrite, assign, nonatomic) BOOL showsActivityIndicator;
@property (readwrite, retain, nonatomic) UIImage *image;
@property (readwrite, copy, nonatomic) NSString *message;
@end

extern NSString *const DKProgressHUDWillAppearNotification;
extern NSString *const DKProgressHUDDidAppearNotification;
extern NSString *const DKProgressHUDWillDisappearNotification;
extern NSString *const DKProgressHUDDidDisappearNotification;

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_IOS_DKPROGRESSHUD_H_INCLUDED
