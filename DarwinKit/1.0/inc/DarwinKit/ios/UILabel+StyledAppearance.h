/*
 * DarwinKit/ios/UILabel+StyledAppearance.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_IOS_UILABEL_STYLEDAPPEARANCE_H_INCLUDED
#define DARWINKIT_IOS_UILABEL_STYLEDAPPEARANCE_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

@interface UILabel (StyledAppearance)
+ (void)loadStyledAppearance;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_IOS_UILABEL_STYLEDAPPEARANCE_H_INCLUDED
