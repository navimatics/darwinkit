/*
 * DarwinKit/ios/DKSearchViewController.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_IOS_DKSEARCHVIEWCONTROLLER_H_INCLUDED
#define DARWINKIT_IOS_DKSEARCHVIEWCONTROLLER_H_INCLUDED

#import <DarwinKit/ios/DKPopoverViewController.h>

#ifdef __cplusplus
extern "C" {
#endif

DARWINKIT_APISYM
@interface DKSearchViewController : DKPopoverViewController
- (void)presentOverViewController:(UIViewController *)controller
    fromSearchBar:(UISearchBar *)item animated:(BOOL)animated;
- (void)searchScopeDidChange:(id)sender;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_IOS_DKSEARCHVIEWCONTROLLER_H_INCLUDED
