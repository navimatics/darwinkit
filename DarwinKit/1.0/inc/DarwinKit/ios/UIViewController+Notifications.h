/*
 * DarwinKit/ios/UIViewController+Notifications.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_IOS_UIVIEWCONTROLLER_NOTIFICATIONS_H_INCLUDED
#define DARWINKIT_IOS_UIVIEWCONTROLLER_NOTIFICATIONS_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

@interface UIViewController (Notifications)
+ (void)loadNotifications;
@end

extern NSString *const UIViewControllerViewWillAppearNotification;
extern NSString *const UIViewControllerViewDidAppearNotification;
extern NSString *const UIViewControllerViewWillDisappearNotification;
extern NSString *const UIViewControllerViewDidDisappearNotification;

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_IOS_UIVIEWCONTROLLER_NOTIFICATIONS_H_INCLUDED
