/*
 * DarwinKit/ios/DKActionSheet.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_IOS_DKACTIONSHEET_H_INCLUDED
#define DARWINKIT_IOS_DKACTIONSHEET_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

DARWINKIT_APISYM
@interface DKActionSheet : UIActionSheet
- (id)initWithTarget:(id)target;
- (id)initWithTarget:(id)target buttonTitlesAndActions:(NSString *)buttonTitle, ... NS_REQUIRES_NIL_TERMINATION;
- (NSInteger)addButtonWithTitle:(NSString *)buttonTitle action:(SEL)action;
@property (readonly, assign, nonatomic) NSInteger dismissedButtonIndex;
    /* only valid when action is triggered after sheet dismissal */
@property (readwrite, retain, nonatomic) id context;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_IOS_DKACTIONSHEET_H_INCLUDED
