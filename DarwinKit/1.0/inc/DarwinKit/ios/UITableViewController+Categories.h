/*
 * DarwinKit/ios/UITableViewController+Categories.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_IOS_UITABLEVIEWCONTROLLER_CATEGORIES_H_INCLUDED
#define DARWINKIT_IOS_UITABLEVIEWCONTROLLER_CATEGORIES_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

@interface UITableViewController (PrototypeCellSpecification)
@property (readwrite, copy, nonatomic) NSString *prototypeCellSpecification;
- (NSArray *)prototypeCellIdentifierSections;
- (NSArray *)prototypeCellIdentifiersInSection:(NSInteger)sectionIndex;
- (NSString *)prototypeCellIdentifierForRowAtIndexPath:(NSIndexPath *)indexPath;
- (CGFloat)prototypeCellHeightForRowAtIndexPath:(NSIndexPath *)indexPath;
@end

@interface UITableViewController (EndsEditingOnTap)
@property (readwrite, assign, nonatomic) BOOL endsEditingOnTap;
@end

@interface UITableViewController (RowChecking)
- (NSSet *)indexPathsForCheckedRowsInSection:(NSInteger)section;
- (void)setIndexPathsForCheckedRows:(NSSet *)indexPaths inSection:(NSInteger)section;
- (BOOL)isCheckedRowAtIndexPath:(NSIndexPath *)indexPath;
- (void)checkRowAtIndexPath:(NSIndexPath *)indexPath allowMultiple:(BOOL)flag;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_IOS_UITABLEVIEWCONTROLLER_CATEGORIES_H_INCLUDED
