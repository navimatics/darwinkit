/*
 * DarwinKit/ios/UIView+ViewSearching.h
 *
 * Copyright 2011-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DARWINKIT_IOS_UIVIEW_VIEWSEARCHING_H_INCLUDED
#define DARWINKIT_IOS_UIVIEW_VIEWSEARCHING_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

@interface UIView (ViewSearching)
- (UIView *)superviewWithClass:(Class)cls;
- (UIView *)viewWithClass:(Class)cls;
- (UIView *)viewWithClass:(Class)cls tag:(int)tag;
@end

#ifdef __cplusplus
}
#endif

#endif // DARWINKIT_IOS_UIVIEW_VIEWSEARCHING_H_INCLUDED
